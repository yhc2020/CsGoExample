﻿namespace fhs
{
    partial class ProductForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_当前 = new System.Windows.Forms.TextBox();
            this.comboBox_列表 = new System.Windows.Forms.ComboBox();
            this.textBox_添加 = new System.Windows.Forms.TextBox();
            this.button_添加 = new System.Windows.Forms.Button();
            this.button_删除 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_应用 = new System.Windows.Forms.Button();
            this.button_保存当前 = new System.Windows.Forms.Button();
            this.button_保存所有 = new System.Windows.Forms.Button();
            this.button_复制 = new System.Windows.Forms.Button();
            this.button_删除标定 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "当前";
            // 
            // textBox_当前
            // 
            this.textBox_当前.Location = new System.Drawing.Point(41, 7);
            this.textBox_当前.Name = "textBox_当前";
            this.textBox_当前.ReadOnly = true;
            this.textBox_当前.Size = new System.Drawing.Size(100, 21);
            this.textBox_当前.TabIndex = 1;
            // 
            // comboBox_列表
            // 
            this.comboBox_列表.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_列表.FormattingEnabled = true;
            this.comboBox_列表.Location = new System.Drawing.Point(7, 34);
            this.comboBox_列表.Name = "comboBox_列表";
            this.comboBox_列表.Size = new System.Drawing.Size(134, 20);
            this.comboBox_列表.TabIndex = 2;
            this.comboBox_列表.SelectedIndexChanged += new System.EventHandler(this.comboBox_列表_SelectedIndexChanged);
            // 
            // textBox_添加
            // 
            this.textBox_添加.Location = new System.Drawing.Point(147, 7);
            this.textBox_添加.MaxLength = 32;
            this.textBox_添加.Name = "textBox_添加";
            this.textBox_添加.Size = new System.Drawing.Size(134, 21);
            this.textBox_添加.TabIndex = 3;
            // 
            // button_添加
            // 
            this.button_添加.Location = new System.Drawing.Point(287, 6);
            this.button_添加.Name = "button_添加";
            this.button_添加.Size = new System.Drawing.Size(75, 23);
            this.button_添加.TabIndex = 4;
            this.button_添加.Text = "添加";
            this.button_添加.UseVisualStyleBackColor = true;
            this.button_添加.Click += new System.EventHandler(this.button_添加_Click);
            // 
            // button_删除
            // 
            this.button_删除.Location = new System.Drawing.Point(228, 33);
            this.button_删除.Name = "button_删除";
            this.button_删除.Size = new System.Drawing.Size(75, 23);
            this.button_删除.TabIndex = 5;
            this.button_删除.Text = "删除";
            this.button_删除.UseVisualStyleBackColor = true;
            this.button_删除.Click += new System.EventHandler(this.button_删除_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Location = new System.Drawing.Point(7, 62);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(625, 558);
            this.panel1.TabIndex = 6;
            // 
            // button_应用
            // 
            this.button_应用.Location = new System.Drawing.Point(147, 33);
            this.button_应用.Name = "button_应用";
            this.button_应用.Size = new System.Drawing.Size(75, 23);
            this.button_应用.TabIndex = 7;
            this.button_应用.Text = "应用";
            this.button_应用.UseVisualStyleBackColor = true;
            this.button_应用.Click += new System.EventHandler(this.button_应用_Click);
            // 
            // button_保存当前
            // 
            this.button_保存当前.Location = new System.Drawing.Point(309, 33);
            this.button_保存当前.Name = "button_保存当前";
            this.button_保存当前.Size = new System.Drawing.Size(75, 23);
            this.button_保存当前.TabIndex = 8;
            this.button_保存当前.Text = "保存当前";
            this.button_保存当前.UseVisualStyleBackColor = true;
            this.button_保存当前.Click += new System.EventHandler(this.button_保存当前_Click);
            // 
            // button_保存所有
            // 
            this.button_保存所有.Location = new System.Drawing.Point(391, 33);
            this.button_保存所有.Name = "button_保存所有";
            this.button_保存所有.Size = new System.Drawing.Size(75, 23);
            this.button_保存所有.TabIndex = 9;
            this.button_保存所有.Text = "保存所有";
            this.button_保存所有.UseVisualStyleBackColor = true;
            this.button_保存所有.Click += new System.EventHandler(this.button_保存所有_Click);
            // 
            // button_复制
            // 
            this.button_复制.Location = new System.Drawing.Point(368, 6);
            this.button_复制.Name = "button_复制";
            this.button_复制.Size = new System.Drawing.Size(75, 23);
            this.button_复制.TabIndex = 4;
            this.button_复制.Text = "复制";
            this.button_复制.UseVisualStyleBackColor = true;
            this.button_复制.Click += new System.EventHandler(this.button_复制_Click);
            // 
            // button_删除标定
            // 
            this.button_删除标定.Location = new System.Drawing.Point(449, 6);
            this.button_删除标定.Name = "button_删除标定";
            this.button_删除标定.Size = new System.Drawing.Size(75, 23);
            this.button_删除标定.TabIndex = 10;
            this.button_删除标定.Text = "删除标定";
            this.button_删除标定.UseVisualStyleBackColor = true;
            this.button_删除标定.Click += new System.EventHandler(this.button_删除标定_Click);
            // 
            // ProductForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 632);
            this.Controls.Add(this.button_删除标定);
            this.Controls.Add(this.button_保存所有);
            this.Controls.Add(this.button_保存当前);
            this.Controls.Add(this.button_应用);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button_删除);
            this.Controls.Add(this.button_复制);
            this.Controls.Add(this.button_添加);
            this.Controls.Add(this.textBox_添加);
            this.Controls.Add(this.comboBox_列表);
            this.Controls.Add(this.textBox_当前);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ProductForm";
            this.Text = "ProductForm";
            this.Load += new System.EventHandler(this.ProductForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_当前;
        private System.Windows.Forms.ComboBox comboBox_列表;
        private System.Windows.Forms.TextBox textBox_添加;
        private System.Windows.Forms.Button button_添加;
        private System.Windows.Forms.Button button_删除;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button_应用;
        private System.Windows.Forms.Button button_保存当前;
        private System.Windows.Forms.Button button_保存所有;
        private System.Windows.Forms.Button button_复制;
        private System.Windows.Forms.Button button_删除标定;
    }
}