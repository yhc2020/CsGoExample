﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace fhs
{
    class ProductMgr
    {
        static public readonly LinkedList<Action<string>> productChangeEvent = new LinkedList<Action<string>>();
        static public readonly Dictionary<string, ProductParam> products = new Dictionary<string, ProductParam>();
        static public string currProduct = "";

        static public void Load()
        {
            currProduct = DefaultLoad.Load("", @".\Product\Current.txt", 0);
            string[] productPaths = Directory.GetDirectories(@".\Product\Config");
            foreach (string productPath in productPaths)
            {
                string productName = Path.GetFileName(productPath);
                try
                {
                    products.Add(productName, ProductParam.Load(productName));
                }
                catch (Exception)
                {
                    products.Remove(productName);
                    LogMgr.Error($@"{productName} 料号异常");
                }
            }
            if (0 == products.Count)
            {
                currProduct = "";
            }
            else if (!products.ContainsKey(currProduct))
            {
                currProduct = products.FirstOrDefault().Key;
            }
        }

        static public void Save(string name)
        {
            File.WriteAllLines(@".\Product\Current.txt", new string[] { currProduct });
            products[name].Save(name);
        }

        static public void Save()
        {
            File.WriteAllLines(@".\Product\Current.txt", new string[] { currProduct });
            foreach (KeyValuePair<string, ProductParam> product in products)
            {
                product.Value.Save(product.Key);
            }
        }

        static public ProductParam New(string productName)
        {
            if (products.ContainsKey(productName))
            {
                return null;
            }
            try
            {
                if (DirectOpt.CopyDirectory(@".\Product\Temp", $@".\Product\Config\{productName}"))
                {
                    ProductParam result = ProductParam.Load(productName);
                    products.Add(productName, result);
                    return result;
                }
                LogMgr.Error($@"{productName} 料号建立错误");
                return null;
            }
            catch (Exception)
            {
                products.Remove(productName);
                LogMgr.Error($@"{productName} 料号建立异常");
                return null;
            }
        }

        static public ProductParam Copy(string productName, string srcProductName)
        {
            if (products.ContainsKey(productName))
            {
                return null;
            }
            try
            {
                if (DirectOpt.CopyDirectory($@".\Product\Config\{srcProductName}", $@".\Product\Config\{productName}"))
                {
                    ProductParam result = ProductParam.Load(productName);
                    products.Add(productName, result);
                    return result;
                }
                LogMgr.Error($@"{productName} 料号建立错误");
                return null;
            }
            catch (Exception)
            {
                products.Remove(productName);
                LogMgr.Error($@"{productName} 料号建立异常");
                return null;
            }
        }

        static public bool Remove(string productName)
        {
            try
            {
                if (DirectOpt.RemoveDirectory($@".\Product\Config\{productName}"))
                {
                    products.Remove(productName);
                    return true;
                }
                LogMgr.Error($@"{productName} 料号移除错误");
            }
            catch (Exception)
            {
                LogMgr.Error($@"{productName} 料号移除异常");
            }
            return false;
        }

        static public void Change(string productName)
        {
            currProduct = productName;
            File.WriteAllLines(@".\Product\Current.txt", new string[] { currProduct });
        }

        static public void ChangeNotify()
        {
            LinkedList<Action<string>> temp = new LinkedList<Action<string>>();
            foreach (Action<string> item in productChangeEvent)
            {
                temp.AddLast(item);
            }
            foreach (Action<string> item in temp)
            {
                item.Invoke(currProduct);
            }
        }
    }
}
