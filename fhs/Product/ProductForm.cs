﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    public partial class ProductForm : Form
    {
        ProductParamForm _paramForm;

        public ProductForm()
        {
            InitializeComponent();
        }

        private void ProductForm_Load(object sender, EventArgs e)
        {
            _paramForm = new ProductParamForm();
            _paramForm.TopLevel = false;
            _paramForm.Dock = DockStyle.Fill;
            _paramForm.Visible = true;
            panel1.Controls.Add(_paramForm);
            textBox_当前.Text = ProductMgr.currProduct;
            foreach (var item in ProductMgr.products)
            {
                comboBox_列表.Items.Add(item.Key);
            }
            if (0 != comboBox_列表.Items.Count)
            {
                comboBox_列表.SelectedItem = ProductMgr.currProduct;
            }
        }

        private void button_添加_Click(object sender, EventArgs e)
        {
            string productName = textBox_添加.Text;
            if (ProductMgr.products.ContainsKey(productName))
            {
                MessageBox.Show($"{productName} 料号已存在");
                return;
            }
            if (0 == productName.Length)
            {
                MessageBox.Show($"料号不能为空");
                return;
            }
            if (productName.IndexOfAny(new char[] { ' ', '\t', '\\', '/', ':', '*', '?', '"', '<', '>', '|' }, 0) >= 0)
            {
                MessageBox.Show("料号中不能使用 \\ / : * ? \" < > | Space Tab 字符");
                return;
            }
            LogMgr.Info($"建立料号 {productName}");
            ProductParam param = ProductMgr.New(productName);
            if (null != param)
            {
                comboBox_列表.SelectedIndex = comboBox_列表.Items.Add(productName);
                DirectOpt.CopyDirectory(@".\PointConfig\Temp", $@".\PointConfig\Product\{productName}");
                string vPath = @".\VisionConfig\Temp";
                DirectOpt.CopyDirectory(vPath, $@".\VisionConfig\Product\{productName}");
                DirectOpt.RemoveDirectory($@".\Product\Config\{productName}\ProductCalib\");
            }
            else
            {
                MessageBox.Show("无法正常创建料号");
            }
        }

        private void button_复制_Click(object sender, EventArgs e)
        {
            string productName = textBox_添加.Text;
            if (ProductMgr.products.ContainsKey(productName))
            {
                MessageBox.Show($"{productName} 料号已存在");
                return;
            }
            if (0 == productName.Length)
            {
                MessageBox.Show($"料号不能为空");
                return;
            }
            if (productName.IndexOfAny(new char[] { ' ', '\t', '\\', '/', ':', '*', '?', '"', '<', '>', '|' }, 0) >= 0)
            {
                MessageBox.Show("料号中不能使用 \\ / : * ? \" < > | Space Tab 字符");
                return;
            }
            LogMgr.Info($"建立料号 {productName}");
            ProductParam param = ProductMgr.Copy(productName, ProductMgr.currProduct);
            if (null != param)
            {
                comboBox_列表.SelectedIndex = comboBox_列表.Items.Add(productName);
                DirectOpt.CopyDirectory($@".\PointConfig\Product\{ProductMgr.currProduct}", $@".\PointConfig\Product\{productName}");
                string vPath = $@".\VisionConfig\Product\{ProductMgr.currProduct}";
                DirectOpt.CopyDirectory(vPath, $@".\VisionConfig\Product\{productName}");
                DirectOpt.RemoveDirectory($@".\Product\Config\{productName}\ProductCalib\");
            }
            else
            {
                MessageBox.Show("无法正常创建料号");
            }
        }

        private void button_删除_Click(object sender, EventArgs e)
        {
            string productName = comboBox_列表.Text;
            if (productName == ProductMgr.currProduct)
            {
                MessageBox.Show($"{productName} 料号正在使用");
                return;
            }
            if (MessageBox.Show($"确定删除料号 {productName} ？", "删除料号", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                LogMgr.Info($"删除料号 {productName}");
                if (ProductMgr.Remove(productName) &&
                    DirectOpt.RemoveDirectory($@".\VisionConfig\Product\{productName}") &&
                    DirectOpt.RemoveDirectory($@".\PointConfig\Product\{productName}"))
                {
                    comboBox_列表.Items.Remove(productName);
                    comboBox_列表.SelectedIndex = 0;
                }
                else
                {
                    MessageBox.Show("无法正常移除料号，尝试去手动移除");
                }
            }
        }

        private void button_应用_Click(object sender, EventArgs e)
        {
            string productName = comboBox_列表.Text;
            if (productName == ProductMgr.currProduct)
            {
                return;
            }
            LogMgr.Info($"应用料号 {productName}");
            ProductMgr.Change(productName);
            textBox_当前.Text = productName;
            CurrProductForm.obj.label_currProduct.Text = productName;
            VisionMgr.Load();
            PointMgr.Load();
            ProductMgr.ChangeNotify();
        }

        private void button_保存当前_Click(object sender, EventArgs e)
        {
            ProductMgr.Save(comboBox_列表.Text);
        }

        private void button_保存所有_Click(object sender, EventArgs e)
        {
            ProductMgr.Save();
        }

        private void comboBox_列表_SelectedIndexChanged(object sender, EventArgs e)
        {
            _paramForm.ReLoad(comboBox_列表.Text);
        }

        private void button_删除标定_Click(object sender, EventArgs e)
        {
            DirectOpt.RemoveDirectory($@".\Product\Config\{ProductMgr.currProduct}\ProductCalib\");
        }
    }
}
