﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Go;

namespace fhs
{
    public partial class PointDebugForm : Form
    {
        private PointsEditForm pointEdit;
        private AxisOptionForm axisOption;

        public PointDebugForm()
        {
            InitializeComponent();
        }

        private void PointDebugForm_Load(object sender, EventArgs e)
        {
            {
                axisOption = new AxisOptionForm(this);
                axisOption.TopLevel = false;
                axisOption.Dock = DockStyle.Fill;
                axisOption.Visible = true;
                panel1.Controls.Add(axisOption);
            }
            {
                pointEdit = new PointsEditForm(this);
                pointEdit.TopLevel = false;
                pointEdit.Dock = DockStyle.Fill;
                pointEdit.Visible = true;
                panel2.Controls.Add(pointEdit);
            }
        }

        public bool AbsMoveHandler(PointName.Axis axis, int pos, int vel, double acc, double dec)
        {
            try
            {
                if (MotionMgr.motions[axis].Run)
                {
                    LogMgr.Info($"{axis}轴 没有停止");
                    return false;
                }
                if (vel > 60)
                {
                    LogMgr.Warning("轴设置速度不能大于60");
                    return false;
                }
                MotionMgr.motions[axis].Vec = vel;
                MotionMgr.motions[axis].Acc = acc;
                MotionMgr.motions[axis].Dec = dec;
                MotionMgr.motions[axis].Abs(pos);
                return true;
            }
            catch (System.Exception)
            {
                LogMgr.Info($"{axis}轴 发生错误");
            }
            return false;
        }

        public bool RelMoveHandler(PointName.Axis axis, int pos, int vel, double acc, double dec)
        {
            try
            {
                if (MotionMgr.motions[axis].Run)
                {
                    LogMgr.Info($"{axis}轴 没有停止");
                    return false;
                }
                if (vel > 60)
                {
                    LogMgr.Warning("轴设置速度不能大于60");
                    return false;
                }
                MotionMgr.motions[axis].Vec = vel;
                MotionMgr.motions[axis].Acc = acc;
                MotionMgr.motions[axis].Dec = dec;
                MotionMgr.motions[axis].Rel(pos);
                return true;
            }
            catch (System.Exception)
            {
                LogMgr.Info($"{axis}轴 发生错误");
            }
            return false;
        }

        public bool HomeHandler(PointName.Axis axis)
        {
            try
            {
                if (MotionMgr.motions[axis].Run)
                {
                    LogMgr.Info($"{axis}轴 没有停止");
                    return false;
                }
                MotionMgr.motions[axis].Home();
                return true;
            }
            catch (System.Exception)
            {
                LogMgr.Info($"{axis}轴 发生错误");
            }
            return false;
        }

        public bool ZeroHandler(PointName.Axis axis)
        {
            try
            {
                if (MotionMgr.motions[axis].Run)
                {
                    LogMgr.Info($"{axis}轴 没有停止");
                    return false;
                }
                MotionMgr.motions[axis].Clear();
                MotionMgr.motions[axis].Zero();
                return true;
            }
            catch (System.Exception)
            {
                LogMgr.Info($"{axis}轴 发生错误");
            }
            return false;
        }

        public bool ClearHandler(PointName.Axis axis)
        {
            try
            {
                if (MotionMgr.motions[axis].Run)
                {
                    LogMgr.Info($"{axis}轴 没有停止");
                    return false;
                }
                MotionMgr.motions[axis].Clear();
                return true;
            }
            catch (System.Exception)
            {
                LogMgr.Info($"{axis}轴 发生错误");
            }
            return false;
        }

        public bool JogMoveHandler(PointName.Axis axis, bool pos, int vel, double acc, double dec)
        {
            try
            {
                if (MotionMgr.motions[axis].Run)
                {
                    LogMgr.Info($"{axis}轴 没有停止");
                    return false;
                }
                if (vel > 60)
                {
                    LogMgr.Warning("轴设置速度不能大于60");
                    return false;
                }
                MotionMgr.motions[axis].Vec = vel;
                MotionMgr.motions[axis].Acc = acc;
                MotionMgr.motions[axis].Dec = dec;
                MotionMgr.motions[axis].Jog(pos);
                return true;
            }
            catch (System.Exception)
            {
                LogMgr.Info($"{axis}轴 发生错误");
            }
            return false;
        }

        public bool EnableHandler(PointName.Axis axis, bool enable)
        {
            try
            {
                if (enable)
                {
                    MotionMgr.motions[axis].On();
                }
                else
                {
                    MotionMgr.motions[axis].Off();
                }
                return true;
            }
            catch (System.Exception)
            {
                LogMgr.Info($"{axis}轴 发生错误");
            }
            return false;
        }

        public void StopHandler()
        {
            foreach (KeyValuePair<PointName.Axis, MotionBase> motItem in MotionMgr.motions)
            {
                try
                {
                    motItem.Value.Stop(false);
                }
                catch (System.Exception)
                {
                }
            }
        }

        public void SelectAxis(PointName.Axis axis)
        {
            axisOption.SelectAxis(axis);
        }

        public void SetActive(List<PointName.Axis> axiss)
        {
            axisOption.SetActive(axiss);
        }

        public int GetAxisPos(PointName.Axis axis)
        {
            return axisOption.GetAxisPos(axis);
        }

        public void SetAxisPos(PointName.Axis axis, int pos)
        {
            axisOption.SetAxisPos(axis, pos);
        }
    }
}
