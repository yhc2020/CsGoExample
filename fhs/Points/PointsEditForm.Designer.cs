﻿namespace fhs
{
    partial class PointsEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox_点位 = new System.Windows.Forms.ComboBox();
            this.dataGridView_轴坐标 = new System.Windows.Forms.DataGridView();
            this.button_应用 = new System.Windows.Forms.Button();
            this.button_保存 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.轴名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.坐标 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.速度 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.加速度 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.减速度 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.顺序 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.移动 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.应用 = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_轴坐标)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox_点位
            // 
            this.comboBox_点位.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_点位.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_点位.FormattingEnabled = true;
            this.comboBox_点位.Location = new System.Drawing.Point(41, 2);
            this.comboBox_点位.Name = "comboBox_点位";
            this.comboBox_点位.Size = new System.Drawing.Size(475, 20);
            this.comboBox_点位.TabIndex = 5;
            this.comboBox_点位.SelectedIndexChanged += new System.EventHandler(this.comboBox_点位_SelectedIndexChanged);
            // 
            // dataGridView_轴坐标
            // 
            this.dataGridView_轴坐标.AllowUserToAddRows = false;
            this.dataGridView_轴坐标.AllowUserToDeleteRows = false;
            this.dataGridView_轴坐标.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_轴坐标.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_轴坐标.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.轴名称,
            this.坐标,
            this.速度,
            this.加速度,
            this.减速度,
            this.顺序,
            this.移动,
            this.应用});
            this.dataGridView_轴坐标.Location = new System.Drawing.Point(0, 27);
            this.dataGridView_轴坐标.MultiSelect = false;
            this.dataGridView_轴坐标.Name = "dataGridView_轴坐标";
            this.dataGridView_轴坐标.RowTemplate.Height = 23;
            this.dataGridView_轴坐标.Size = new System.Drawing.Size(516, 272);
            this.dataGridView_轴坐标.TabIndex = 4;
            this.dataGridView_轴坐标.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_轴坐标_CellContentClick);
            this.dataGridView_轴坐标.SelectionChanged += new System.EventHandler(this.dataGridView_轴坐标_SelectionChanged);
            // 
            // button_应用
            // 
            this.button_应用.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_应用.Location = new System.Drawing.Point(-1, 305);
            this.button_应用.Name = "button_应用";
            this.button_应用.Size = new System.Drawing.Size(140, 33);
            this.button_应用.TabIndex = 11;
            this.button_应用.Text = "应用到当前点位";
            this.button_应用.UseVisualStyleBackColor = true;
            this.button_应用.Click += new System.EventHandler(this.button_应用_Click);
            // 
            // button_保存
            // 
            this.button_保存.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_保存.Location = new System.Drawing.Point(377, 305);
            this.button_保存.Name = "button_保存";
            this.button_保存.Size = new System.Drawing.Size(140, 33);
            this.button_保存.TabIndex = 12;
            this.button_保存.Text = "保存当前点位";
            this.button_保存.UseVisualStyleBackColor = true;
            this.button_保存.Click += new System.EventHandler(this.button_保存_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "点位";
            // 
            // 轴名称
            // 
            this.轴名称.HeaderText = "轴名称";
            this.轴名称.Name = "轴名称";
            this.轴名称.ReadOnly = true;
            this.轴名称.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // 坐标
            // 
            this.坐标.HeaderText = "坐标";
            this.坐标.MaxInputLength = 10;
            this.坐标.Name = "坐标";
            this.坐标.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.坐标.Width = 80;
            // 
            // 速度
            // 
            this.速度.HeaderText = "速度";
            this.速度.MaxInputLength = 3;
            this.速度.Name = "速度";
            this.速度.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.速度.Width = 50;
            // 
            // 加速度
            // 
            this.加速度.HeaderText = "加速度";
            this.加速度.MaxInputLength = 3;
            this.加速度.Name = "加速度";
            this.加速度.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.加速度.Width = 50;
            // 
            // 减速度
            // 
            this.减速度.HeaderText = "减速度";
            this.减速度.MaxInputLength = 3;
            this.减速度.Name = "减速度";
            this.减速度.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.减速度.Width = 50;
            // 
            // 顺序
            // 
            this.顺序.HeaderText = "顺序";
            this.顺序.MaxInputLength = 1;
            this.顺序.Name = "顺序";
            this.顺序.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.顺序.Width = 40;
            // 
            // 移动
            // 
            this.移动.HeaderText = "移动";
            this.移动.Name = "移动";
            this.移动.ReadOnly = true;
            this.移动.Width = 50;
            // 
            // 应用
            // 
            this.应用.HeaderText = "应用";
            this.应用.Name = "应用";
            this.应用.Width = 50;
            // 
            // PointsEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 344);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_应用);
            this.Controls.Add(this.button_保存);
            this.Controls.Add(this.comboBox_点位);
            this.Controls.Add(this.dataGridView_轴坐标);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PointsEditForm";
            this.Text = "PointsEditForm";
            this.Load += new System.EventHandler(this.PointsEditForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_轴坐标)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_点位;
        private System.Windows.Forms.DataGridView dataGridView_轴坐标;
        private System.Windows.Forms.Button button_应用;
        private System.Windows.Forms.Button button_保存;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn 轴名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 坐标;
        private System.Windows.Forms.DataGridViewTextBoxColumn 速度;
        private System.Windows.Forms.DataGridViewTextBoxColumn 加速度;
        private System.Windows.Forms.DataGridViewTextBoxColumn 减速度;
        private System.Windows.Forms.DataGridViewTextBoxColumn 顺序;
        private System.Windows.Forms.DataGridViewButtonColumn 移动;
        private System.Windows.Forms.DataGridViewButtonColumn 应用;
    }
}