﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace fhs
{
    class PointMgr
    {
        public class Position : IComparable<Position>
        {
            public PointName.Axis Name;
            public int Pos;
            public int Vec;
            public double Acc;
            public double Dec;
            public int Pri;

            public int CompareTo(Position other)
            {
                if (Pri < other.Pri)
                {
                    return -1;
                }
                else if (Pri > other.Pri)
                {
                    return 1;
                }
                return 0;
            }
        }
        static public readonly Dictionary<PointName.Point, Dictionary<PointName.Axis, Position>> commPoints = new Dictionary<PointName.Point, Dictionary<PointName.Axis, Position>>();
        static public readonly Dictionary<PointName.Point, Dictionary<PointName.Axis, Position>> productPoints = new Dictionary<PointName.Point, Dictionary<PointName.Axis, Position>>();

        static void Load(string path, Dictionary<PointName.Point, Dictionary<PointName.Axis, Position>> points)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(path);
            }
            catch (Exception ec)
            {
                LogMgr.Error($"读取点位异常:{ec.Message}");
                return;
            }
            XmlElement root = doc.DocumentElement;
            foreach (XmlNode pointItem in root.ChildNodes)
            {
                string pointNameStr = ((XmlElement)pointItem).GetAttribute("Name");
                PointName.Point pointName = (PointName.Point)(-1);
                if (StringToEnum.Convert(pointNameStr, ref pointName))
                {
                    if (points.ContainsKey(pointName))
                    {
                        LogMgr.Excep($"初始化 {path}:{pointNameStr} 点位名称重名");
                        continue;
                    }
                    Dictionary<PointName.Axis, Position> point = new Dictionary<PointName.Axis, Position>();
                    foreach (XmlNode axisItem in pointItem.ChildNodes)
                    {
                        string axisNameStr = ((XmlElement)axisItem).GetAttribute("Name");
                        PointName.Axis axisName = (PointName.Axis)(-1);
                        if (StringToEnum.Convert(axisNameStr, ref axisName))
                        {
                            if (point.ContainsKey(axisName))
                            {
                                LogMgr.Excep($"初始化 {path}:{pointNameStr}.{axisNameStr} 轴名称重名");
                                continue;
                            }
                            point[axisName] = new Position
                            {
                                Name = axisName,
                                Pos = int.Parse(((XmlElement)axisItem).GetAttribute("Pos")),
                                Vec = int.Parse(((XmlElement)axisItem).GetAttribute("Vec")),
                                Acc = double.Parse(((XmlElement)axisItem).GetAttribute("Acc")),
                                Dec = double.Parse(((XmlElement)axisItem).GetAttribute("Dec")),
                                Pri = int.Parse(((XmlElement)axisItem).GetAttribute("Pri"))
                            };
                        }
                        else
                        {
                            LogMgr.Excep($"初始化 {path}:{pointNameStr}.{axisNameStr} 轴名称未定义");
                        }
                    }
                    points[pointName] = point;
                }
                else
                {
                    LogMgr.Excep($"初始化 {path}:{pointNameStr} 点位名称未定义");
                }
            }
        }

        static void Save(string path, Dictionary<PointName.Point, Dictionary<PointName.Axis, Position>> points)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("Points");
            doc.AppendChild(root);
            foreach (KeyValuePair<PointName.Point, Dictionary<PointName.Axis, Position>> point in points)
            {
                XmlElement pointEle = doc.CreateElement("Point");
                pointEle.SetAttribute("Name", point.Key.ToString());
                foreach (KeyValuePair<PointName.Axis, Position> axis in point.Value)
                {
                    XmlElement axisEle = doc.CreateElement("Axis");
                    axisEle.SetAttribute("Name", axis.Key.ToString());
                    axisEle.SetAttribute("Pos", axis.Value.Pos.ToString());
                    axisEle.SetAttribute("Vec", axis.Value.Vec.ToString());
                    axisEle.SetAttribute("Acc", axis.Value.Acc.ToString());
                    axisEle.SetAttribute("Dec", axis.Value.Dec.ToString());
                    axisEle.SetAttribute("Pri", axis.Value.Pri.ToString());
                    pointEle.AppendChild(axisEle);
                }
                root.AppendChild(pointEle);
            }
            doc.Save(path);
        }

        static public void Load()
        {
            commPoints.Clear();
            productPoints.Clear();
            try
            {
                Load(@".\PointConfig\Common\Point.xml", commPoints);
                Load($@".\PointConfig\Product\{ProductMgr.currProduct}\Point.xml", productPoints);
            }
            catch (Exception ec)
            {
                LogMgr.Error($@"加载点位数据异常:{ec.Message}");
            }
        }

        static public void Save()
        {
            try
            {
                Save(@".\PointConfig\Common\Point.xml", commPoints);
                Save($@".\PointConfig\Product\{ProductMgr.currProduct}\Point.xml", productPoints);
            }
            catch (Exception ec)
            {
                LogMgr.Error($@"保存点位数据异常:{ec.Message}");
            }
        }
    }
}
