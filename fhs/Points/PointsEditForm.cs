﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    public partial class PointsEditForm : Form
    {
        private PointDebugForm _debugForm;

        public PointsEditForm(PointDebugForm debugForm)
        {
            _debugForm = debugForm;
            InitializeComponent();
        }

        private void PointsEditForm_Load(object sender, EventArgs e)
        {
            ProductMgr.productChangeEvent.AddLast((string _) => ReLoad());
            ReLoad();
        }

        private void ReLoad()
        {
            comboBox_点位.Items.Clear();
            foreach (var item in PointMgr.commPoints)
            {
                comboBox_点位.Items.Add(item.Key.ToString());
            }
            foreach (var item in PointMgr.productPoints)
            {
                comboBox_点位.Items.Add(item.Key.ToString());
            }
            if (0 != comboBox_点位.Items.Count)
            {
                comboBox_点位.SelectedIndex = 0;
            }
        }

        private void comboBox_点位_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView_轴坐标.Rows.Clear();
            List<PointName.Axis> axiss = new List<PointName.Axis>();

            if (comboBox_点位.SelectedIndex < PointMgr.commPoints.Count)
            {
                foreach (var item in PointMgr.commPoints[StringToEnum.Convert<PointName.Point>(comboBox_点位.Text)])
                {
                    DataGridViewRow newRows = new DataGridViewRow();
                    newRows.CreateCells(dataGridView_轴坐标);
                    newRows.Cells[dataGridView_轴坐标.Columns["轴名称"].Index].Value = item.Key.ToString();
                    newRows.Cells[dataGridView_轴坐标.Columns["坐标"].Index].Value = item.Value.Pos.ToString();
                    newRows.Cells[dataGridView_轴坐标.Columns["速度"].Index].Value = item.Value.Vec.ToString();
                    newRows.Cells[dataGridView_轴坐标.Columns["加速度"].Index].Value = item.Value.Acc.ToString();
                    newRows.Cells[dataGridView_轴坐标.Columns["减速度"].Index].Value = item.Value.Dec.ToString();
                    newRows.Cells[dataGridView_轴坐标.Columns["顺序"].Index].Value = item.Value.Pri.ToString();
                    dataGridView_轴坐标.Rows.Add(newRows);
                    axiss.Add(StringToEnum.Convert<PointName.Axis>(item.Key.ToString()));
                }
            }
            else
            {
                foreach (var item in PointMgr.productPoints[StringToEnum.Convert<PointName.Point>(comboBox_点位.Text)])
                {
                    DataGridViewRow newRows = new DataGridViewRow();
                    newRows.CreateCells(dataGridView_轴坐标);
                    newRows.Cells[dataGridView_轴坐标.Columns["轴名称"].Index].Value = item.Key.ToString();
                    newRows.Cells[dataGridView_轴坐标.Columns["坐标"].Index].Value = item.Value.Pos.ToString();
                    newRows.Cells[dataGridView_轴坐标.Columns["速度"].Index].Value = item.Value.Vec.ToString();
                    newRows.Cells[dataGridView_轴坐标.Columns["加速度"].Index].Value = item.Value.Acc.ToString();
                    newRows.Cells[dataGridView_轴坐标.Columns["减速度"].Index].Value = item.Value.Dec.ToString();
                    newRows.Cells[dataGridView_轴坐标.Columns["顺序"].Index].Value = item.Value.Pri.ToString();
                    dataGridView_轴坐标.Rows.Add(newRows);
                    axiss.Add(StringToEnum.Convert<PointName.Axis>(item.Key.ToString()));
                }
            }
            _debugForm.SetActive(axiss);
        }

        private void dataGridView_轴坐标_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            try
            {
                DataGridViewRow selectRow = dataGridView_轴坐标.Rows[e.RowIndex];
                switch (dataGridView_轴坐标.Columns[e.ColumnIndex].Name)
                {
                    case "移动":
                        _debugForm.SetAxisPos(StringToEnum.Convert<PointName.Axis>(selectRow.Cells[dataGridView_轴坐标.Columns["轴名称"].Index].Value.ToString()), int.Parse(selectRow.Cells[dataGridView_轴坐标.Columns["坐标"].Index].Value.ToString()));
                        break;
                    case "应用":
                        selectRow.Cells[dataGridView_轴坐标.Columns["坐标"].Index].Value = _debugForm.GetAxisPos(StringToEnum.Convert<PointName.Axis>(selectRow.Cells[dataGridView_轴坐标.Columns["轴名称"].Index].Value.ToString())).ToString();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button_保存_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<PointName.Axis, PointMgr.Position> selectPoint = null;
                if (comboBox_点位.SelectedIndex < PointMgr.commPoints.Count)
                {
                    selectPoint = PointMgr.commPoints[StringToEnum.Convert<PointName.Point>(comboBox_点位.Text)];
                }
                else
                {
                    selectPoint = PointMgr.productPoints[StringToEnum.Convert<PointName.Point>(comboBox_点位.Text)];
                }
                foreach (DataGridViewRow selectRow in dataGridView_轴坐标.Rows)
                {
                    PointMgr.Position axis = selectPoint[StringToEnum.Convert<PointName.Axis>(selectRow.Cells[dataGridView_轴坐标.Columns["轴名称"].Index].Value.ToString())];
                    axis.Pos = int.Parse(selectRow.Cells[dataGridView_轴坐标.Columns["坐标"].Index].Value.ToString());
                    axis.Vec = int.Parse(selectRow.Cells[dataGridView_轴坐标.Columns["速度"].Index].Value.ToString());
                    axis.Acc = double.Parse(selectRow.Cells[dataGridView_轴坐标.Columns["加速度"].Index].Value.ToString());
                    axis.Dec = double.Parse(selectRow.Cells[dataGridView_轴坐标.Columns["减速度"].Index].Value.ToString());
                    axis.Pri = int.Parse(selectRow.Cells[dataGridView_轴坐标.Columns["顺序"].Index].Value.ToString());
                }
                PointMgr.Save();
            }
            catch (Exception)
            {
                LogMgr.Error($"保存点位 {comboBox_点位.Text} 出错");
            }
        }

        private void button_应用_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow selectRow in dataGridView_轴坐标.Rows)
            {
                PointName.Axis axis = StringToEnum.Convert<PointName.Axis>(selectRow.Cells[dataGridView_轴坐标.Columns["轴名称"].Index].Value.ToString());
                try
                {
                    selectRow.Cells[dataGridView_轴坐标.Columns["坐标"].Index].Value = _debugForm.GetAxisPos(axis).ToString();
                }
                catch (System.Exception)
                {
                    MessageBox.Show($"读取 {axis} 轴数据出错");
                    break;
                }
            }
        }

        private void dataGridView_轴坐标_SelectionChanged(object sender, EventArgs e)
        {
            foreach (var item in  dataGridView_轴坐标.SelectedCells)
            {
                DataGridViewCell selectRow = (DataGridViewCell)item;
                _debugForm.SelectAxis(StringToEnum.Convert<PointName.Axis>(dataGridView_轴坐标.Rows[selectRow.RowIndex].Cells[dataGridView_轴坐标.Columns["轴名称"].Index].Value.ToString()));
            }
        }
    }
}
