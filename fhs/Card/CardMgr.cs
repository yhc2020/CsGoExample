﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace fhs
{
    class CardMgr
    {
        static public readonly Dictionary<string, CardBase> cards = new Dictionary<string, CardBase>();

        static CardBase InitMotion(XmlElement param)
        {
            string brand = param.GetAttribute("Brand");
            switch (brand)
            {
                case "gts":
                    string no = param.GetAttribute("CardNo");
                    return new GtsMotionCard(int.Parse(no));
                default:
                    break;
            }
            return null;
        }

        static public void Load()
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(@".\Config\Card.xml");
            }
            catch (Exception ec)
            {
                LogMgr.Error($"读取Card异常:{ec.Message}");
                return;
            }
            XmlElement root = doc.DocumentElement;
            foreach (XmlNode cardItem in root.ChildNodes)
            {
                string type = ((XmlElement)cardItem).GetAttribute("Type");
                switch (type)
                {
                    case "Motion":
                        {
                            foreach (XmlNode motionItem in cardItem.ChildNodes)
                            {
                                string name = ((XmlElement)motionItem).GetAttribute("Name");
                                if (!cards.ContainsKey(name))
                                {
                                    try
                                    {
                                        cards.Add(name, InitMotion((XmlElement)motionItem));
                                    }
                                    catch (System.Exception ec)
                                    {
                                        LogMgr.Error($"{name}卡初始化失败{ec.Message}");
                                    }
                                }
                                else
                                {
                                    LogMgr.Error($"{name}卡名称重定义");
                                }
                            }
                        }
                        break;
                    case "IO":
                        break;
                    default:
                        LogMgr.Error($"未定义的卡类型{type}");
                        break;
                }
            }
        }
    }
}
