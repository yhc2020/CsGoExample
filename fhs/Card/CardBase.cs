﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    abstract class CardBase
    {
        public enum CardType
        {
            Motion,
            IO,
        }

        protected CardBase()
        {
        }

        public abstract CardType Type { get; }
    }
}
