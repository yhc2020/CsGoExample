﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    abstract class GtsCard : CardBase
    {
        protected int _card;

        protected GtsCard(int card)
        {
            _card = card;
            if (!Init())
            {
                LogMgr.Error($"gts:{_card}初始化异常");
            }
        }

        public bool Init()
        {
            return 0 == gts.mc.GT_Open((short)_card, 1) &&
                0 == gts.mc.GT_Reset() &&
                0 == gts.mc.GT_LoadConfig($".\\Config\\gts{_card}.cfg");
        }

        public bool Close()
        {
            return 0 == gts.mc.GT_Close();
        }
    }
}
