﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    public partial class DebugForm : Form
    {
        VisionDebugForm _cameraDebug;
        VisionCalibForm _visionCalib;
        PointDebugForm _pointDebug;
        ProductForm _productDebug;
        IoDebugForm _ioDebug;

        public DebugForm()
        {
            InitializeComponent();
        }

        private void DebugForm_Load(object sender, EventArgs e)
        {
            {
                _pointDebug = new PointDebugForm();
                _pointDebug.TopLevel = false;
                _pointDebug.Dock = DockStyle.Fill;
                _pointDebug.Visible = true;
                tabPage1.Controls.Add(_pointDebug);
            }
            {
                _ioDebug = new IoDebugForm();
                _ioDebug.TopLevel = false;
                _ioDebug.Dock = DockStyle.Fill;
                _ioDebug.Visible = true;
                tabPage2.Controls.Add(_ioDebug);
            }
            {
                _cameraDebug = new VisionDebugForm();
                _cameraDebug.TopLevel = false;
                _cameraDebug.Dock = DockStyle.Fill;
                _cameraDebug.Visible = true;
                tabPage3.Controls.Add(_cameraDebug);
            }
            {
                _productDebug = new ProductForm();
                _productDebug.TopLevel = false;
                _productDebug.Dock = DockStyle.Fill;
                _productDebug.Visible = true;
                tabPage4.Controls.Add(_productDebug);
            }
            {
                _visionCalib = new VisionCalibForm();
                _visionCalib.TopLevel = false;
                _visionCalib.Dock = DockStyle.Fill;
                _visionCalib.Visible = true;
                tabPage5.Controls.Add(_visionCalib);
            }
        }
    }
}
