﻿namespace fhs
{
    partial class StateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_time = new System.Windows.Forms.Label();
            this.label_stateInfo = new System.Windows.Forms.Label();
            this.pictureBox_stateLight = new System.Windows.Forms.PictureBox();
            this.button_log = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_stateLight)).BeginInit();
            this.SuspendLayout();
            // 
            // label_time
            // 
            this.label_time.AutoSize = true;
            this.label_time.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_time.ForeColor = System.Drawing.Color.Red;
            this.label_time.Location = new System.Drawing.Point(0, 7);
            this.label_time.Name = "label_time";
            this.label_time.Size = new System.Drawing.Size(53, 19);
            this.label_time.TabIndex = 0;
            this.label_time.Text = "time";
            // 
            // label_stateInfo
            // 
            this.label_stateInfo.AutoSize = true;
            this.label_stateInfo.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label_stateInfo.Location = new System.Drawing.Point(269, 7);
            this.label_stateInfo.Name = "label_stateInfo";
            this.label_stateInfo.Size = new System.Drawing.Size(89, 19);
            this.label_stateInfo.TabIndex = 1;
            this.label_stateInfo.Text = "设备停止";
            // 
            // pictureBox_stateLight
            // 
            this.pictureBox_stateLight.Image = global::fhs.Properties.Resources.light_gray;
            this.pictureBox_stateLight.Location = new System.Drawing.Point(229, 0);
            this.pictureBox_stateLight.Name = "pictureBox_stateLight";
            this.pictureBox_stateLight.Size = new System.Drawing.Size(40, 40);
            this.pictureBox_stateLight.TabIndex = 2;
            this.pictureBox_stateLight.TabStop = false;
            // 
            // button_log
            // 
            this.button_log.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_log.Location = new System.Drawing.Point(681, 7);
            this.button_log.Name = "button_log";
            this.button_log.Size = new System.Drawing.Size(75, 23);
            this.button_log.TabIndex = 3;
            this.button_log.Text = "日志";
            this.button_log.UseVisualStyleBackColor = true;
            this.button_log.Click += new System.EventHandler(this.button_log_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(600, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "日志文件";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_logFile_Click);
            // 
            // StateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 44);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button_log);
            this.Controls.Add(this.pictureBox_stateLight);
            this.Controls.Add(this.label_stateInfo);
            this.Controls.Add(this.label_time);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "StateForm";
            this.Text = "StateForm";
            this.Load += new System.EventHandler(this.StateForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_stateLight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_time;
        private System.Windows.Forms.Label label_stateInfo;
        private System.Windows.Forms.PictureBox pictureBox_stateLight;
        private System.Windows.Forms.Button button_log;
        private System.Windows.Forms.Button button1;
    }
}