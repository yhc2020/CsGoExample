﻿namespace fhs
{
    partial class LogViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_logView = new System.Windows.Forms.DataGridView();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.msg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_logView)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_logView
            // 
            this.dataGridView_logView.AllowUserToAddRows = false;
            this.dataGridView_logView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_logView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_logView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.time,
            this.type,
            this.msg});
            this.dataGridView_logView.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_logView.Name = "dataGridView_logView";
            this.dataGridView_logView.ReadOnly = true;
            this.dataGridView_logView.RowTemplate.Height = 23;
            this.dataGridView_logView.Size = new System.Drawing.Size(606, 240);
            this.dataGridView_logView.TabIndex = 0;
            // 
            // time
            // 
            this.time.HeaderText = "时间";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            this.time.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.time.Width = 140;
            // 
            // type
            // 
            this.type.HeaderText = "类型";
            this.type.Name = "type";
            this.type.ReadOnly = true;
            this.type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.type.Width = 60;
            // 
            // msg
            // 
            this.msg.HeaderText = "消息";
            this.msg.Name = "msg";
            this.msg.ReadOnly = true;
            this.msg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.msg.Width = 2000;
            // 
            // LogViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 240);
            this.Controls.Add(this.dataGridView_logView);
            this.Icon = global::fhs.Properties.Resources.msdn;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LogViewForm";
            this.Text = "LogView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LogViewForm_FormClosing);
            this.Load += new System.EventHandler(this.LogViewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_logView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_logView;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn type;
        private System.Windows.Forms.DataGridViewTextBoxColumn msg;
    }
}