﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Go;

namespace fhs
{
    static class Program
    {
        static readonly string _visionPath = functional.init(delegate ()
        {
            try
            {
                return System.IO.File.ReadAllLines("./Config/VisionPath.txt")[0];
            }
            catch (System.Exception) { }
            return @"C:\Windows\System32\vision";
        });
        static internal Version _visionVersion = null;
        static readonly bool _highPartStyle = functional.init(delegate ()
        {
            try
            {
                return bool.Parse(System.IO.File.ReadAllLines("./Config/HighPartStyle.txt")[0]);
            }
            catch (System.Exception) { }
            return false;
        });

        static public string VisionPath
        {
            get
            {
                return _visionPath;
            }
        }

        static public Version VisionVersion
        {
            get
            {
                return _visionVersion;
            }
        }

        static public bool HighPartStyle
        {
            get
            {
                return _highPartStyle;
            }
        }

        static private bool _enumCameras = false;
        static public bool EnumCameras
        {
            get
            {
                return _enumCameras;
            }
        }

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] cmdParam)
        {
            System.Threading.Mutex only = new System.Threading.Mutex(true, "屏幕贴附");
            if (only.WaitOne(0))
            {
                if (cmdParam.Length > 0)
                {
                    if ("enum_cameras" == cmdParam[0])
                    {
                        _enumCameras = true;
                    }
                }
                string visionPath = VisionPath ?? "";
                Environment.SetEnvironmentVariable("PATH", $@"{visionPath};{visionPath}\genicam;{Environment.GetEnvironmentVariable("PATH")}");
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
                only.ReleaseMutex();
            }
            else
            {
                MessageBox.Show("已打开");
            }
        }
    }
}
