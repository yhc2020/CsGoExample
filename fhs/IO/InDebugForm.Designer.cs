﻿namespace fhs
{
    partial class InDebugForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_输入 = new System.Windows.Forms.DataGridView();
            this.点名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.状态 = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_输入)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_输入
            // 
            this.dataGridView_输入.AllowUserToAddRows = false;
            this.dataGridView_输入.AllowUserToDeleteRows = false;
            this.dataGridView_输入.AllowUserToResizeRows = false;
            this.dataGridView_输入.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_输入.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_输入.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.点名称,
            this.状态});
            this.dataGridView_输入.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_输入.Name = "dataGridView_输入";
            this.dataGridView_输入.RowTemplate.Height = 23;
            this.dataGridView_输入.Size = new System.Drawing.Size(244, 322);
            this.dataGridView_输入.TabIndex = 0;
            // 
            // 点名称
            // 
            this.点名称.HeaderText = "点名称";
            this.点名称.Name = "点名称";
            this.点名称.ReadOnly = true;
            this.点名称.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // 状态
            // 
            this.状态.HeaderText = "状态";
            this.状态.Image = global::fhs.Properties.Resources.light_gray;
            this.状态.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.状态.Name = "状态";
            this.状态.ReadOnly = true;
            // 
            // InDebugForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(245, 323);
            this.Controls.Add(this.dataGridView_输入);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "InDebugForm";
            this.Text = "InDebugForm";
            this.Load += new System.EventHandler(this.InDebugForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_输入)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_输入;
        private System.Windows.Forms.DataGridViewTextBoxColumn 点名称;
        private System.Windows.Forms.DataGridViewImageColumn 状态;
    }
}