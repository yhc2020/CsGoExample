﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    public partial class IoDebugForm : Form
    {
        InDebugForm inDebug;
        OutDebugForm outDebug;

        public IoDebugForm()
        {
            InitializeComponent();
        }

        private void IoDebugForm_Load(object sender, EventArgs e)
        {
            {
                inDebug = new InDebugForm();
                inDebug.TopLevel = false;
                inDebug.Dock = DockStyle.Fill;
                inDebug.Visible = true;
                panel1.Controls.Add(inDebug);
            }
            {
                outDebug = new OutDebugForm();
                outDebug.TopLevel = false;
                outDebug.Dock = DockStyle.Fill;
                outDebug.Visible = true;
                panel2.Controls.Add(outDebug);
            }
        }
    }
}
