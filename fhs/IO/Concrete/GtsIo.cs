﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    class GtsIn : InBase
    {
        GtsMotionCard _card;
        int _no;

        public GtsIn(GtsCard card, int no, IoName.In name, bool not) : base(name, not)
        {
            _card = (GtsMotionCard)card;
            _no = no;
        }

        public override State Stat
        {
            get
            {
                int state;
                if (!_card.GetIn(out state))
                {
                    throw new InException { error = Error.未知错误 };
                }
                return 0 != (state & (1 << _no)) ? State.高 : State.低;
            }
        }
    }

    class GtsOut : OutBase
    {
        GtsMotionCard _card;
        int _no;

        public GtsOut(GtsCard card, int no, IoName.Out name, bool not) : base(name, not)
        {
            _card = (GtsMotionCard)card;
            _no = no;
        }

        public override State Stat
        {
            get
            {
                int state;
                if (!_card.GetOut(out state))
                {
                    throw new OutException { error = Error.未知错误 };
                }
                return 0 != (state & (1 << _no)) ? State.高 : State.低;
            }
            set
            {
                switch (value)
                {
                    case State.高:
                        if (!_card.SetDo(_no, true))
                        {
                            throw new OutException { error = Error.未知错误 };
                        }
                        break;
                    case State.低:
                        if (!_card.SetDo(_no, false))
                        {
                            throw new OutException { error = Error.未知错误 };
                        }
                        break;
                    default:
                        throw new OutException { error = Error.未知错误 };
                }
            }
        }
    }
}
