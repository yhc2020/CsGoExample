﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace fhs
{
    class IoMgr
    {
        static public Dictionary<IoName.In, InBase> Ins = new Dictionary<IoName.In, InBase>();
        static public Dictionary<IoName.Out, OutBase> Outs = new Dictionary<IoName.Out, OutBase>();

        static public void Load()
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(@".\Config\Io.xml");
            }
            catch (Exception ec)
            {
                LogMgr.Error($"读取IO异常:{ec.Message}");
                return;
            }
            XmlElement root = doc.DocumentElement;
            foreach (XmlNode ioItem in root.ChildNodes)
            {
                string ioName = ((XmlElement)ioItem).GetAttribute("Name");
                string brand = ((XmlElement)ioItem).GetAttribute("Brand");
                if ("In" == ioItem.Name)
                {
                    IoName.In ioIn = (IoName.In)(-1);
                    if (!StringToEnum.Convert(ioName, ref ioIn))
                    {
                        LogMgr.Error($"{ioName} In名称未定义");
                        continue;
                    }
                    if (!Ins.ContainsKey(ioIn))
                    {
                        switch (brand.ToLower())
                        {
                            case "gts":
                                {
                                    string card = ((XmlElement)ioItem).GetAttribute("Card");
                                    string no = ((XmlElement)ioItem).GetAttribute("No");
                                    string not = ((XmlElement)ioItem).GetAttribute("Not");
                                    if (CardMgr.cards.ContainsKey(card))
                                    {
                                        try
                                        {
                                            GtsCard gtsCard = (GtsCard)CardMgr.cards[card];
                                            Ins[ioIn] = new GtsIn(gtsCard, int.Parse(no), ioIn, "true" == not.ToLower());
                                        }
                                        catch (System.Exception ec)
                                        {
                                            LogMgr.Error($"IO初始化失败{card}:{no} {ec.Message}");
                                        }
                                    }
                                    else
                                    {
                                        LogMgr.Error($"{card}卡未定义");
                                    }
                                }
                                break;
                            default:
                                LogMgr.Error($"未定义的IO品牌 {brand}");
                                break;
                        }
                    }
                    else
                    {
                        LogMgr.Error($"{ioName}In名称重定义");
                    }
                }
                else if ("Out" == ioItem.Name)
                {
                    IoName.Out ioOut = (IoName.Out)(-1);
                    if (!StringToEnum.Convert(ioName, ref ioOut))
                    {
                        LogMgr.Error($"{ioName} Out名称未定义");
                        continue;
                    }
                    if (!Outs.ContainsKey(ioOut))
                    {
                        switch (brand.ToLower())
                        {
                            case "gts":
                                {
                                    string card = ((XmlElement)ioItem).GetAttribute("Card");
                                    string no = ((XmlElement)ioItem).GetAttribute("No");
                                    string not = ((XmlElement)ioItem).GetAttribute("Not");
                                    if (CardMgr.cards.ContainsKey(card))
                                    {
                                        try
                                        {
                                            GtsCard gtsCard = (GtsCard)CardMgr.cards[card];
                                            Outs[ioOut] = new GtsOut(gtsCard, int.Parse(no), ioOut, "true" == not.ToLower());
                                        }
                                        catch (System.Exception ec)
                                        {
                                            LogMgr.Error($"IO初始化失败{card}:{no} {ec.Message}");
                                        }
                                    }
                                    else
                                    {
                                        LogMgr.Error($"{card}卡未定义");
                                    }
                                }
                                break;
                            default:
                                LogMgr.Error($"未定义的IO品牌 {brand}");
                                break;
                        }
                    }
                    else
                    {
                        LogMgr.Error($"{ioName}Out名称重定义");
                    }
                }
            }
        }
    }
}
