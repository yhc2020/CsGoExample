﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Go;

namespace fhs
{
    public partial class InDebugForm : Form
    {
        generator _refreshAction;

        public InDebugForm()
        {
            InitializeComponent();
        }

        private void InDebugForm_Load(object sender, EventArgs e)
        {
            int ptIdx = dataGridView_输入.Columns["点名称"].Index;
            int stIdx = dataGridView_输入.Columns["状态"].Index;
            foreach (var item in Enum.GetValues(typeof(IoName.In)))
            {
                DataGridViewRow newRows = new DataGridViewRow();
                newRows.CreateCells(dataGridView_输入);
                newRows.Cells[ptIdx].Value = item.ToString();
                newRows.Cells[stIdx].Value = Properties.Resources.light_gray;
                dataGridView_输入.Rows.Add(newRows);
            }
            _refreshAction = generator.tgo(GlobalData.mainStrand, RefreshIoState);
        }

        private async Task RefreshIoState()
        {
            int ptIdx = dataGridView_输入.Columns["点名称"].Index;
            int stIdx = dataGridView_输入.Columns["状态"].Index;
            while (true)
            {
                foreach (DataGridViewRow selectRow in dataGridView_输入.Rows)
                {
                    try
                    {
                        string name = selectRow.Cells[ptIdx].Value.ToString();
                        if (InBase.State.高 == IoMgr.Ins[StringToEnum.Convert<IoName.In>(name)].Stat)
                        {
                            selectRow.Cells[stIdx].Value = Properties.Resources.light_green;
                        }
                        else
                        {
                            selectRow.Cells[stIdx].Value = Properties.Resources.light_gray;
                        }
                    }
                    catch (System.Exception)
                    {

                    }
                    await generator.sleep(1);
                }
                await generator.sleep(100);
            }
        }
    }
}
