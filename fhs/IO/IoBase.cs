﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    abstract class InBase
    {
        public enum State
        {
            未准备,
            高,
            低,
            出错,
        }

        public enum Error
        {
            成功,
            未知错误
        }

        public class InException : System.Exception
        {
            public Error error;
        }

        readonly IoName.In _name;
        readonly bool _not;

        protected InBase(IoName.In name, bool not)
        {
            _name = name;
            _not = not;
        }

        public IoName.In Name
        {
            get
            {
                return _name;
            }
        }

        public bool Not
        {
            get
            {
                return _not;
            }
        }

        public bool High
        {
            get
            {
                switch (Stat)
                {
                    case State.高:
                        return true;
                    case State.低:
                        return false;
                }
                throw new InException();
            }
        }

        public bool Low
        {
            get
            {
                switch (Stat)
                {
                    case State.高:
                        return false;
                    case State.低:
                        return true;
                }
                throw new InException();
            }
        }

        public abstract State Stat { get; }
    }

    abstract class OutBase
    {
        public enum State
        {
            未准备,
            高,
            低,
            出错,
        }

        public enum Error
        {
            成功,
            未知错误
        }

        public class OutException : System.Exception
        {
            public Error error;
        }

        readonly IoName.Out _name;
        readonly bool _not;

        protected OutBase(IoName.Out name, bool not)
        {
            _name = name;
            _not = not;
        }

        public IoName.Out Name
        {
            get
            {
                return _name;
            }
        }

        public bool Not
        {
            get
            {
                return _not;
            }
        }

        public bool High
        {
            get
            {
                switch (Stat)
                {
                    case State.高:
                        return true;
                    case State.低:
                        return false;
                }
                throw new OutException();
            }
            set
            {
                Stat = value ? State.高 : State.低;
            }
        }

        public bool Low
        {
            get
            {
                switch (Stat)
                {
                    case State.高:
                        return false;
                    case State.低:
                        return true;
                }
                throw new OutException();
            }
            set
            {
                Stat = value ? State.低 : State.高;
            }
        }

        public abstract State Stat { get; set; }
    }
}
