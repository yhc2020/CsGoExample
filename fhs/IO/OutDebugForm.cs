﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Go;

namespace fhs
{
    public partial class OutDebugForm : Form
    {
        Dictionary<IoName.Out, bool> _ioDict = new Dictionary<IoName.Out, bool>();

        public OutDebugForm()
        {
            InitializeComponent();
        }

        private void InDebugForm_Load(object sender, EventArgs e)
        {
            int ptIdx = dataGridView_输出.Columns["点名称"].Index;
            int stIdx = dataGridView_输出.Columns["状态"].Index;
            foreach (var item in Enum.GetValues(typeof(IoName.Out)))
            {
                DataGridViewRow newRows = new DataGridViewRow();
                newRows.CreateCells(dataGridView_输出);
                newRows.Cells[ptIdx].Value = item.ToString();
                newRows.Cells[stIdx].Value = Properties.Resources.light_gray;
                dataGridView_输出.Rows.Add(newRows);
                _ioDict[(IoName.Out)item] = false;
            }
        }

        private void dataGridView_输出_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView_输出.Columns[e.ColumnIndex].Name == "状态")
            {
                int ptIdx = dataGridView_输出.Columns["点名称"].Index;
                int stIdx = dataGridView_输出.Columns["状态"].Index;
                DataGridViewRow selectRow = dataGridView_输出.Rows[e.RowIndex];
                try
                {
                    IoName.Out name = StringToEnum.Convert<IoName.Out>(selectRow.Cells[ptIdx].Value.ToString());
                    if (_ioDict[name])
                    {
                        _ioDict[name] = false;
                        selectRow.Cells[stIdx].Value = Properties.Resources.light_gray;
                        IoMgr.Outs[name].Stat = OutBase.State.低;
                    }
                    else
                    {
                        _ioDict[name] = true;
                        selectRow.Cells[stIdx].Value = Properties.Resources.light_green;
                        IoMgr.Outs[name].Stat = OutBase.State.高;
                    }
                }
                catch (System.Exception)
                {
                	
                }
            }
        }
    }
}
