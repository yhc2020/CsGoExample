﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    public partial class LogViewForm : Form
    {
        static public LogViewForm obj;

        public LogViewForm()
        {
            obj = this;
            InitializeComponent();
            for (int i = 0; i < 100; i++)
            {
                DataGridViewRow newRows = new DataGridViewRow();
                newRows.CreateCells(dataGridView_logView);
                dataGridView_logView.Rows.Add(newRows);
            }
        }

        private void LogViewForm_Load(object sender, EventArgs e)
        {

        }

        public void Append(LogMgr.LogPack log)
        {
            GlobalData.mainStrand.dispatch(delegate ()
            {
                if (dataGridView_logView.RowCount > 99)
                {
                    dataGridView_logView.Rows.RemoveAt(99);
                }
                DataGridViewRow newRows = new DataGridViewRow();
                newRows.CreateCells(dataGridView_logView);
                newRows.Cells[dataGridView_logView.Columns["time"].Index].Value = log.time.ToString("yy-MM-dd HH.mm.ss.fff");
                newRows.Cells[dataGridView_logView.Columns["type"].Index].Value = log.type.ToString();
                newRows.Cells[dataGridView_logView.Columns["msg"].Index].Value = log.msg;
                dataGridView_logView.Rows.Insert(0, newRows);
            });
        }

        private void LogViewForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }
    }
}
