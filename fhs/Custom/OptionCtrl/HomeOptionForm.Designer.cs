﻿namespace fhs
{
    partial class HomeOptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_MCState = new System.Windows.Forms.Button();
            this.button_ResetCamera = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_MCState
            // 
            this.button_MCState.Location = new System.Drawing.Point(12, 12);
            this.button_MCState.Name = "button_MCState";
            this.button_MCState.Size = new System.Drawing.Size(123, 23);
            this.button_MCState.TabIndex = 3;
            this.button_MCState.Text = "PLC指令监控";
            this.button_MCState.UseVisualStyleBackColor = true;
            this.button_MCState.Click += new System.EventHandler(this.button_MCState_Click);
            // 
            // button_ResetCamera
            // 
            this.button_ResetCamera.Location = new System.Drawing.Point(12, 42);
            this.button_ResetCamera.Name = "button_ResetCamera";
            this.button_ResetCamera.Size = new System.Drawing.Size(123, 23);
            this.button_ResetCamera.TabIndex = 4;
            this.button_ResetCamera.Text = "重置相机";
            this.button_ResetCamera.UseVisualStyleBackColor = true;
            this.button_ResetCamera.Click += new System.EventHandler(this.button_ResetCamera_Click);
            // 
            // HomeOptionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 265);
            this.Controls.Add(this.button_ResetCamera);
            this.Controls.Add(this.button_MCState);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HomeOptionForm";
            this.Text = "HomeOptionForm";
            this.Load += new System.EventHandler(this.HomeOptionForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_MCState;
        private System.Windows.Forms.Button button_ResetCamera;
    }
}