﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Go;

namespace fhs
{
    public partial class HomeOptionForm : Form
    {
        static HomeOptionForm obj;
        CmdViewForm mcView;

        public HomeOptionForm()
        {
            InitializeComponent();
            obj = this;
        }

        static public void Reset()
        {
        }

        private void HomeOptionForm_Load(object sender, EventArgs e)
        {
            mcView = new CmdViewForm();
        }

        private void button_MCState_Click(object sender, EventArgs e)
        {
            mcView.Activate();
            mcView.Show();
        }

        private void button_ResetCamera_Click(object sender, EventArgs e)
        {
            button_ResetCamera.Enabled = false;
            LogMgr.Info("重置相机");
            generator resetCamera;
            WaitAction.Run(out resetCamera, async delegate ()
            {
                await generator.send_task(delegate ()
                {
                    foreach (var item in CameraMgr.cameras)
                    {
                        item.Value.Reset();
                        if (!item.Value.Opened)
                        {
                            LogMgr.Error($"{item.Key}重置失败");
                        }
                    }
                });
                LogMgr.Info("重置相机完成");
                button_ResetCamera.Enabled = true;
            });
        }
    }
}
