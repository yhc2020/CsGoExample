﻿namespace fhs
{
    partial class VisionParamForm_Mark
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_DrawMark = new System.Windows.Forms.Button();
            this.checkBox_Refresh = new System.Windows.Forms.CheckBox();
            this.comboBox_Mark = new System.Windows.Forms.ComboBox();
            this.button_SelectROI = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDown_ROIPhi = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ROILen2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ROILen1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ROICol = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ROIRow = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.numericUpDown_ExposureTime = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown_Score = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_OffsetRow = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown_OffsetCol = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton_NCC = new System.Windows.Forms.RadioButton();
            this.radioButton_Sharp = new System.Windows.Forms.RadioButton();
            this.groupBox_EnableLine = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButton_ShuNegative = new System.Windows.Forms.RadioButton();
            this.radioButton_ShuPositive = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioButton_HengNegative = new System.Windows.Forms.RadioButton();
            this.radioButton_HengPositive = new System.Windows.Forms.RadioButton();
            this.checkBox_MetLine = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button_SelectShu = new System.Windows.Forms.Button();
            this.button_SelectHeng = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.numericUpDown_Threshold = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Sigma = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_DeltaAngle = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ShuPhi = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_HengPhi = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ShuLen2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_HengLen2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ShuLen1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_HengLen1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ShuCol = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ShuRow = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_HengCol = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_HengRow = new System.Windows.Forms.NumericUpDown();
            this.checkBox_EnableLine = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ROIPhi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ROILen2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ROILen1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ROICol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ROIRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ExposureTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Score)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_OffsetRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_OffsetCol)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox_EnableLine.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Threshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Sigma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_DeltaAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuPhi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengPhi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuLen2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengLen2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuLen1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengLen1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengRow)).BeginInit();
            this.SuspendLayout();
            // 
            // button_DrawMark
            // 
            this.button_DrawMark.Location = new System.Drawing.Point(150, 137);
            this.button_DrawMark.Name = "button_DrawMark";
            this.button_DrawMark.Size = new System.Drawing.Size(75, 23);
            this.button_DrawMark.TabIndex = 76;
            this.button_DrawMark.Text = "框选Mark";
            this.button_DrawMark.UseVisualStyleBackColor = true;
            this.button_DrawMark.Click += new System.EventHandler(this.button_DrawMark_Click);
            // 
            // checkBox_Refresh
            // 
            this.checkBox_Refresh.AutoSize = true;
            this.checkBox_Refresh.Location = new System.Drawing.Point(150, 15);
            this.checkBox_Refresh.Name = "checkBox_Refresh";
            this.checkBox_Refresh.Size = new System.Drawing.Size(48, 16);
            this.checkBox_Refresh.TabIndex = 75;
            this.checkBox_Refresh.Text = "刷新";
            this.checkBox_Refresh.UseVisualStyleBackColor = true;
            // 
            // comboBox_Mark
            // 
            this.comboBox_Mark.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Mark.FormattingEnabled = true;
            this.comboBox_Mark.Location = new System.Drawing.Point(12, 12);
            this.comboBox_Mark.Name = "comboBox_Mark";
            this.comboBox_Mark.Size = new System.Drawing.Size(121, 20);
            this.comboBox_Mark.TabIndex = 74;
            this.comboBox_Mark.SelectedIndexChanged += new System.EventHandler(this.comboBox_Mark_SelectedIndexChanged);
            // 
            // button_SelectROI
            // 
            this.button_SelectROI.Location = new System.Drawing.Point(150, 88);
            this.button_SelectROI.Name = "button_SelectROI";
            this.button_SelectROI.Size = new System.Drawing.Size(75, 23);
            this.button_SelectROI.TabIndex = 73;
            this.button_SelectROI.Text = "ROI框选";
            this.button_SelectROI.UseVisualStyleBackColor = true;
            this.button_SelectROI.Click += new System.EventHandler(this.button_SelectROI_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 92);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 12);
            this.label11.TabIndex = 67;
            this.label11.Text = "Phi";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(121, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 65;
            this.label10.Text = "Len2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 12);
            this.label9.TabIndex = 64;
            this.label9.Text = "Len1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(121, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 12);
            this.label8.TabIndex = 66;
            this.label8.Text = "Col";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 12);
            this.label7.TabIndex = 63;
            this.label7.Text = "Row";
            // 
            // numericUpDown_ROIPhi
            // 
            this.numericUpDown_ROIPhi.DecimalPlaces = 1;
            this.numericUpDown_ROIPhi.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_ROIPhi.Location = new System.Drawing.Point(40, 89);
            this.numericUpDown_ROIPhi.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDown_ROIPhi.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDown_ROIPhi.Name = "numericUpDown_ROIPhi";
            this.numericUpDown_ROIPhi.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ROIPhi.TabIndex = 72;
            this.numericUpDown_ROIPhi.ValueChanged += new System.EventHandler(this.numericUpDown_ROIPhi_ValueChanged);
            // 
            // numericUpDown_ROILen2
            // 
            this.numericUpDown_ROILen2.Location = new System.Drawing.Point(150, 65);
            this.numericUpDown_ROILen2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ROILen2.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_ROILen2.Name = "numericUpDown_ROILen2";
            this.numericUpDown_ROILen2.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ROILen2.TabIndex = 71;
            this.numericUpDown_ROILen2.ValueChanged += new System.EventHandler(this.numericUpDown_ROILen2_ValueChanged);
            // 
            // numericUpDown_ROILen1
            // 
            this.numericUpDown_ROILen1.Location = new System.Drawing.Point(40, 65);
            this.numericUpDown_ROILen1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ROILen1.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_ROILen1.Name = "numericUpDown_ROILen1";
            this.numericUpDown_ROILen1.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ROILen1.TabIndex = 70;
            this.numericUpDown_ROILen1.ValueChanged += new System.EventHandler(this.numericUpDown_ROILen1_ValueChanged);
            // 
            // numericUpDown_ROICol
            // 
            this.numericUpDown_ROICol.Location = new System.Drawing.Point(150, 41);
            this.numericUpDown_ROICol.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ROICol.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_ROICol.Name = "numericUpDown_ROICol";
            this.numericUpDown_ROICol.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ROICol.TabIndex = 69;
            this.numericUpDown_ROICol.ValueChanged += new System.EventHandler(this.numericUpDown_ROICol_ValueChanged);
            // 
            // numericUpDown_ROIRow
            // 
            this.numericUpDown_ROIRow.Location = new System.Drawing.Point(40, 41);
            this.numericUpDown_ROIRow.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ROIRow.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_ROIRow.Name = "numericUpDown_ROIRow";
            this.numericUpDown_ROIRow.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ROIRow.TabIndex = 68;
            this.numericUpDown_ROIRow.ValueChanged += new System.EventHandler(this.numericUpDown_ROIRow_ValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(11, 116);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 12);
            this.label17.TabIndex = 77;
            this.label17.Text = "匹配";
            // 
            // numericUpDown_ExposureTime
            // 
            this.numericUpDown_ExposureTime.Location = new System.Drawing.Point(150, 113);
            this.numericUpDown_ExposureTime.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_ExposureTime.Name = "numericUpDown_ExposureTime";
            this.numericUpDown_ExposureTime.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ExposureTime.TabIndex = 80;
            this.numericUpDown_ExposureTime.ValueChanged += new System.EventHandler(this.numericUpDown_ExposureTime_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(121, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 78;
            this.label6.Text = "曝光";
            // 
            // numericUpDown_Score
            // 
            this.numericUpDown_Score.DecimalPlaces = 2;
            this.numericUpDown_Score.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDown_Score.Location = new System.Drawing.Point(40, 113);
            this.numericUpDown_Score.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_Score.Name = "numericUpDown_Score";
            this.numericUpDown_Score.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_Score.TabIndex = 79;
            this.numericUpDown_Score.ValueChanged += new System.EventHandler(this.numericUpDown_Score_ValueChanged);
            // 
            // numericUpDown_OffsetRow
            // 
            this.numericUpDown_OffsetRow.Location = new System.Drawing.Point(58, 137);
            this.numericUpDown_OffsetRow.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDown_OffsetRow.Name = "numericUpDown_OffsetRow";
            this.numericUpDown_OffsetRow.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_OffsetRow.TabIndex = 81;
            this.numericUpDown_OffsetRow.ValueChanged += new System.EventHandler(this.numericUpDown_OffsetRow_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 77;
            this.label1.Text = "偏移Row";
            // 
            // numericUpDown_OffsetCol
            // 
            this.numericUpDown_OffsetCol.Location = new System.Drawing.Point(58, 161);
            this.numericUpDown_OffsetCol.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDown_OffsetCol.Name = "numericUpDown_OffsetCol";
            this.numericUpDown_OffsetCol.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_OffsetCol.TabIndex = 81;
            this.numericUpDown_OffsetCol.ValueChanged += new System.EventHandler(this.numericUpDown_OffsetCol_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 164);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 77;
            this.label2.Text = "偏移Col";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton_NCC);
            this.panel1.Controls.Add(this.radioButton_Sharp);
            this.panel1.Location = new System.Drawing.Point(13, 188);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(212, 21);
            this.panel1.TabIndex = 83;
            // 
            // radioButton_NCC
            // 
            this.radioButton_NCC.AutoSize = true;
            this.radioButton_NCC.Location = new System.Drawing.Point(102, 4);
            this.radioButton_NCC.Name = "radioButton_NCC";
            this.radioButton_NCC.Size = new System.Drawing.Size(107, 16);
            this.radioButton_NCC.TabIndex = 0;
            this.radioButton_NCC.Text = "基于灰度相关性";
            this.radioButton_NCC.UseVisualStyleBackColor = true;
            this.radioButton_NCC.CheckedChanged += new System.EventHandler(this.radioButton_NCC_CheckedChanged);
            // 
            // radioButton_Sharp
            // 
            this.radioButton_Sharp.AutoSize = true;
            this.radioButton_Sharp.Checked = true;
            this.radioButton_Sharp.Location = new System.Drawing.Point(3, 4);
            this.radioButton_Sharp.Name = "radioButton_Sharp";
            this.radioButton_Sharp.Size = new System.Drawing.Size(95, 16);
            this.radioButton_Sharp.TabIndex = 0;
            this.radioButton_Sharp.TabStop = true;
            this.radioButton_Sharp.Text = "基于轮廓形状";
            this.radioButton_Sharp.UseVisualStyleBackColor = true;
            this.radioButton_Sharp.CheckedChanged += new System.EventHandler(this.radioButton_Sharp_CheckedChanged);
            // 
            // groupBox_EnableLine
            // 
            this.groupBox_EnableLine.Controls.Add(this.panel3);
            this.groupBox_EnableLine.Controls.Add(this.panel2);
            this.groupBox_EnableLine.Controls.Add(this.checkBox_MetLine);
            this.groupBox_EnableLine.Controls.Add(this.label19);
            this.groupBox_EnableLine.Controls.Add(this.label18);
            this.groupBox_EnableLine.Controls.Add(this.label3);
            this.groupBox_EnableLine.Controls.Add(this.button_SelectShu);
            this.groupBox_EnableLine.Controls.Add(this.button_SelectHeng);
            this.groupBox_EnableLine.Controls.Add(this.label16);
            this.groupBox_EnableLine.Controls.Add(this.label4);
            this.groupBox_EnableLine.Controls.Add(this.label15);
            this.groupBox_EnableLine.Controls.Add(this.label5);
            this.groupBox_EnableLine.Controls.Add(this.label14);
            this.groupBox_EnableLine.Controls.Add(this.label12);
            this.groupBox_EnableLine.Controls.Add(this.label13);
            this.groupBox_EnableLine.Controls.Add(this.label20);
            this.groupBox_EnableLine.Controls.Add(this.label21);
            this.groupBox_EnableLine.Controls.Add(this.label22);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_Threshold);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_Sigma);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_DeltaAngle);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_ShuPhi);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_HengPhi);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_ShuLen2);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_HengLen2);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_ShuLen1);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_HengLen1);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_ShuCol);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_ShuRow);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_HengCol);
            this.groupBox_EnableLine.Controls.Add(this.numericUpDown_HengRow);
            this.groupBox_EnableLine.Location = new System.Drawing.Point(9, 227);
            this.groupBox_EnableLine.Name = "groupBox_EnableLine";
            this.groupBox_EnableLine.Size = new System.Drawing.Size(223, 267);
            this.groupBox_EnableLine.TabIndex = 84;
            this.groupBox_EnableLine.TabStop = false;
            this.groupBox_EnableLine.Text = "根据Mark找线";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radioButton_ShuNegative);
            this.panel3.Controls.Add(this.radioButton_ShuPositive);
            this.panel3.Location = new System.Drawing.Point(4, 190);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(132, 23);
            this.panel3.TabIndex = 89;
            // 
            // radioButton_ShuNegative
            // 
            this.radioButton_ShuNegative.AutoSize = true;
            this.radioButton_ShuNegative.Location = new System.Drawing.Point(69, 4);
            this.radioButton_ShuNegative.Name = "radioButton_ShuNegative";
            this.radioButton_ShuNegative.Size = new System.Drawing.Size(59, 16);
            this.radioButton_ShuNegative.TabIndex = 2;
            this.radioButton_ShuNegative.TabStop = true;
            this.radioButton_ShuNegative.Text = "白到黑";
            this.radioButton_ShuNegative.UseVisualStyleBackColor = true;
            this.radioButton_ShuNegative.CheckedChanged += new System.EventHandler(this.radioButton_ShuNegative_CheckedChanged);
            // 
            // radioButton_ShuPositive
            // 
            this.radioButton_ShuPositive.AutoSize = true;
            this.radioButton_ShuPositive.Location = new System.Drawing.Point(4, 4);
            this.radioButton_ShuPositive.Name = "radioButton_ShuPositive";
            this.radioButton_ShuPositive.Size = new System.Drawing.Size(59, 16);
            this.radioButton_ShuPositive.TabIndex = 1;
            this.radioButton_ShuPositive.TabStop = true;
            this.radioButton_ShuPositive.Text = "黑到白";
            this.radioButton_ShuPositive.UseVisualStyleBackColor = true;
            this.radioButton_ShuPositive.CheckedChanged += new System.EventHandler(this.radioButton_ShuPositive_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radioButton_HengNegative);
            this.panel2.Controls.Add(this.radioButton_HengPositive);
            this.panel2.Location = new System.Drawing.Point(4, 92);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(132, 23);
            this.panel2.TabIndex = 88;
            // 
            // radioButton_HengNegative
            // 
            this.radioButton_HengNegative.AutoSize = true;
            this.radioButton_HengNegative.Location = new System.Drawing.Point(69, 4);
            this.radioButton_HengNegative.Name = "radioButton_HengNegative";
            this.radioButton_HengNegative.Size = new System.Drawing.Size(59, 16);
            this.radioButton_HengNegative.TabIndex = 2;
            this.radioButton_HengNegative.TabStop = true;
            this.radioButton_HengNegative.Text = "白到黑";
            this.radioButton_HengNegative.UseVisualStyleBackColor = true;
            this.radioButton_HengNegative.CheckedChanged += new System.EventHandler(this.radioButton_HengNegative_CheckedChanged);
            // 
            // radioButton_HengPositive
            // 
            this.radioButton_HengPositive.AutoSize = true;
            this.radioButton_HengPositive.Location = new System.Drawing.Point(4, 4);
            this.radioButton_HengPositive.Name = "radioButton_HengPositive";
            this.radioButton_HengPositive.Size = new System.Drawing.Size(59, 16);
            this.radioButton_HengPositive.TabIndex = 1;
            this.radioButton_HengPositive.TabStop = true;
            this.radioButton_HengPositive.Text = "黑到白";
            this.radioButton_HengPositive.UseVisualStyleBackColor = true;
            this.radioButton_HengPositive.CheckedChanged += new System.EventHandler(this.radioButton_HengPositive_CheckedChanged);
            // 
            // checkBox_MetLine
            // 
            this.checkBox_MetLine.AutoSize = true;
            this.checkBox_MetLine.Location = new System.Drawing.Point(141, 242);
            this.checkBox_MetLine.Name = "checkBox_MetLine";
            this.checkBox_MetLine.Size = new System.Drawing.Size(72, 16);
            this.checkBox_MetLine.TabIndex = 86;
            this.checkBox_MetLine.Text = "过滤杂点";
            this.checkBox_MetLine.UseVisualStyleBackColor = true;
            this.checkBox_MetLine.CheckedChanged += new System.EventHandler(this.checkBox_MetLine_CheckedChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(2, 219);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 12);
            this.label19.TabIndex = 46;
            this.label19.Text = "阈值";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(112, 219);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 12);
            this.label18.TabIndex = 57;
            this.label18.Text = "过滤";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 243);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 56;
            this.label3.Text = "角差";
            // 
            // button_SelectShu
            // 
            this.button_SelectShu.Location = new System.Drawing.Point(141, 165);
            this.button_SelectShu.Name = "button_SelectShu";
            this.button_SelectShu.Size = new System.Drawing.Size(75, 23);
            this.button_SelectShu.TabIndex = 70;
            this.button_SelectShu.Text = "竖线框选";
            this.button_SelectShu.UseVisualStyleBackColor = true;
            this.button_SelectShu.Click += new System.EventHandler(this.button_SelectShu_Click);
            // 
            // button_SelectHeng
            // 
            this.button_SelectHeng.Location = new System.Drawing.Point(141, 67);
            this.button_SelectHeng.Name = "button_SelectHeng";
            this.button_SelectHeng.Size = new System.Drawing.Size(75, 23);
            this.button_SelectHeng.TabIndex = 64;
            this.button_SelectHeng.Text = "横线框选";
            this.button_SelectHeng.UseVisualStyleBackColor = true;
            this.button_SelectHeng.Click += new System.EventHandler(this.button_SelectHeng_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(2, 169);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(23, 12);
            this.label16.TabIndex = 55;
            this.label16.Text = "Phi";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 12);
            this.label4.TabIndex = 54;
            this.label4.Text = "Phi";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(112, 145);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 58;
            this.label15.Text = "Len2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(112, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 52;
            this.label5.Text = "Len2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(2, 145);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 51;
            this.label14.Text = "Len1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(2, 47);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 50;
            this.label12.Text = "Len1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(112, 121);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 12);
            this.label13.TabIndex = 49;
            this.label13.Text = "Col";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(112, 23);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(23, 12);
            this.label20.TabIndex = 53;
            this.label20.Text = "Col";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(2, 121);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(23, 12);
            this.label21.TabIndex = 48;
            this.label21.Text = "Row";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(2, 23);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(23, 12);
            this.label22.TabIndex = 47;
            this.label22.Text = "Row";
            // 
            // numericUpDown_Threshold
            // 
            this.numericUpDown_Threshold.Location = new System.Drawing.Point(31, 216);
            this.numericUpDown_Threshold.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDown_Threshold.Name = "numericUpDown_Threshold";
            this.numericUpDown_Threshold.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_Threshold.TabIndex = 71;
            this.numericUpDown_Threshold.ValueChanged += new System.EventHandler(this.numericUpDown_Threshold_ValueChanged);
            // 
            // numericUpDown_Sigma
            // 
            this.numericUpDown_Sigma.DecimalPlaces = 1;
            this.numericUpDown_Sigma.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_Sigma.Location = new System.Drawing.Point(141, 216);
            this.numericUpDown_Sigma.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown_Sigma.Name = "numericUpDown_Sigma";
            this.numericUpDown_Sigma.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_Sigma.TabIndex = 72;
            this.numericUpDown_Sigma.ValueChanged += new System.EventHandler(this.numericUpDown_Sigma_ValueChanged);
            // 
            // numericUpDown_DeltaAngle
            // 
            this.numericUpDown_DeltaAngle.DecimalPlaces = 1;
            this.numericUpDown_DeltaAngle.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_DeltaAngle.Location = new System.Drawing.Point(31, 240);
            this.numericUpDown_DeltaAngle.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown_DeltaAngle.Name = "numericUpDown_DeltaAngle";
            this.numericUpDown_DeltaAngle.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_DeltaAngle.TabIndex = 73;
            this.numericUpDown_DeltaAngle.ValueChanged += new System.EventHandler(this.numericUpDown_DeltaAngle_ValueChanged);
            // 
            // numericUpDown_ShuPhi
            // 
            this.numericUpDown_ShuPhi.DecimalPlaces = 1;
            this.numericUpDown_ShuPhi.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_ShuPhi.Location = new System.Drawing.Point(31, 166);
            this.numericUpDown_ShuPhi.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDown_ShuPhi.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDown_ShuPhi.Name = "numericUpDown_ShuPhi";
            this.numericUpDown_ShuPhi.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ShuPhi.TabIndex = 69;
            this.numericUpDown_ShuPhi.ValueChanged += new System.EventHandler(this.numericUpDown_ShuPhi_ValueChanged);
            // 
            // numericUpDown_HengPhi
            // 
            this.numericUpDown_HengPhi.DecimalPlaces = 1;
            this.numericUpDown_HengPhi.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_HengPhi.Location = new System.Drawing.Point(31, 68);
            this.numericUpDown_HengPhi.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDown_HengPhi.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDown_HengPhi.Name = "numericUpDown_HengPhi";
            this.numericUpDown_HengPhi.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_HengPhi.TabIndex = 63;
            this.numericUpDown_HengPhi.ValueChanged += new System.EventHandler(this.numericUpDown_HengPhi_ValueChanged);
            // 
            // numericUpDown_ShuLen2
            // 
            this.numericUpDown_ShuLen2.Location = new System.Drawing.Point(141, 142);
            this.numericUpDown_ShuLen2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ShuLen2.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_ShuLen2.Name = "numericUpDown_ShuLen2";
            this.numericUpDown_ShuLen2.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ShuLen2.TabIndex = 68;
            this.numericUpDown_ShuLen2.ValueChanged += new System.EventHandler(this.numericUpDown_ShuLen2_ValueChanged);
            // 
            // numericUpDown_HengLen2
            // 
            this.numericUpDown_HengLen2.Location = new System.Drawing.Point(141, 44);
            this.numericUpDown_HengLen2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_HengLen2.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_HengLen2.Name = "numericUpDown_HengLen2";
            this.numericUpDown_HengLen2.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_HengLen2.TabIndex = 62;
            this.numericUpDown_HengLen2.ValueChanged += new System.EventHandler(this.numericUpDown_HengLen2_ValueChanged);
            // 
            // numericUpDown_ShuLen1
            // 
            this.numericUpDown_ShuLen1.Location = new System.Drawing.Point(31, 142);
            this.numericUpDown_ShuLen1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ShuLen1.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_ShuLen1.Name = "numericUpDown_ShuLen1";
            this.numericUpDown_ShuLen1.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ShuLen1.TabIndex = 67;
            this.numericUpDown_ShuLen1.ValueChanged += new System.EventHandler(this.numericUpDown_ShuLen1_ValueChanged);
            // 
            // numericUpDown_HengLen1
            // 
            this.numericUpDown_HengLen1.Location = new System.Drawing.Point(31, 44);
            this.numericUpDown_HengLen1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_HengLen1.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_HengLen1.Name = "numericUpDown_HengLen1";
            this.numericUpDown_HengLen1.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_HengLen1.TabIndex = 61;
            this.numericUpDown_HengLen1.ValueChanged += new System.EventHandler(this.numericUpDown_HengLen1_ValueChanged);
            // 
            // numericUpDown_ShuCol
            // 
            this.numericUpDown_ShuCol.Location = new System.Drawing.Point(141, 118);
            this.numericUpDown_ShuCol.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ShuCol.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_ShuCol.Name = "numericUpDown_ShuCol";
            this.numericUpDown_ShuCol.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ShuCol.TabIndex = 66;
            this.numericUpDown_ShuCol.ValueChanged += new System.EventHandler(this.numericUpDown_ShuCol_ValueChanged);
            // 
            // numericUpDown_ShuRow
            // 
            this.numericUpDown_ShuRow.Location = new System.Drawing.Point(31, 118);
            this.numericUpDown_ShuRow.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ShuRow.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_ShuRow.Name = "numericUpDown_ShuRow";
            this.numericUpDown_ShuRow.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ShuRow.TabIndex = 65;
            this.numericUpDown_ShuRow.ValueChanged += new System.EventHandler(this.numericUpDown_ShuRow_ValueChanged);
            // 
            // numericUpDown_HengCol
            // 
            this.numericUpDown_HengCol.Location = new System.Drawing.Point(141, 20);
            this.numericUpDown_HengCol.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_HengCol.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_HengCol.Name = "numericUpDown_HengCol";
            this.numericUpDown_HengCol.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_HengCol.TabIndex = 60;
            this.numericUpDown_HengCol.ValueChanged += new System.EventHandler(this.numericUpDown_HengCol_ValueChanged);
            // 
            // numericUpDown_HengRow
            // 
            this.numericUpDown_HengRow.Location = new System.Drawing.Point(31, 20);
            this.numericUpDown_HengRow.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_HengRow.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_HengRow.Name = "numericUpDown_HengRow";
            this.numericUpDown_HengRow.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_HengRow.TabIndex = 59;
            this.numericUpDown_HengRow.ValueChanged += new System.EventHandler(this.numericUpDown_HengRow_ValueChanged);
            // 
            // checkBox_EnableLine
            // 
            this.checkBox_EnableLine.AutoSize = true;
            this.checkBox_EnableLine.Location = new System.Drawing.Point(150, 215);
            this.checkBox_EnableLine.Name = "checkBox_EnableLine";
            this.checkBox_EnableLine.Size = new System.Drawing.Size(48, 16);
            this.checkBox_EnableLine.TabIndex = 85;
            this.checkBox_EnableLine.Text = "找线";
            this.checkBox_EnableLine.UseVisualStyleBackColor = true;
            this.checkBox_EnableLine.CheckedChanged += new System.EventHandler(this.checkBox_EnableLine_CheckedChanged);
            // 
            // VisionParamForm_Mark
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 603);
            this.Controls.Add(this.checkBox_EnableLine);
            this.Controls.Add(this.groupBox_EnableLine);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.numericUpDown_OffsetCol);
            this.Controls.Add(this.numericUpDown_OffsetRow);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.numericUpDown_ExposureTime);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numericUpDown_Score);
            this.Controls.Add(this.button_DrawMark);
            this.Controls.Add(this.checkBox_Refresh);
            this.Controls.Add(this.comboBox_Mark);
            this.Controls.Add(this.button_SelectROI);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numericUpDown_ROIPhi);
            this.Controls.Add(this.numericUpDown_ROILen2);
            this.Controls.Add(this.numericUpDown_ROILen1);
            this.Controls.Add(this.numericUpDown_ROICol);
            this.Controls.Add(this.numericUpDown_ROIRow);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VisionParamForm_Mark";
            this.Text = "VisionParamForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VisionParamForm_Mark_FormClosing);
            this.Load += new System.EventHandler(this.VisionParamForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ROIPhi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ROILen2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ROILen1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ROICol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ROIRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ExposureTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Score)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_OffsetRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_OffsetCol)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox_EnableLine.ResumeLayout(false);
            this.groupBox_EnableLine.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Threshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Sigma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_DeltaAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuPhi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengPhi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuLen2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengLen2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuLen1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengLen1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengRow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_DrawMark;
        private System.Windows.Forms.CheckBox checkBox_Refresh;
        private System.Windows.Forms.ComboBox comboBox_Mark;
        private System.Windows.Forms.Button button_SelectROI;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDown_ROIPhi;
        private System.Windows.Forms.NumericUpDown numericUpDown_ROILen2;
        private System.Windows.Forms.NumericUpDown numericUpDown_ROILen1;
        private System.Windows.Forms.NumericUpDown numericUpDown_ROICol;
        private System.Windows.Forms.NumericUpDown numericUpDown_ROIRow;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numericUpDown_ExposureTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown_Score;
        private System.Windows.Forms.NumericUpDown numericUpDown_OffsetRow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown_OffsetCol;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton_NCC;
        private System.Windows.Forms.RadioButton radioButton_Sharp;
        private System.Windows.Forms.GroupBox groupBox_EnableLine;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_SelectShu;
        private System.Windows.Forms.Button button_SelectHeng;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown numericUpDown_Threshold;
        private System.Windows.Forms.NumericUpDown numericUpDown_Sigma;
        private System.Windows.Forms.NumericUpDown numericUpDown_DeltaAngle;
        private System.Windows.Forms.NumericUpDown numericUpDown_ShuPhi;
        private System.Windows.Forms.NumericUpDown numericUpDown_HengPhi;
        private System.Windows.Forms.NumericUpDown numericUpDown_ShuLen2;
        private System.Windows.Forms.NumericUpDown numericUpDown_HengLen2;
        private System.Windows.Forms.NumericUpDown numericUpDown_ShuLen1;
        private System.Windows.Forms.NumericUpDown numericUpDown_HengLen1;
        private System.Windows.Forms.NumericUpDown numericUpDown_ShuCol;
        private System.Windows.Forms.NumericUpDown numericUpDown_ShuRow;
        private System.Windows.Forms.NumericUpDown numericUpDown_HengCol;
        private System.Windows.Forms.NumericUpDown numericUpDown_HengRow;
        private System.Windows.Forms.CheckBox checkBox_EnableLine;
        private System.Windows.Forms.CheckBox checkBox_MetLine;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioButton_ShuNegative;
        private System.Windows.Forms.RadioButton radioButton_ShuPositive;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radioButton_HengNegative;
        private System.Windows.Forms.RadioButton radioButton_HengPositive;
    }
}