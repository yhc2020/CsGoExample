﻿namespace fhs
{
    partial class VisionParamForm_Line
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.button_SelectShu = new System.Windows.Forms.Button();
            this.button_SelectHeng = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.numericUpDown_ExposureTime = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDown_Threshold = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Sigma = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_DeltaAngle = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ShuPhi = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_HengPhi = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ShuLen2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_HengLen2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ShuLen1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_HengLen1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ShuCol = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ShuRow = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_HengCol = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_HengRow = new System.Windows.Forms.NumericUpDown();
            this.comboBox_Line = new System.Windows.Forms.ComboBox();
            this.checkBox_Refresh = new System.Windows.Forms.CheckBox();
            this.checkBox_MetLine = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton_HengNegative = new System.Windows.Forms.RadioButton();
            this.radioButton_HengPositive = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioButton_ShuNegative = new System.Windows.Forms.RadioButton();
            this.radioButton_ShuPositive = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ExposureTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Threshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Sigma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_DeltaAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuPhi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengPhi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuLen2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengLen2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuLen1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengLen1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengRow)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 238);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 12);
            this.label19.TabIndex = 17;
            this.label19.Text = "阈值";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(119, 238);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 12);
            this.label18.TabIndex = 28;
            this.label18.Text = "过滤";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 262);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 12);
            this.label17.TabIndex = 27;
            this.label17.Text = "角差";
            // 
            // button_SelectShu
            // 
            this.button_SelectShu.Location = new System.Drawing.Point(148, 184);
            this.button_SelectShu.Name = "button_SelectShu";
            this.button_SelectShu.Size = new System.Drawing.Size(75, 23);
            this.button_SelectShu.TabIndex = 42;
            this.button_SelectShu.Text = "竖线框选";
            this.button_SelectShu.UseVisualStyleBackColor = true;
            this.button_SelectShu.Click += new System.EventHandler(this.button_SelectShu_Click);
            // 
            // button_SelectHeng
            // 
            this.button_SelectHeng.Location = new System.Drawing.Point(148, 86);
            this.button_SelectHeng.Name = "button_SelectHeng";
            this.button_SelectHeng.Size = new System.Drawing.Size(75, 23);
            this.button_SelectHeng.TabIndex = 35;
            this.button_SelectHeng.Text = "横线框选";
            this.button_SelectHeng.UseVisualStyleBackColor = true;
            this.button_SelectHeng.Click += new System.EventHandler(this.button_SelectHeng_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 188);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(23, 12);
            this.label16.TabIndex = 26;
            this.label16.Text = "Phi";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 12);
            this.label11.TabIndex = 25;
            this.label11.Text = "Phi";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(119, 164);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 29;
            this.label15.Text = "Len2";
            // 
            // numericUpDown_ExposureTime
            // 
            this.numericUpDown_ExposureTime.Location = new System.Drawing.Point(148, 259);
            this.numericUpDown_ExposureTime.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_ExposureTime.Name = "numericUpDown_ExposureTime";
            this.numericUpDown_ExposureTime.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ExposureTime.TabIndex = 46;
            this.numericUpDown_ExposureTime.ValueChanged += new System.EventHandler(this.numericUpDown_ExposureTime_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(119, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 23;
            this.label10.Text = "Len2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 164);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 22;
            this.label14.Text = "Len1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 12);
            this.label9.TabIndex = 21;
            this.label9.Text = "Len1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(119, 140);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 12);
            this.label13.TabIndex = 20;
            this.label13.Text = "Col";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(119, 262);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 36;
            this.label6.Text = "曝光";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(119, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 12);
            this.label8.TabIndex = 24;
            this.label8.Text = "Col";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 140);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 12);
            this.label12.TabIndex = 19;
            this.label12.Text = "Row";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 12);
            this.label7.TabIndex = 18;
            this.label7.Text = "Row";
            // 
            // numericUpDown_Threshold
            // 
            this.numericUpDown_Threshold.Location = new System.Drawing.Point(38, 235);
            this.numericUpDown_Threshold.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDown_Threshold.Name = "numericUpDown_Threshold";
            this.numericUpDown_Threshold.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_Threshold.TabIndex = 43;
            this.numericUpDown_Threshold.ValueChanged += new System.EventHandler(this.numericUpDown_Threshold_ValueChanged);
            // 
            // numericUpDown_Sigma
            // 
            this.numericUpDown_Sigma.DecimalPlaces = 1;
            this.numericUpDown_Sigma.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_Sigma.Location = new System.Drawing.Point(148, 235);
            this.numericUpDown_Sigma.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown_Sigma.Name = "numericUpDown_Sigma";
            this.numericUpDown_Sigma.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_Sigma.TabIndex = 44;
            this.numericUpDown_Sigma.ValueChanged += new System.EventHandler(this.numericUpDown_Sigma_ValueChanged);
            // 
            // numericUpDown_DeltaAngle
            // 
            this.numericUpDown_DeltaAngle.DecimalPlaces = 1;
            this.numericUpDown_DeltaAngle.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_DeltaAngle.Location = new System.Drawing.Point(38, 259);
            this.numericUpDown_DeltaAngle.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown_DeltaAngle.Name = "numericUpDown_DeltaAngle";
            this.numericUpDown_DeltaAngle.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_DeltaAngle.TabIndex = 45;
            this.numericUpDown_DeltaAngle.ValueChanged += new System.EventHandler(this.numericUpDown_DeltaAngle_ValueChanged);
            // 
            // numericUpDown_ShuPhi
            // 
            this.numericUpDown_ShuPhi.DecimalPlaces = 1;
            this.numericUpDown_ShuPhi.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_ShuPhi.Location = new System.Drawing.Point(38, 185);
            this.numericUpDown_ShuPhi.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDown_ShuPhi.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDown_ShuPhi.Name = "numericUpDown_ShuPhi";
            this.numericUpDown_ShuPhi.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ShuPhi.TabIndex = 41;
            this.numericUpDown_ShuPhi.ValueChanged += new System.EventHandler(this.numericUpDown_ShuPhi_ValueChanged);
            // 
            // numericUpDown_HengPhi
            // 
            this.numericUpDown_HengPhi.DecimalPlaces = 1;
            this.numericUpDown_HengPhi.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_HengPhi.Location = new System.Drawing.Point(38, 87);
            this.numericUpDown_HengPhi.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDown_HengPhi.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDown_HengPhi.Name = "numericUpDown_HengPhi";
            this.numericUpDown_HengPhi.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_HengPhi.TabIndex = 34;
            this.numericUpDown_HengPhi.ValueChanged += new System.EventHandler(this.numericUpDown_HengPhi_ValueChanged);
            // 
            // numericUpDown_ShuLen2
            // 
            this.numericUpDown_ShuLen2.Location = new System.Drawing.Point(148, 161);
            this.numericUpDown_ShuLen2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ShuLen2.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_ShuLen2.Name = "numericUpDown_ShuLen2";
            this.numericUpDown_ShuLen2.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ShuLen2.TabIndex = 40;
            this.numericUpDown_ShuLen2.ValueChanged += new System.EventHandler(this.numericUpDown_ShuLen2_ValueChanged);
            // 
            // numericUpDown_HengLen2
            // 
            this.numericUpDown_HengLen2.Location = new System.Drawing.Point(148, 63);
            this.numericUpDown_HengLen2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_HengLen2.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_HengLen2.Name = "numericUpDown_HengLen2";
            this.numericUpDown_HengLen2.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_HengLen2.TabIndex = 33;
            this.numericUpDown_HengLen2.ValueChanged += new System.EventHandler(this.numericUpDown_HengLen2_ValueChanged);
            // 
            // numericUpDown_ShuLen1
            // 
            this.numericUpDown_ShuLen1.Location = new System.Drawing.Point(38, 161);
            this.numericUpDown_ShuLen1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ShuLen1.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_ShuLen1.Name = "numericUpDown_ShuLen1";
            this.numericUpDown_ShuLen1.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ShuLen1.TabIndex = 39;
            this.numericUpDown_ShuLen1.ValueChanged += new System.EventHandler(this.numericUpDown_ShuLen1_ValueChanged);
            // 
            // numericUpDown_HengLen1
            // 
            this.numericUpDown_HengLen1.Location = new System.Drawing.Point(38, 63);
            this.numericUpDown_HengLen1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_HengLen1.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_HengLen1.Name = "numericUpDown_HengLen1";
            this.numericUpDown_HengLen1.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_HengLen1.TabIndex = 32;
            this.numericUpDown_HengLen1.ValueChanged += new System.EventHandler(this.numericUpDown_HengLen1_ValueChanged);
            // 
            // numericUpDown_ShuCol
            // 
            this.numericUpDown_ShuCol.Location = new System.Drawing.Point(148, 137);
            this.numericUpDown_ShuCol.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ShuCol.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_ShuCol.Name = "numericUpDown_ShuCol";
            this.numericUpDown_ShuCol.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ShuCol.TabIndex = 38;
            this.numericUpDown_ShuCol.ValueChanged += new System.EventHandler(this.numericUpDown_ShuCol_ValueChanged);
            // 
            // numericUpDown_ShuRow
            // 
            this.numericUpDown_ShuRow.Location = new System.Drawing.Point(38, 137);
            this.numericUpDown_ShuRow.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_ShuRow.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_ShuRow.Name = "numericUpDown_ShuRow";
            this.numericUpDown_ShuRow.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_ShuRow.TabIndex = 37;
            this.numericUpDown_ShuRow.ValueChanged += new System.EventHandler(this.numericUpDown_ShuRow_ValueChanged);
            // 
            // numericUpDown_HengCol
            // 
            this.numericUpDown_HengCol.Location = new System.Drawing.Point(148, 39);
            this.numericUpDown_HengCol.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_HengCol.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_HengCol.Name = "numericUpDown_HengCol";
            this.numericUpDown_HengCol.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_HengCol.TabIndex = 31;
            this.numericUpDown_HengCol.ValueChanged += new System.EventHandler(this.numericUpDown_HengCol_ValueChanged);
            // 
            // numericUpDown_HengRow
            // 
            this.numericUpDown_HengRow.Location = new System.Drawing.Point(38, 39);
            this.numericUpDown_HengRow.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_HengRow.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numericUpDown_HengRow.Name = "numericUpDown_HengRow";
            this.numericUpDown_HengRow.Size = new System.Drawing.Size(75, 21);
            this.numericUpDown_HengRow.TabIndex = 30;
            this.numericUpDown_HengRow.ValueChanged += new System.EventHandler(this.numericUpDown_HengRow_ValueChanged);
            // 
            // comboBox_Line
            // 
            this.comboBox_Line.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Line.FormattingEnabled = true;
            this.comboBox_Line.Location = new System.Drawing.Point(10, 10);
            this.comboBox_Line.Name = "comboBox_Line";
            this.comboBox_Line.Size = new System.Drawing.Size(121, 20);
            this.comboBox_Line.TabIndex = 47;
            this.comboBox_Line.SelectedIndexChanged += new System.EventHandler(this.comboBox_Line_SelectedIndexChanged);
            // 
            // checkBox_Refresh
            // 
            this.checkBox_Refresh.AutoSize = true;
            this.checkBox_Refresh.Location = new System.Drawing.Point(148, 13);
            this.checkBox_Refresh.Name = "checkBox_Refresh";
            this.checkBox_Refresh.Size = new System.Drawing.Size(48, 16);
            this.checkBox_Refresh.TabIndex = 48;
            this.checkBox_Refresh.Text = "刷新";
            this.checkBox_Refresh.UseVisualStyleBackColor = true;
            // 
            // checkBox_MetLine
            // 
            this.checkBox_MetLine.AutoSize = true;
            this.checkBox_MetLine.Location = new System.Drawing.Point(38, 287);
            this.checkBox_MetLine.Name = "checkBox_MetLine";
            this.checkBox_MetLine.Size = new System.Drawing.Size(72, 16);
            this.checkBox_MetLine.TabIndex = 49;
            this.checkBox_MetLine.Text = "过滤杂点";
            this.checkBox_MetLine.UseVisualStyleBackColor = true;
            this.checkBox_MetLine.CheckedChanged += new System.EventHandler(this.checkBox_MetLine_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton_HengNegative);
            this.panel1.Controls.Add(this.radioButton_HengPositive);
            this.panel1.Location = new System.Drawing.Point(11, 111);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(132, 23);
            this.panel1.TabIndex = 59;
            // 
            // radioButton_HengNegative
            // 
            this.radioButton_HengNegative.AutoSize = true;
            this.radioButton_HengNegative.Location = new System.Drawing.Point(69, 4);
            this.radioButton_HengNegative.Name = "radioButton_HengNegative";
            this.radioButton_HengNegative.Size = new System.Drawing.Size(59, 16);
            this.radioButton_HengNegative.TabIndex = 2;
            this.radioButton_HengNegative.TabStop = true;
            this.radioButton_HengNegative.Text = "白到黑";
            this.radioButton_HengNegative.UseVisualStyleBackColor = true;
            this.radioButton_HengNegative.CheckedChanged += new System.EventHandler(this.radioButton_HengNegative_CheckedChanged);
            // 
            // radioButton_HengPositive
            // 
            this.radioButton_HengPositive.AutoSize = true;
            this.radioButton_HengPositive.Location = new System.Drawing.Point(4, 4);
            this.radioButton_HengPositive.Name = "radioButton_HengPositive";
            this.radioButton_HengPositive.Size = new System.Drawing.Size(59, 16);
            this.radioButton_HengPositive.TabIndex = 1;
            this.radioButton_HengPositive.TabStop = true;
            this.radioButton_HengPositive.Text = "黑到白";
            this.radioButton_HengPositive.UseVisualStyleBackColor = true;
            this.radioButton_HengPositive.CheckedChanged += new System.EventHandler(this.radioButton_HengPositive_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radioButton_ShuNegative);
            this.panel2.Controls.Add(this.radioButton_ShuPositive);
            this.panel2.Location = new System.Drawing.Point(11, 209);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(132, 23);
            this.panel2.TabIndex = 60;
            // 
            // radioButton_ShuNegative
            // 
            this.radioButton_ShuNegative.AutoSize = true;
            this.radioButton_ShuNegative.Location = new System.Drawing.Point(69, 4);
            this.radioButton_ShuNegative.Name = "radioButton_ShuNegative";
            this.radioButton_ShuNegative.Size = new System.Drawing.Size(59, 16);
            this.radioButton_ShuNegative.TabIndex = 2;
            this.radioButton_ShuNegative.TabStop = true;
            this.radioButton_ShuNegative.Text = "白到黑";
            this.radioButton_ShuNegative.UseVisualStyleBackColor = true;
            this.radioButton_ShuNegative.CheckedChanged += new System.EventHandler(this.radioButton_ShuNegative_CheckedChanged);
            // 
            // radioButton_ShuPositive
            // 
            this.radioButton_ShuPositive.AutoSize = true;
            this.radioButton_ShuPositive.Location = new System.Drawing.Point(4, 4);
            this.radioButton_ShuPositive.Name = "radioButton_ShuPositive";
            this.radioButton_ShuPositive.Size = new System.Drawing.Size(59, 16);
            this.radioButton_ShuPositive.TabIndex = 1;
            this.radioButton_ShuPositive.TabStop = true;
            this.radioButton_ShuPositive.Text = "黑到白";
            this.radioButton_ShuPositive.UseVisualStyleBackColor = true;
            this.radioButton_ShuPositive.CheckedChanged += new System.EventHandler(this.radioButton_ShuPositive_CheckedChanged);
            // 
            // VisionParamForm_Line
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(257, 651);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.checkBox_MetLine);
            this.Controls.Add(this.checkBox_Refresh);
            this.Controls.Add(this.comboBox_Line);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.button_SelectShu);
            this.Controls.Add(this.button_SelectHeng);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.numericUpDown_ExposureTime);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numericUpDown_Threshold);
            this.Controls.Add(this.numericUpDown_Sigma);
            this.Controls.Add(this.numericUpDown_DeltaAngle);
            this.Controls.Add(this.numericUpDown_ShuPhi);
            this.Controls.Add(this.numericUpDown_HengPhi);
            this.Controls.Add(this.numericUpDown_ShuLen2);
            this.Controls.Add(this.numericUpDown_HengLen2);
            this.Controls.Add(this.numericUpDown_ShuLen1);
            this.Controls.Add(this.numericUpDown_HengLen1);
            this.Controls.Add(this.numericUpDown_ShuCol);
            this.Controls.Add(this.numericUpDown_ShuRow);
            this.Controls.Add(this.numericUpDown_HengCol);
            this.Controls.Add(this.numericUpDown_HengRow);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VisionParamForm_Line";
            this.Text = "VisionParamForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VisionParamForm_Line_FormClosing);
            this.Load += new System.EventHandler(this.VisionParamForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ExposureTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Threshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Sigma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_DeltaAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuPhi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengPhi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuLen2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengLen2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuLen1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengLen1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ShuRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_HengRow)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button_SelectShu;
        private System.Windows.Forms.Button button_SelectHeng;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numericUpDown_ExposureTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDown_Threshold;
        private System.Windows.Forms.NumericUpDown numericUpDown_Sigma;
        private System.Windows.Forms.NumericUpDown numericUpDown_DeltaAngle;
        private System.Windows.Forms.NumericUpDown numericUpDown_ShuPhi;
        private System.Windows.Forms.NumericUpDown numericUpDown_HengPhi;
        private System.Windows.Forms.NumericUpDown numericUpDown_ShuLen2;
        private System.Windows.Forms.NumericUpDown numericUpDown_HengLen2;
        private System.Windows.Forms.NumericUpDown numericUpDown_ShuLen1;
        private System.Windows.Forms.NumericUpDown numericUpDown_HengLen1;
        private System.Windows.Forms.NumericUpDown numericUpDown_ShuCol;
        private System.Windows.Forms.NumericUpDown numericUpDown_ShuRow;
        private System.Windows.Forms.NumericUpDown numericUpDown_HengCol;
        private System.Windows.Forms.NumericUpDown numericUpDown_HengRow;
        private System.Windows.Forms.ComboBox comboBox_Line;
        private System.Windows.Forms.CheckBox checkBox_Refresh;
        private System.Windows.Forms.CheckBox checkBox_MetLine;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton_HengNegative;
        private System.Windows.Forms.RadioButton radioButton_HengPositive;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radioButton_ShuNegative;
        private System.Windows.Forms.RadioButton radioButton_ShuPositive;
    }
}