﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HalconDotNet;
using Go;

namespace fhs
{
    class VisionParamForm_Calib : VisionParamForm_Mark
    {
        protected override VisionName.Vision Vision
        {
            get
            {
                return VisionName.Vision.Calib;
            }
        }
    }
}
