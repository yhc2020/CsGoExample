﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Go;

namespace fhs
{
    public partial class VisionParamForm_Line : Form
    {
        Vision_Line.Param _param;
        generator _aciton;
        bool _refresh;

        public VisionParamForm_Line()
        {
            InitializeComponent();
        }

        private void VisionParamForm_Load(object sender, EventArgs e)
        {
            try
            {
                ReLoad();
            }
            catch (System.Exception ex)
            {
                LogMgr.Error($"视觉参数填充错误{ex.Message}");
            }
            generator.go(out _aciton, GlobalData.mainStrand, RefreshAction);
        }

        private void ReLoad()
        {
            _param = (Vision_Line.Param)VisionMgr.visionParams[VisionName.Vision.Line];
            comboBox_Line.Items.Clear();
            foreach (var item in _param.VisionConfig)
            {
                comboBox_Line.Items.Add(item.Key);
            }
            if (comboBox_Line.Items.Count > 0)
            {
                comboBox_Line.SelectedIndex = 0;
            }
        }

        private async Task RefreshAction()
        {
            while (true)
            {
                int idx = comboBox_Line.SelectedIndex;
                await generator.sleep(100);
                if (!Visible || !Enabled || !checkBox_Refresh.Checked || idx < 0 || null == VisionDebugForm.obj.LastImage)
                {
                    continue;
                }
                VisionName.Camera camera = StringToEnum.Convert<VisionName.Camera>(comboBox_Line.Text);
                _refresh = true;
                await VisionMgr.visions[VisionName.Vision.Line].Translate(VisionDebugForm.obj.CameraView, VisionDebugForm.obj.ObjectsPreview, _param, new Vision_Line.Option { camera = camera, image = VisionDebugForm.obj.LastImage });
                _refresh = false;
            }
        }

        private void VisionParamForm_Line_FormClosing(object sender, FormClosingEventArgs e)
        {
            _aciton?.stop();
        }

        private Vision_Line.Param.Line CurrCamera
        {
            get
            {
                int idx = comboBox_Line.SelectedIndex;
                if (idx < 0)
                {
                    return new Vision_Line.Param.Line();
                }
                VisionName.Camera camera = StringToEnum.Convert<VisionName.Camera>(comboBox_Line.Text);
                return _param.VisionConfig[camera];
            }
        }

        private void numericUpDown_HengRow_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.hengLine.Row = (double)numericUpDown_HengRow.Value;
        }

        private void numericUpDown_HengCol_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.hengLine.Col = (double)numericUpDown_HengCol.Value;
        }

        private void numericUpDown_HengLen1_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.hengLine.Len1 = (double)numericUpDown_HengLen1.Value;
        }

        private void numericUpDown_HengLen2_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.hengLine.Len2 = (double)numericUpDown_HengLen2.Value;
        }

        private void numericUpDown_HengPhi_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.hengLine.Phi = FlatCalib.ToRad((double)numericUpDown_HengPhi.Value);
        }

        private void numericUpDown_ShuRow_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.shuLine.Row = (double)numericUpDown_ShuRow.Value;
        }

        private void numericUpDown_ShuCol_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.shuLine.Col = (double)numericUpDown_ShuCol.Value;
        }

        private void numericUpDown_ShuLen1_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.shuLine.Len1 = (double)numericUpDown_ShuLen1.Value;
        }

        private void numericUpDown_ShuLen2_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.shuLine.Len2 = (double)numericUpDown_ShuLen2.Value;
        }

        private void numericUpDown_ShuPhi_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.shuLine.Phi = FlatCalib.ToRad((double)numericUpDown_ShuPhi.Value);
        }

        private void numericUpDown_Threshold_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.threshold = (int)numericUpDown_Threshold.Value;
        }

        private void numericUpDown_Sigma_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.sigma = (double)numericUpDown_Sigma.Value;
        }

        private void numericUpDown_DeltaAngle_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.deltaAngle = FlatCalib.ToRad((double)numericUpDown_DeltaAngle.Value);
        }

        private void numericUpDown_ExposureTime_ValueChanged(object sender, EventArgs e)
        {
            CurrCamera.exposureTime = (int)numericUpDown_ExposureTime.Value;
        }

        private void checkBox_MetLine_CheckedChanged(object sender, EventArgs e)
        {
            CurrCamera.metrologyLine = checkBox_MetLine.Checked;
        }

        private void button_SelectHeng_Click(object sender, EventArgs e)
        {
            if (_refresh) return;
            Enabled = false;
            VisionDebugForm.obj.DrawRectangle2(numericUpDown_HengRow, numericUpDown_HengCol, numericUpDown_HengPhi, numericUpDown_HengLen1, numericUpDown_HengLen2);
            Enabled = true;
        }

        private void button_SelectShu_Click(object sender, EventArgs e)
        {
            if (_refresh) return;
            Enabled = false;
            VisionDebugForm.obj.DrawRectangle2(numericUpDown_ShuRow, numericUpDown_ShuCol, numericUpDown_ShuPhi, numericUpDown_ShuLen1, numericUpDown_ShuLen2);
            Enabled = true;
        }

        private void comboBox_Line_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int idx = comboBox_Line.SelectedIndex;
                if (idx < 0) return;
                VisionName.Camera cameraName = StringToEnum.Convert<VisionName.Camera>(comboBox_Line.Text);
                ((Vision_Line)VisionMgr.visions[VisionName.Vision.Line]).PreviewCamera = cameraName;
                Vision_Line.Param.Line curr = _param.VisionConfig[cameraName];
                numericUpDown_ExposureTime.Value = (decimal)curr.exposureTime;
                numericUpDown_HengRow.Value = (decimal)curr.hengLine.Row;
                numericUpDown_HengCol.Value = (decimal)curr.hengLine.Col;
                numericUpDown_HengLen1.Value = (decimal)curr.hengLine.Len1;
                numericUpDown_HengLen2.Value = (decimal)curr.hengLine.Len2;
                numericUpDown_HengPhi.Value = (decimal)FlatCalib.ToDeg(curr.hengLine.Phi);
                numericUpDown_ShuRow.Value = (decimal)curr.shuLine.Row;
                numericUpDown_ShuCol.Value = (decimal)curr.shuLine.Col;
                numericUpDown_ShuLen1.Value = (decimal)curr.shuLine.Len1;
                numericUpDown_ShuLen2.Value = (decimal)curr.shuLine.Len2;
                numericUpDown_ShuPhi.Value = (decimal)FlatCalib.ToDeg(curr.shuLine.Phi);
                numericUpDown_Threshold.Value = curr.threshold;
                numericUpDown_Sigma.Value = (decimal)curr.sigma;
                numericUpDown_DeltaAngle.Value = (decimal)FlatCalib.ToDeg(curr.deltaAngle);
                radioButton_HengPositive.Checked = curr.hengTransition;
                radioButton_HengNegative.Checked = !curr.hengTransition;
                radioButton_ShuPositive.Checked = curr.shuTransition;
                radioButton_ShuNegative.Checked = !curr.shuTransition;
                checkBox_MetLine.Checked = curr.metrologyLine;
            }
            catch (Exception)
            {
                LogMgr.Error("Line控件填充错误");
            }
        }

        private void radioButton_HengPositive_CheckedChanged(object sender, EventArgs e)
        {
            CurrCamera.hengTransition = radioButton_HengPositive.Checked;
        }

        private void radioButton_HengNegative_CheckedChanged(object sender, EventArgs e)
        {
            CurrCamera.hengTransition = !radioButton_HengNegative.Checked;
        }

        private void radioButton_ShuPositive_CheckedChanged(object sender, EventArgs e)
        {
            CurrCamera.shuTransition = radioButton_ShuPositive.Checked;
        }

        private void radioButton_ShuNegative_CheckedChanged(object sender, EventArgs e)
        {
            CurrCamera.shuTransition = !radioButton_ShuNegative.Checked;
        }
    }
}
