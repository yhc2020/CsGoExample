﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    public partial class VisionParamForm_Temp : Form
    {
        Vision_Temp.Param _param;

        public VisionParamForm_Temp()
        {
            InitializeComponent();
        }

        private void VisionParamForm_Load(object sender, EventArgs e)
        {
            try
            {
                ReLoad();
            }
            catch (System.Exception ex)
            {
                LogMgr.Error($"视觉参数填充错误{ex.Message}");
            }
        }

        private void ReLoad()
        {
            _param = (Vision_Temp.Param)VisionMgr.visionParams[VisionName.Vision.Temp];
        }
    }
}
