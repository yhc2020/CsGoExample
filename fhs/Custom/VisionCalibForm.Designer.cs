﻿namespace fhs
{
    partial class VisionCalibForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_停止 = new System.Windows.Forms.Button();
            this.button_暂停 = new System.Windows.Forms.Button();
            this.textBox_Msg = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button_CalibHand = new System.Windows.Forms.Button();
            this.comboBox_CalibHand = new System.Windows.Forms.ComboBox();
            this.button_RepeTest = new System.Windows.Forms.Button();
            this.checkBox_T1 = new System.Windows.Forms.CheckBox();
            this.checkBox_T2 = new System.Windows.Forms.CheckBox();
            this.checkBox_T3 = new System.Windows.Forms.CheckBox();
            this.checkBox_T4 = new System.Windows.Forms.CheckBox();
            this.button_CalibRotate = new System.Windows.Forms.Button();
            this.button_CameraAxis = new System.Windows.Forms.Button();
            this.button_CalibProductRotate = new System.Windows.Forms.Button();
            this.button_Monitor = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_停止
            // 
            this.button_停止.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_停止.Location = new System.Drawing.Point(12, 474);
            this.button_停止.Name = "button_停止";
            this.button_停止.Size = new System.Drawing.Size(199, 65);
            this.button_停止.TabIndex = 1;
            this.button_停止.Text = "停止";
            this.button_停止.UseVisualStyleBackColor = true;
            this.button_停止.Click += new System.EventHandler(this.button_停止_Click);
            // 
            // button_暂停
            // 
            this.button_暂停.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_暂停.Location = new System.Drawing.Point(12, 545);
            this.button_暂停.Name = "button_暂停";
            this.button_暂停.Size = new System.Drawing.Size(199, 65);
            this.button_暂停.TabIndex = 1;
            this.button_暂停.Text = "暂停";
            this.button_暂停.UseVisualStyleBackColor = true;
            this.button_暂停.Click += new System.EventHandler(this.button_暂停_Click);
            // 
            // textBox_Msg
            // 
            this.textBox_Msg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox_Msg.Location = new System.Drawing.Point(13, 617);
            this.textBox_Msg.Name = "textBox_Msg";
            this.textBox_Msg.Size = new System.Drawing.Size(197, 21);
            this.textBox_Msg.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(237, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(814, 629);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // button_CalibHand
            // 
            this.button_CalibHand.Location = new System.Drawing.Point(12, 82);
            this.button_CalibHand.Name = "button_CalibHand";
            this.button_CalibHand.Size = new System.Drawing.Size(197, 40);
            this.button_CalibHand.TabIndex = 4;
            this.button_CalibHand.Text = "手眼标定(第一步)";
            this.button_CalibHand.UseVisualStyleBackColor = true;
            this.button_CalibHand.Click += new System.EventHandler(this.button_CalibHand_Click);
            // 
            // comboBox_CalibHand
            // 
            this.comboBox_CalibHand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_CalibHand.FormattingEnabled = true;
            this.comboBox_CalibHand.Location = new System.Drawing.Point(13, 12);
            this.comboBox_CalibHand.Name = "comboBox_CalibHand";
            this.comboBox_CalibHand.Size = new System.Drawing.Size(121, 20);
            this.comboBox_CalibHand.TabIndex = 5;
            // 
            // button_RepeTest
            // 
            this.button_RepeTest.Location = new System.Drawing.Point(12, 314);
            this.button_RepeTest.Name = "button_RepeTest";
            this.button_RepeTest.Size = new System.Drawing.Size(197, 40);
            this.button_RepeTest.TabIndex = 6;
            this.button_RepeTest.Text = "相机重复性验证";
            this.button_RepeTest.UseVisualStyleBackColor = true;
            this.button_RepeTest.Click += new System.EventHandler(this.button_RepeTest_Click);
            // 
            // checkBox_T1
            // 
            this.checkBox_T1.AutoSize = true;
            this.checkBox_T1.Checked = true;
            this.checkBox_T1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_T1.Location = new System.Drawing.Point(70, 38);
            this.checkBox_T1.Name = "checkBox_T1";
            this.checkBox_T1.Size = new System.Drawing.Size(54, 16);
            this.checkBox_T1.TabIndex = 7;
            this.checkBox_T1.Text = "T1/T5";
            this.checkBox_T1.UseVisualStyleBackColor = true;
            // 
            // checkBox_T2
            // 
            this.checkBox_T2.AutoSize = true;
            this.checkBox_T2.Checked = true;
            this.checkBox_T2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_T2.Location = new System.Drawing.Point(70, 60);
            this.checkBox_T2.Name = "checkBox_T2";
            this.checkBox_T2.Size = new System.Drawing.Size(54, 16);
            this.checkBox_T2.TabIndex = 7;
            this.checkBox_T2.Text = "T2/T6";
            this.checkBox_T2.UseVisualStyleBackColor = true;
            // 
            // checkBox_T3
            // 
            this.checkBox_T3.AutoSize = true;
            this.checkBox_T3.Checked = true;
            this.checkBox_T3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_T3.Location = new System.Drawing.Point(13, 60);
            this.checkBox_T3.Name = "checkBox_T3";
            this.checkBox_T3.Size = new System.Drawing.Size(54, 16);
            this.checkBox_T3.TabIndex = 7;
            this.checkBox_T3.Text = "T3/T7";
            this.checkBox_T3.UseVisualStyleBackColor = true;
            // 
            // checkBox_T4
            // 
            this.checkBox_T4.AutoSize = true;
            this.checkBox_T4.Checked = true;
            this.checkBox_T4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_T4.Location = new System.Drawing.Point(13, 38);
            this.checkBox_T4.Name = "checkBox_T4";
            this.checkBox_T4.Size = new System.Drawing.Size(54, 16);
            this.checkBox_T4.TabIndex = 7;
            this.checkBox_T4.Text = "T4/T8";
            this.checkBox_T4.UseVisualStyleBackColor = true;
            // 
            // button_CalibRotate
            // 
            this.button_CalibRotate.Location = new System.Drawing.Point(12, 127);
            this.button_CalibRotate.Name = "button_CalibRotate";
            this.button_CalibRotate.Size = new System.Drawing.Size(197, 40);
            this.button_CalibRotate.TabIndex = 4;
            this.button_CalibRotate.Text = "旋转标定(第二步)";
            this.button_CalibRotate.UseVisualStyleBackColor = true;
            this.button_CalibRotate.Click += new System.EventHandler(this.button_CalibRotate_Click);
            // 
            // button_CameraAxis
            // 
            this.button_CameraAxis.Location = new System.Drawing.Point(12, 172);
            this.button_CameraAxis.Name = "button_CameraAxis";
            this.button_CameraAxis.Size = new System.Drawing.Size(197, 40);
            this.button_CameraAxis.TabIndex = 4;
            this.button_CameraAxis.Text = "相机轴标定(第三步)";
            this.button_CameraAxis.UseVisualStyleBackColor = true;
            this.button_CameraAxis.Click += new System.EventHandler(this.button_CameraAxis_Click);
            // 
            // button_CalibProductRotate
            // 
            this.button_CalibProductRotate.ForeColor = System.Drawing.Color.Red;
            this.button_CalibProductRotate.Location = new System.Drawing.Point(12, 217);
            this.button_CalibProductRotate.Name = "button_CalibProductRotate";
            this.button_CalibProductRotate.Size = new System.Drawing.Size(197, 40);
            this.button_CalibProductRotate.TabIndex = 4;
            this.button_CalibProductRotate.Text = "当前机种旋转标定(可选)";
            this.button_CalibProductRotate.UseVisualStyleBackColor = true;
            this.button_CalibProductRotate.Click += new System.EventHandler(this.button_CalibProductRotate_Click);
            // 
            // button_Monitor
            // 
            this.button_Monitor.Location = new System.Drawing.Point(12, 359);
            this.button_Monitor.Name = "button_Monitor";
            this.button_Monitor.Size = new System.Drawing.Size(197, 40);
            this.button_Monitor.TabIndex = 6;
            this.button_Monitor.Text = "相机实时监视";
            this.button_Monitor.UseVisualStyleBackColor = true;
            this.button_Monitor.Click += new System.EventHandler(this.button_Monitor_Click);
            // 
            // VisionCalibForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 653);
            this.Controls.Add(this.checkBox_T4);
            this.Controls.Add(this.checkBox_T3);
            this.Controls.Add(this.checkBox_T2);
            this.Controls.Add(this.checkBox_T1);
            this.Controls.Add(this.button_Monitor);
            this.Controls.Add(this.button_RepeTest);
            this.Controls.Add(this.comboBox_CalibHand);
            this.Controls.Add(this.button_CameraAxis);
            this.Controls.Add(this.button_CalibProductRotate);
            this.Controls.Add(this.button_CalibRotate);
            this.Controls.Add(this.button_CalibHand);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.textBox_Msg);
            this.Controls.Add(this.button_暂停);
            this.Controls.Add(this.button_停止);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VisionCalibForm";
            this.Text = "VisionCalibForm";
            this.Load += new System.EventHandler(this.VisionCalibForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_停止;
        private System.Windows.Forms.Button button_暂停;
        private System.Windows.Forms.TextBox textBox_Msg;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button_CalibHand;
        private System.Windows.Forms.ComboBox comboBox_CalibHand;
        private System.Windows.Forms.Button button_RepeTest;
        private System.Windows.Forms.CheckBox checkBox_T1;
        private System.Windows.Forms.CheckBox checkBox_T2;
        private System.Windows.Forms.CheckBox checkBox_T3;
        private System.Windows.Forms.CheckBox checkBox_T4;
        private System.Windows.Forms.Button button_CalibRotate;
        private System.Windows.Forms.Button button_CameraAxis;
        private System.Windows.Forms.Button button_CalibProductRotate;
        private System.Windows.Forms.Button button_Monitor;
    }
}