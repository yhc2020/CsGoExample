﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using HalconDotNet;
using Go;

namespace fhs
{
    class Vision_Calib : Vision_Mark
    {
        new public class Param : Vision_Mark.Param
        {
            public override VisionName.Vision Name
            {
                get
                {
                    return VisionName.Vision.Calib;
                }
            }
        }

        public override VisionName.Vision Name
        {
            get
            {
                return VisionName.Vision.Calib;
            }
        }

        public override ParamBase LoadParam(string productName)
        {
            Param result = new Param();
            result.Load(productName);
            return result;
        }
    }
}
