﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using HalconDotNet;
using Go;

namespace fhs
{
    class Vision_Temp : VisionBase
    {
        public class Param : ParamBase
        {

            public override VisionName.Vision Name
            {
                get
                {
                    return VisionName.Vision.Temp;
                }
            }

            public override void Load(string productName)
            {
                try
                {
                }
                catch (System.Exception ex)
                {
                    LogMgr.Error($"{Name}参数读取异常{ex.Message}");
                }
            }

            public override void Save(string productName)
            {
                try
                {
                }
                catch (System.Exception ex)
                {
                    LogMgr.Error($"{Name}参数保存异常{ex.Message}");
                }
            }
        }

        public override VisionName.Vision Name
        {
            get
            {
                return VisionName.Vision.Temp;
            }
        }

        protected override async Task<ResultBase> RefPriview(CameraControl mainView, HObject[] images, ParamBase param)
        {
            return null;
        }

        protected override async Task<ResultBase> Translate(bool isPreview, CameraControl mainView, ParamBase param, OptionBase option)
        {
            return null;
        }

        public override ParamBase LoadParam(string productName)
        {
            Param result = new Param();
            result.Load(productName);
            return result;
        }
    }
}
