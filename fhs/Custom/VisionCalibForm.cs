﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Go;

namespace fhs
{
    partial class VisionCalibForm : Form
    {
        static public CameraControl[] views;

        public VisionCalibForm()
        {
            InitializeComponent();
        }

        private void VisionCalibForm_Load(object sender, EventArgs e)
        {
            generator.go(GlobalData.mainStrand, async delegate ()
            {
                while (true)
                {
                    if (!Visible)
                    {
                        await generator.sleep(1000);
                        continue;
                    }
                    textBox_Msg.Text = CalibAction.msgView;
                    await generator.sleep(300);
                    textBox_Msg.Text = $"{CalibAction.msgView}.";
                    await generator.sleep(300);
                    textBox_Msg.Text = $"{CalibAction.msgView}..";
                    await generator.sleep(300);
                    textBox_Msg.Text = $"{CalibAction.msgView}...";
                    await generator.sleep(300);
                }
            });
            views = new CameraControl[8];
            for (int i = 0; i < 8; i++)
            {
                views[i] = new CameraControl();
                views[i].Width = (int)tableLayoutPanel1.ColumnStyles[i % 4].Width;
                views[i].Height = (int)tableLayoutPanel1.RowStyles[i / 4].Height;
                views[i].Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            }
            tableLayoutPanel1.Controls.Add(views[0], 1, 0); views[0].Text = views[0].Title = "T1/T5";
            tableLayoutPanel1.Controls.Add(views[1], 1, 1); views[1].Text = views[1].Title = "T2/T6";
            tableLayoutPanel1.Controls.Add(views[2], 0, 1); views[2].Text = views[2].Title = "T3/T7";
            tableLayoutPanel1.Controls.Add(views[3], 0, 0); views[3].Text = views[3].Title = "T4/T8";

            tableLayoutPanel1.Controls.Add(views[4], 3, 0); views[4].Text = views[4].Title = "T1/T5";
            tableLayoutPanel1.Controls.Add(views[5], 3, 1); views[5].Text = views[5].Title = "T2/T6";
            tableLayoutPanel1.Controls.Add(views[6], 2, 1); views[6].Text = views[6].Title = "T3/T7";
            tableLayoutPanel1.Controls.Add(views[7], 2, 0); views[7].Text = views[7].Title = "T4/T8";

            comboBox_CalibHand.Items.Add("Cell");
            if (8 == CameraMgr.cameras.Count)
            {
                comboBox_CalibHand.Items.Add("Bl");
            }
            comboBox_CalibHand.SelectedIndex = 0;
        }

        private void button_停止_Click(object sender, EventArgs e)
        {
            CalibAction.Stop();
        }

        private void button_暂停_Click(object sender, EventArgs e)
        {
            if (!CalibAction.IsRun())
            {
                return;
            }
            if ("恢复" == button_暂停.Text)
            {
                CalibAction.Resume();
            }
            else if ("暂停" == button_暂停.Text)
            {
                CalibAction.Pause();
            }
        }

        private void StopHandler()
        {
            LogMgr.Info("标定完成");
            button_暂停.Text = "暂停";
            ResetEnable();
        }

        private void ResetEnable()
        {
            button_Monitor.Enabled = true;
            button_RepeTest.Enabled = true;
            button_CalibHand.Enabled = true;
            button_CameraAxis.Enabled = true;
            button_CalibRotate.Enabled = true;
            button_CalibProductRotate.Enabled = true;
        }

        private void PauseHandler(bool isPause)
        {
            if (isPause)
            {
                button_暂停.Text = "恢复";
            }
            else
            {
                button_暂停.Text = "暂停";
            }
        }

        private void MakeAction(generator.action action)
        {
            if (CalibAction.IsRun() || StepAction.IsRun() || AutoAction.IsRun())
            {
                ResetEnable();
                return;
            }
            if (DialogResult.Yes == MessageBox.Show("是否确定标定？", "是否确定标定？", MessageBoxButtons.YesNo))
            {
                foreach (var item in views)
                {
                    item.Reset(); item.ShowImage();
                }
                CalibAction.MakeAction(action, StopHandler, PauseHandler);
            }
            else
            {
                ResetEnable();
            }
        }

        private bool hasShow(bool[] shows)
        {
            bool has = false;
            foreach (var item in shows)
            {
                has |= item;
            }
            return has;
        }

        private void button_CalibHand_Click(object sender, EventArgs e)
        {
            bool[] shows = Make.Array(checkBox_T1.Checked, checkBox_T2.Checked, checkBox_T3.Checked, checkBox_T4.Checked);
            if (hasShow(shows))
            {
                button_CalibHand.Enabled = false;
                MakeAction(() => CalibAction.CalibHand("Cell" == comboBox_CalibHand.Text, views, shows));
            }
        }

        private void button_CalibRotate_Click(object sender, EventArgs e)
        {
            bool[] shows = Make.Array(checkBox_T1.Checked, checkBox_T2.Checked, checkBox_T3.Checked, checkBox_T4.Checked);
            if (hasShow(shows))
            {
                button_CalibRotate.Enabled = false;
                MakeAction(() => CalibAction.RotateCalib(true, "Cell" == comboBox_CalibHand.Text, views, shows));
            }
        }

        private void button_CalibProductRotate_Click(object sender, EventArgs e)
        {
            bool[] shows = Make.Array(checkBox_T1.Checked, checkBox_T2.Checked, checkBox_T3.Checked, checkBox_T4.Checked);
            if (hasShow(shows))
            {
                button_CalibProductRotate.Enabled = false;
                MakeAction(() => CalibAction.RotateCalib(false, "Cell" == comboBox_CalibHand.Text, views, shows));
            }
        }

        private void button_CameraAxis_Click(object sender, EventArgs e)
        {
            bool[] shows = Make.Array(checkBox_T1.Checked, checkBox_T2.Checked, checkBox_T3.Checked, checkBox_T4.Checked);
            if (hasShow(shows))
            {
                button_CameraAxis.Enabled = false;
                MakeAction(() => CalibAction.CameraAxisCalib("Cell" == comboBox_CalibHand.Text, views, shows));
            }
        }

        private void button_RepeTest_Click(object sender, EventArgs e)
        {
            bool[] shows = Make.Array(checkBox_T1.Checked, checkBox_T2.Checked, checkBox_T3.Checked, checkBox_T4.Checked);
            if (hasShow(shows))
            {
                button_RepeTest.Enabled = false;
                MakeAction(() => CalibAction.RepeTest("Cell" == comboBox_CalibHand.Text, views, shows));
            }
        }

        private void button_Monitor_Click(object sender, EventArgs e)
        {
            bool[] shows = Make.Array(checkBox_T1.Checked, checkBox_T2.Checked, checkBox_T3.Checked, checkBox_T4.Checked);
            if (hasShow(shows))
            {
                button_Monitor.Enabled = false;
                MakeAction(() => CalibAction.Monitor("Cell" == comboBox_CalibHand.Text, views, shows));
            }
        }
    }
}
