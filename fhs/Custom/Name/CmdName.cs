﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace fhs
{
    partial class CmdName
    {
        public enum MC
        {
            复位,

            机械手X,
            机械手Y,
            机械手U,
            入料轴,

            Cell相机轴X1,
            Cell相机轴X2,
            Cell相机轴Y,

            BL相机轴X1,
            BL相机轴X2,
            BL相机轴Y,

            BL组装位X,
            BL组装位Y,
            BL入料轴位,

            Cell相机轴组装位X1,
            Cell相机轴组装位X2,
            Cell相机轴组装位Y,

            BL相机轴组装位X1,
            BL相机轴组装位X2,
            BL相机轴组装位Y,

            Cell拍照,
            Cell反馈,
            底板拍照,
            底板反馈,

            X补偿,
            Y补偿,
            U补偿,

            底板超限,
            Cell超限,
            移动超限
        }
    }
}
