﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    class VisionName
    {
        [Serializable]
        public enum Vision
        {
            Temp,
            Line,
            Mark,
            Calib
        }

        [Serializable]
        public enum Camera
        {
            T1Cell,
            T2Cell,
            T3Cell,
            T4Cell,
            T5Bl,
            T6Bl,
            T7Bl,
            T8Bl,
        }
    }
}
