﻿namespace fhs
{
    partial class DataViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.补偿X = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.补偿Y = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.补偿U = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cell偏差 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BL偏差 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.时间,
            this.补偿X,
            this.补偿Y,
            this.补偿U,
            this.Cell偏差,
            this.BL偏差});
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(812, 300);
            this.dataGridView1.TabIndex = 2;
            // 
            // 时间
            // 
            this.时间.HeaderText = "时间";
            this.时间.Name = "时间";
            this.时间.ReadOnly = true;
            this.时间.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // 补偿X
            // 
            this.补偿X.HeaderText = "补偿X";
            this.补偿X.Name = "补偿X";
            this.补偿X.ReadOnly = true;
            this.补偿X.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // 补偿Y
            // 
            this.补偿Y.HeaderText = "补偿Y";
            this.补偿Y.Name = "补偿Y";
            this.补偿Y.ReadOnly = true;
            this.补偿Y.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // 补偿U
            // 
            this.补偿U.HeaderText = "补偿U";
            this.补偿U.Name = "补偿U";
            this.补偿U.ReadOnly = true;
            this.补偿U.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Cell偏差
            // 
            this.Cell偏差.HeaderText = "Cell偏差";
            this.Cell偏差.Name = "Cell偏差";
            this.Cell偏差.ReadOnly = true;
            this.Cell偏差.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Bl偏差
            // 
            this.BL偏差.HeaderText = "BL偏差";
            this.BL偏差.Name = "BL偏差";
            this.BL偏差.ReadOnly = true;
            this.BL偏差.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // DataViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 300);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DataViewForm";
            this.Text = "DataViewForm";
            this.Load += new System.EventHandler(this.DataShowForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn 时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 补偿X;
        private System.Windows.Forms.DataGridViewTextBoxColumn 补偿Y;
        private System.Windows.Forms.DataGridViewTextBoxColumn 补偿U;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cell偏差;
        private System.Windows.Forms.DataGridViewTextBoxColumn BL偏差;
    }
}