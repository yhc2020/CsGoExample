﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Go;

namespace fhs
{
    class GlobalData
    {
        static public control_strand mainStrand;

        static public class 轴比例
        {
            static public double X = 10000;
            static public double Y = 10000;
            static public double U = 100000;

            static public double CellX1 = 100;
            static public double CellX2 = 100;
            static public double CellY = 100;

            static public double BlX1 = 100;
            static public double BlX2 = 100;
            static public double BlY = 100;

            static public double FeedIn = 10000;

            static public void Load()
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(@".\Config\RobotData.xml");
                    XmlElement root = doc.DocumentElement;
                    X = double.Parse(root.GetAttribute("X"));
                    Y = double.Parse(root.GetAttribute("Y"));
                    U = double.Parse(root.GetAttribute("U"));

                    CellX1 = double.Parse(root.GetAttribute("CellX1"));
                    CellX2 = double.Parse(root.GetAttribute("CellX2"));
                    CellY = double.Parse(root.GetAttribute("CellY"));

                    BlX1 = double.Parse(root.GetAttribute("BlX1"));
                    BlX2 = double.Parse(root.GetAttribute("BlX2"));
                    BlY = double.Parse(root.GetAttribute("BlY"));

                    FeedIn = double.Parse(root.GetAttribute("FeedIn"));
                }
                catch (Exception)
                {
                    LogMgr.Error("轴数据加载错误");
                }
            }
        }
    }
}
