﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    class InitOption
    {
        static public CameraControl T1View;
        static public CameraControl T2View;
        static public CameraControl T3View;
        static public CameraControl T4View;
        static public CameraControl T5View;
        static public CameraControl T6View;
        static public CameraControl T7View;
        static public CameraControl T8View;

        public InitOption()
        {

        }

        static public void InitProduct()
        {
            ProductMgr.Load();
        }

        static public void InitCameraShow(MatrixShowForm cameraView)
        {
            T1View = new CameraControl();
            T2View = new CameraControl();
            T3View = new CameraControl();
            T4View = new CameraControl();
            T5View = new CameraControl();
            T6View = new CameraControl();
            T7View = new CameraControl();
            T8View = new CameraControl();
            T1View.HighMode = true;
            T2View.HighMode = true;
            T3View.HighMode = true;
            T4View.HighMode = true;
            T5View.HighMode = true;
            T6View.HighMode = true;
            T7View.HighMode = true;
            T8View.HighMode = true;
            cameraView.InitView(2, 4);
            T5View.Text = T5View.Title = "BL T5";
            T6View.Text = T6View.Title = "BL T6";
            T7View.Text = T7View.Title = "BL T7";
            T8View.Text = T8View.Title = "BL T8";
            T1View.Text = T1View.Title = "Cell T1";
            T2View.Text = T2View.Title = "Cell T2";
            T3View.Text = T3View.Title = "Cell T3";
            T4View.Text = T4View.Title = "Cell T4";
            cameraView.Add("", T5View, 1, 0);
            cameraView.Add("", T6View, 1, 1);
            cameraView.Add("", T7View, 0, 1);
            cameraView.Add("", T8View, 0, 0);
            cameraView.Add("", T1View, 3, 0);
            cameraView.Add("", T2View, 3, 1);
            cameraView.Add("", T3View, 2, 1);
            cameraView.Add("", T4View, 2, 0);
        }

        static public void InitCamera()
        {
            CameraMgr.Load();
        }

        static public void InitVision()
        {
            VisionMgr.Load();
        }

        static public void InitCard()
        {
            CardMgr.Load();
        }

        static public void InitPoints()
        {
            PointMgr.Load();
        }

        static public void InitLight()
        {
            LightMgr.Load();
        }

        static public void InitIo()
        {
            IoMgr.Load();
        }

        static public void InitMotion()
        {
            MotionMgr.Load();
            GlobalData.轴比例.Load();
        }

        static public void InitComm()
        {
            CmdName.Load();
        }
    }
}
