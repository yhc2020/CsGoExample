﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    public partial class RobotPosForm : Form
    {
        public double posX;
        public double posY;
        public double posU;

        public RobotPosForm(bool enableXY, bool enableU)
        {
            InitializeComponent();
            numericUpDown_X.Enabled = numericUpDown_Y.Enabled = enableXY;
            numericUpDown_U.Enabled = enableU;
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            posX = (double)numericUpDown_X.Value;
            posY = (double)numericUpDown_Y.Value;
            posU = (double)numericUpDown_U.Value;
            Close();
        }
    }
}
