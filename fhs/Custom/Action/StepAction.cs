﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Go;

namespace fhs
{
    abstract class StepAction
    {
        static private generator _action;

        static public bool IsRun()
        {
            return null != _action;
        }

        static public void MakeAction(generator.action action, Action completedHandler)
        {
            if (IsRun() || CalibAction.IsRun() || AutoAction.IsRun())
            {
                return;
            }
            _action = generator.tgo(GlobalData.mainStrand, action, delegate ()
            {
                _action = null;
                completedHandler?.Invoke();
            });
        }

        static public void Stop()
        {
            _action?.stop();
        }
    }
}
