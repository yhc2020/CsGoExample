﻿namespace fhs
{
    partial class ProductParamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown_OffsetX = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown_OffsetY = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDown_OffsetU = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDown_ItCount = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown_ItErrDis = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown_ItErrAng = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDown_ErrorLen = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDown_MaxAngle = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.numericUpDown_MaxDis = new System.Windows.Forms.NumericUpDown();
            this.groupBox_Camera = new System.Windows.Forms.GroupBox();
            this.radioButton_1234 = new System.Windows.Forms.RadioButton();
            this.radioButton_24 = new System.Windows.Forms.RadioButton();
            this.radioButton_13 = new System.Windows.Forms.RadioButton();
            this.radioButton_41 = new System.Windows.Forms.RadioButton();
            this.radioButton_34 = new System.Windows.Forms.RadioButton();
            this.radioButton_23 = new System.Windows.Forms.RadioButton();
            this.radioButton_12 = new System.Windows.Forms.RadioButton();
            this.checkBox_T1Mark = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox_T4Mark = new System.Windows.Forms.CheckBox();
            this.checkBox_T3Mark = new System.Windows.Forms.CheckBox();
            this.checkBox_T2Mark = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.numericUpDown_BlDelay = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_CellDelay = new System.Windows.Forms.NumericUpDown();
            this.checkBox_BlInter = new System.Windows.Forms.CheckBox();
            this.checkBox_CellInter = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox_T8Mark = new System.Windows.Forms.CheckBox();
            this.checkBox_T7Mark = new System.Windows.Forms.CheckBox();
            this.checkBox_T6Mark = new System.Windows.Forms.CheckBox();
            this.checkBox_T5Mark = new System.Windows.Forms.CheckBox();
            this.pictureBox_Direct = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDown_AxisAngle = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_OffsetX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_OffsetY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_OffsetU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ItCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ItErrDis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ItErrAng)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ErrorLen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MaxAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MaxDis)).BeginInit();
            this.groupBox_Camera.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BlDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CellDelay)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Direct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AxisAngle)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "补偿X";
            // 
            // numericUpDown_OffsetX
            // 
            this.numericUpDown_OffsetX.DecimalPlaces = 2;
            this.numericUpDown_OffsetX.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDown_OffsetX.Location = new System.Drawing.Point(65, 3);
            this.numericUpDown_OffsetX.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDown_OffsetX.Name = "numericUpDown_OffsetX";
            this.numericUpDown_OffsetX.Size = new System.Drawing.Size(84, 21);
            this.numericUpDown_OffsetX.TabIndex = 1;
            this.numericUpDown_OffsetX.ValueChanged += new System.EventHandler(this.numericUpDown_OffsetX_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "补偿Y";
            // 
            // numericUpDown_OffsetY
            // 
            this.numericUpDown_OffsetY.DecimalPlaces = 2;
            this.numericUpDown_OffsetY.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDown_OffsetY.Location = new System.Drawing.Point(65, 27);
            this.numericUpDown_OffsetY.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDown_OffsetY.Name = "numericUpDown_OffsetY";
            this.numericUpDown_OffsetY.Size = new System.Drawing.Size(84, 21);
            this.numericUpDown_OffsetY.TabIndex = 2;
            this.numericUpDown_OffsetY.ValueChanged += new System.EventHandler(this.numericUpDown_OffsetY_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "补偿U";
            // 
            // numericUpDown_OffsetU
            // 
            this.numericUpDown_OffsetU.DecimalPlaces = 3;
            this.numericUpDown_OffsetU.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericUpDown_OffsetU.Location = new System.Drawing.Point(65, 51);
            this.numericUpDown_OffsetU.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDown_OffsetU.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.numericUpDown_OffsetU.Name = "numericUpDown_OffsetU";
            this.numericUpDown_OffsetU.Size = new System.Drawing.Size(84, 21);
            this.numericUpDown_OffsetU.TabIndex = 3;
            this.numericUpDown_OffsetU.ValueChanged += new System.EventHandler(this.numericUpDown_OffsetU_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "对位次数";
            // 
            // numericUpDown_ItCount
            // 
            this.numericUpDown_ItCount.Location = new System.Drawing.Point(65, 99);
            this.numericUpDown_ItCount.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_ItCount.Name = "numericUpDown_ItCount";
            this.numericUpDown_ItCount.Size = new System.Drawing.Size(84, 21);
            this.numericUpDown_ItCount.TabIndex = 4;
            this.numericUpDown_ItCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_ItCount.ValueChanged += new System.EventHandler(this.numericUpDown_ItCount_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "误差距离";
            // 
            // numericUpDown_ItErrDis
            // 
            this.numericUpDown_ItErrDis.DecimalPlaces = 2;
            this.numericUpDown_ItErrDis.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDown_ItErrDis.Location = new System.Drawing.Point(65, 123);
            this.numericUpDown_ItErrDis.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_ItErrDis.Name = "numericUpDown_ItErrDis";
            this.numericUpDown_ItErrDis.Size = new System.Drawing.Size(84, 21);
            this.numericUpDown_ItErrDis.TabIndex = 5;
            this.numericUpDown_ItErrDis.ValueChanged += new System.EventHandler(this.numericUpDown_ItErrDis_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "误差角度";
            // 
            // numericUpDown_ItErrAng
            // 
            this.numericUpDown_ItErrAng.DecimalPlaces = 2;
            this.numericUpDown_ItErrAng.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDown_ItErrAng.Location = new System.Drawing.Point(65, 147);
            this.numericUpDown_ItErrAng.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_ItErrAng.Name = "numericUpDown_ItErrAng";
            this.numericUpDown_ItErrAng.Size = new System.Drawing.Size(84, 21);
            this.numericUpDown_ItErrAng.TabIndex = 6;
            this.numericUpDown_ItErrAng.ValueChanged += new System.EventHandler(this.numericUpDown_ItErrAng_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "Mark超限";
            // 
            // numericUpDown_ErrorLen
            // 
            this.numericUpDown_ErrorLen.DecimalPlaces = 2;
            this.numericUpDown_ErrorLen.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDown_ErrorLen.Location = new System.Drawing.Point(65, 171);
            this.numericUpDown_ErrorLen.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_ErrorLen.Name = "numericUpDown_ErrorLen";
            this.numericUpDown_ErrorLen.Size = new System.Drawing.Size(84, 21);
            this.numericUpDown_ErrorLen.TabIndex = 7;
            this.numericUpDown_ErrorLen.ValueChanged += new System.EventHandler(this.numericUpDown_ErrorLen_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 198);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "角度超限";
            // 
            // numericUpDown_MaxAngle
            // 
            this.numericUpDown_MaxAngle.DecimalPlaces = 2;
            this.numericUpDown_MaxAngle.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDown_MaxAngle.Location = new System.Drawing.Point(65, 195);
            this.numericUpDown_MaxAngle.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDown_MaxAngle.Name = "numericUpDown_MaxAngle";
            this.numericUpDown_MaxAngle.Size = new System.Drawing.Size(84, 21);
            this.numericUpDown_MaxAngle.TabIndex = 8;
            this.numericUpDown_MaxAngle.ValueChanged += new System.EventHandler(this.numericUpDown_MaxAngle_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 222);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "距离超限";
            // 
            // numericUpDown_MaxDis
            // 
            this.numericUpDown_MaxDis.DecimalPlaces = 2;
            this.numericUpDown_MaxDis.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDown_MaxDis.Location = new System.Drawing.Point(65, 219);
            this.numericUpDown_MaxDis.Name = "numericUpDown_MaxDis";
            this.numericUpDown_MaxDis.Size = new System.Drawing.Size(84, 21);
            this.numericUpDown_MaxDis.TabIndex = 9;
            this.numericUpDown_MaxDis.ValueChanged += new System.EventHandler(this.numericUpDown_MaxDis_ValueChanged);
            // 
            // groupBox_Camera
            // 
            this.groupBox_Camera.Controls.Add(this.radioButton_1234);
            this.groupBox_Camera.Controls.Add(this.radioButton_24);
            this.groupBox_Camera.Controls.Add(this.radioButton_13);
            this.groupBox_Camera.Controls.Add(this.radioButton_41);
            this.groupBox_Camera.Controls.Add(this.radioButton_34);
            this.groupBox_Camera.Controls.Add(this.radioButton_23);
            this.groupBox_Camera.Controls.Add(this.radioButton_12);
            this.groupBox_Camera.Location = new System.Drawing.Point(291, 3);
            this.groupBox_Camera.Name = "groupBox_Camera";
            this.groupBox_Camera.Size = new System.Drawing.Size(124, 102);
            this.groupBox_Camera.TabIndex = 10;
            this.groupBox_Camera.TabStop = false;
            this.groupBox_Camera.Text = "对位相机";
            // 
            // radioButton_1234
            // 
            this.radioButton_1234.AutoSize = true;
            this.radioButton_1234.Checked = true;
            this.radioButton_1234.Location = new System.Drawing.Point(7, 81);
            this.radioButton_1234.Name = "radioButton_1234";
            this.radioButton_1234.Size = new System.Drawing.Size(89, 16);
            this.radioButton_1234.TabIndex = 0;
            this.radioButton_1234.TabStop = true;
            this.radioButton_1234.Text = "T1-T2-T3-T4";
            this.radioButton_1234.UseVisualStyleBackColor = true;
            this.radioButton_1234.CheckedChanged += new System.EventHandler(this.radioButton_1234_CheckedChanged);
            // 
            // radioButton_24
            // 
            this.radioButton_24.AutoSize = true;
            this.radioButton_24.Location = new System.Drawing.Point(66, 59);
            this.radioButton_24.Name = "radioButton_24";
            this.radioButton_24.Size = new System.Drawing.Size(53, 16);
            this.radioButton_24.TabIndex = 0;
            this.radioButton_24.Text = "T2-T4";
            this.radioButton_24.UseVisualStyleBackColor = true;
            this.radioButton_24.CheckedChanged += new System.EventHandler(this.radioButton_24_CheckedChanged);
            // 
            // radioButton_13
            // 
            this.radioButton_13.AutoSize = true;
            this.radioButton_13.Location = new System.Drawing.Point(7, 59);
            this.radioButton_13.Name = "radioButton_13";
            this.radioButton_13.Size = new System.Drawing.Size(53, 16);
            this.radioButton_13.TabIndex = 0;
            this.radioButton_13.Text = "T1-T3";
            this.radioButton_13.UseVisualStyleBackColor = true;
            this.radioButton_13.CheckedChanged += new System.EventHandler(this.radioButton_13_CheckedChanged);
            // 
            // radioButton_41
            // 
            this.radioButton_41.AutoSize = true;
            this.radioButton_41.Location = new System.Drawing.Point(66, 37);
            this.radioButton_41.Name = "radioButton_41";
            this.radioButton_41.Size = new System.Drawing.Size(53, 16);
            this.radioButton_41.TabIndex = 0;
            this.radioButton_41.Text = "T4-T1";
            this.radioButton_41.UseVisualStyleBackColor = true;
            this.radioButton_41.CheckedChanged += new System.EventHandler(this.radioButton_41_CheckedChanged);
            // 
            // radioButton_34
            // 
            this.radioButton_34.AutoSize = true;
            this.radioButton_34.Location = new System.Drawing.Point(7, 37);
            this.radioButton_34.Name = "radioButton_34";
            this.radioButton_34.Size = new System.Drawing.Size(53, 16);
            this.radioButton_34.TabIndex = 0;
            this.radioButton_34.Text = "T3-T4";
            this.radioButton_34.UseVisualStyleBackColor = true;
            this.radioButton_34.CheckedChanged += new System.EventHandler(this.radioButton_34_CheckedChanged);
            // 
            // radioButton_23
            // 
            this.radioButton_23.AutoSize = true;
            this.radioButton_23.Location = new System.Drawing.Point(66, 15);
            this.radioButton_23.Name = "radioButton_23";
            this.radioButton_23.Size = new System.Drawing.Size(53, 16);
            this.radioButton_23.TabIndex = 0;
            this.radioButton_23.Text = "T2-T3";
            this.radioButton_23.UseVisualStyleBackColor = true;
            this.radioButton_23.CheckedChanged += new System.EventHandler(this.radioButton_23_CheckedChanged);
            // 
            // radioButton_12
            // 
            this.radioButton_12.AutoSize = true;
            this.radioButton_12.Location = new System.Drawing.Point(7, 15);
            this.radioButton_12.Name = "radioButton_12";
            this.radioButton_12.Size = new System.Drawing.Size(53, 16);
            this.radioButton_12.TabIndex = 0;
            this.radioButton_12.Text = "T1-T2";
            this.radioButton_12.UseVisualStyleBackColor = true;
            this.radioButton_12.CheckedChanged += new System.EventHandler(this.radioButton_12_CheckedChanged);
            // 
            // checkBox_T1Mark
            // 
            this.checkBox_T1Mark.AutoSize = true;
            this.checkBox_T1Mark.Location = new System.Drawing.Point(6, 20);
            this.checkBox_T1Mark.Name = "checkBox_T1Mark";
            this.checkBox_T1Mark.Size = new System.Drawing.Size(36, 16);
            this.checkBox_T1Mark.TabIndex = 11;
            this.checkBox_T1Mark.Text = "T1";
            this.checkBox_T1Mark.UseVisualStyleBackColor = true;
            this.checkBox_T1Mark.CheckedChanged += new System.EventHandler(this.checkBox_T1Mark_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox_T4Mark);
            this.groupBox1.Controls.Add(this.checkBox_T3Mark);
            this.groupBox1.Controls.Add(this.checkBox_T2Mark);
            this.groupBox1.Controls.Add(this.checkBox_T1Mark);
            this.groupBox1.Location = new System.Drawing.Point(9, 294);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(171, 43);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cell使用Mark";
            // 
            // checkBox_T4Mark
            // 
            this.checkBox_T4Mark.AutoSize = true;
            this.checkBox_T4Mark.Location = new System.Drawing.Point(132, 20);
            this.checkBox_T4Mark.Name = "checkBox_T4Mark";
            this.checkBox_T4Mark.Size = new System.Drawing.Size(36, 16);
            this.checkBox_T4Mark.TabIndex = 11;
            this.checkBox_T4Mark.Text = "T4";
            this.checkBox_T4Mark.UseVisualStyleBackColor = true;
            this.checkBox_T4Mark.CheckedChanged += new System.EventHandler(this.checkBox_T4Mark_CheckedChanged);
            // 
            // checkBox_T3Mark
            // 
            this.checkBox_T3Mark.AutoSize = true;
            this.checkBox_T3Mark.Location = new System.Drawing.Point(90, 20);
            this.checkBox_T3Mark.Name = "checkBox_T3Mark";
            this.checkBox_T3Mark.Size = new System.Drawing.Size(36, 16);
            this.checkBox_T3Mark.TabIndex = 11;
            this.checkBox_T3Mark.Text = "T3";
            this.checkBox_T3Mark.UseVisualStyleBackColor = true;
            this.checkBox_T3Mark.CheckedChanged += new System.EventHandler(this.checkBox_T3Mark_CheckedChanged);
            // 
            // checkBox_T2Mark
            // 
            this.checkBox_T2Mark.AutoSize = true;
            this.checkBox_T2Mark.Location = new System.Drawing.Point(48, 20);
            this.checkBox_T2Mark.Name = "checkBox_T2Mark";
            this.checkBox_T2Mark.Size = new System.Drawing.Size(36, 16);
            this.checkBox_T2Mark.TabIndex = 11;
            this.checkBox_T2Mark.Text = "T2";
            this.checkBox_T2Mark.UseVisualStyleBackColor = true;
            this.checkBox_T2Mark.CheckedChanged += new System.EventHandler(this.checkBox_T2Mark_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(155, 150);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 14;
            this.label12.Text = "BL曝光延时";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(155, 174);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 12);
            this.label13.TabIndex = 14;
            this.label13.Text = "Cell首曝延时";
            // 
            // numericUpDown_BlDelay
            // 
            this.numericUpDown_BlDelay.Location = new System.Drawing.Point(238, 147);
            this.numericUpDown_BlDelay.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_BlDelay.Name = "numericUpDown_BlDelay";
            this.numericUpDown_BlDelay.Size = new System.Drawing.Size(76, 21);
            this.numericUpDown_BlDelay.TabIndex = 15;
            this.numericUpDown_BlDelay.ValueChanged += new System.EventHandler(this.numericUpDown_BlDelay_ValueChanged);
            // 
            // numericUpDown_CellDelay
            // 
            this.numericUpDown_CellDelay.Location = new System.Drawing.Point(238, 172);
            this.numericUpDown_CellDelay.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_CellDelay.Name = "numericUpDown_CellDelay";
            this.numericUpDown_CellDelay.Size = new System.Drawing.Size(76, 21);
            this.numericUpDown_CellDelay.TabIndex = 15;
            this.numericUpDown_CellDelay.ValueChanged += new System.EventHandler(this.numericUpDown_CellDelay_ValueChanged);
            // 
            // checkBox_BlInter
            // 
            this.checkBox_BlInter.AutoSize = true;
            this.checkBox_BlInter.Location = new System.Drawing.Point(186, 265);
            this.checkBox_BlInter.Name = "checkBox_BlInter";
            this.checkBox_BlInter.Size = new System.Drawing.Size(84, 16);
            this.checkBox_BlInter.TabIndex = 16;
            this.checkBox_BlInter.Text = "BL同边相连";
            this.checkBox_BlInter.UseVisualStyleBackColor = true;
            this.checkBox_BlInter.CheckedChanged += new System.EventHandler(this.checkBox_BlInter_CheckedChanged);
            // 
            // checkBox_CellInter
            // 
            this.checkBox_CellInter.AutoSize = true;
            this.checkBox_CellInter.Location = new System.Drawing.Point(186, 314);
            this.checkBox_CellInter.Name = "checkBox_CellInter";
            this.checkBox_CellInter.Size = new System.Drawing.Size(96, 16);
            this.checkBox_CellInter.TabIndex = 16;
            this.checkBox_CellInter.Text = "Cell同边相连";
            this.checkBox_CellInter.UseVisualStyleBackColor = true;
            this.checkBox_CellInter.CheckedChanged += new System.EventHandler(this.checkBox_CellInter_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBox_T8Mark);
            this.groupBox2.Controls.Add(this.checkBox_T7Mark);
            this.groupBox2.Controls.Add(this.checkBox_T6Mark);
            this.groupBox2.Controls.Add(this.checkBox_T5Mark);
            this.groupBox2.Location = new System.Drawing.Point(9, 245);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(171, 43);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "BL使用Mark";
            // 
            // checkBox_T8Mark
            // 
            this.checkBox_T8Mark.AutoSize = true;
            this.checkBox_T8Mark.Location = new System.Drawing.Point(132, 20);
            this.checkBox_T8Mark.Name = "checkBox_T8Mark";
            this.checkBox_T8Mark.Size = new System.Drawing.Size(36, 16);
            this.checkBox_T8Mark.TabIndex = 11;
            this.checkBox_T8Mark.Text = "T8";
            this.checkBox_T8Mark.UseVisualStyleBackColor = true;
            this.checkBox_T8Mark.CheckedChanged += new System.EventHandler(this.checkBox_T8Mark_CheckedChanged);
            // 
            // checkBox_T7Mark
            // 
            this.checkBox_T7Mark.AutoSize = true;
            this.checkBox_T7Mark.Location = new System.Drawing.Point(90, 20);
            this.checkBox_T7Mark.Name = "checkBox_T7Mark";
            this.checkBox_T7Mark.Size = new System.Drawing.Size(36, 16);
            this.checkBox_T7Mark.TabIndex = 11;
            this.checkBox_T7Mark.Text = "T7";
            this.checkBox_T7Mark.UseVisualStyleBackColor = true;
            this.checkBox_T7Mark.CheckedChanged += new System.EventHandler(this.checkBox_T7Mark_CheckedChanged);
            // 
            // checkBox_T6Mark
            // 
            this.checkBox_T6Mark.AutoSize = true;
            this.checkBox_T6Mark.Location = new System.Drawing.Point(48, 20);
            this.checkBox_T6Mark.Name = "checkBox_T6Mark";
            this.checkBox_T6Mark.Size = new System.Drawing.Size(36, 16);
            this.checkBox_T6Mark.TabIndex = 11;
            this.checkBox_T6Mark.Text = "T6";
            this.checkBox_T6Mark.UseVisualStyleBackColor = true;
            this.checkBox_T6Mark.CheckedChanged += new System.EventHandler(this.checkBox_T6Mark_CheckedChanged);
            // 
            // checkBox_T5Mark
            // 
            this.checkBox_T5Mark.AutoSize = true;
            this.checkBox_T5Mark.Location = new System.Drawing.Point(6, 20);
            this.checkBox_T5Mark.Name = "checkBox_T5Mark";
            this.checkBox_T5Mark.Size = new System.Drawing.Size(36, 16);
            this.checkBox_T5Mark.TabIndex = 11;
            this.checkBox_T5Mark.Text = "T5";
            this.checkBox_T5Mark.UseVisualStyleBackColor = true;
            this.checkBox_T5Mark.CheckedChanged += new System.EventHandler(this.checkBox_T5Mark_CheckedChanged);
            // 
            // pictureBox_Direct
            // 
            this.pictureBox_Direct.Image = global::fhs.Properties.Resources.direct;
            this.pictureBox_Direct.Location = new System.Drawing.Point(157, 3);
            this.pictureBox_Direct.Name = "pictureBox_Direct";
            this.pictureBox_Direct.Size = new System.Drawing.Size(128, 128);
            this.pictureBox_Direct.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_Direct.TabIndex = 17;
            this.pictureBox_Direct.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "轴角补偿";
            // 
            // numericUpDown_AxisAngle
            // 
            this.numericUpDown_AxisAngle.DecimalPlaces = 3;
            this.numericUpDown_AxisAngle.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericUpDown_AxisAngle.Location = new System.Drawing.Point(65, 75);
            this.numericUpDown_AxisAngle.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDown_AxisAngle.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.numericUpDown_AxisAngle.Name = "numericUpDown_AxisAngle";
            this.numericUpDown_AxisAngle.Size = new System.Drawing.Size(84, 21);
            this.numericUpDown_AxisAngle.TabIndex = 3;
            this.numericUpDown_AxisAngle.ValueChanged += new System.EventHandler(this.numericUpDown_AxisAngle_ValueChanged);
            // 
            // ProductParamForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 610);
            this.Controls.Add(this.pictureBox_Direct);
            this.Controls.Add(this.checkBox_CellInter);
            this.Controls.Add(this.checkBox_BlInter);
            this.Controls.Add(this.numericUpDown_CellDelay);
            this.Controls.Add(this.numericUpDown_BlDelay);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox_Camera);
            this.Controls.Add(this.numericUpDown_ItCount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numericUpDown_MaxDis);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.numericUpDown_MaxAngle);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numericUpDown_ErrorLen);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numericUpDown_ItErrAng);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numericUpDown_ItErrDis);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numericUpDown_AxisAngle);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.numericUpDown_OffsetU);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDown_OffsetY);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDown_OffsetX);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ProductParamForm";
            this.Text = "CompForm";
            this.Load += new System.EventHandler(this.ProductParamForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_OffsetX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_OffsetY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_OffsetU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ItCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ItErrDis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ItErrAng)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ErrorLen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MaxAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MaxDis)).EndInit();
            this.groupBox_Camera.ResumeLayout(false);
            this.groupBox_Camera.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BlDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CellDelay)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Direct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_AxisAngle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown_OffsetX;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown_OffsetY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDown_OffsetU;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDown_ItCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDown_ItErrDis;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown_ItErrAng;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDown_ErrorLen;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDown_MaxAngle;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericUpDown_MaxDis;
        private System.Windows.Forms.GroupBox groupBox_Camera;
        private System.Windows.Forms.RadioButton radioButton_1234;
        private System.Windows.Forms.RadioButton radioButton_24;
        private System.Windows.Forms.RadioButton radioButton_13;
        private System.Windows.Forms.RadioButton radioButton_41;
        private System.Windows.Forms.RadioButton radioButton_34;
        private System.Windows.Forms.RadioButton radioButton_23;
        private System.Windows.Forms.RadioButton radioButton_12;
        private System.Windows.Forms.CheckBox checkBox_T1Mark;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox_T4Mark;
        private System.Windows.Forms.CheckBox checkBox_T3Mark;
        private System.Windows.Forms.CheckBox checkBox_T2Mark;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown numericUpDown_BlDelay;
        private System.Windows.Forms.NumericUpDown numericUpDown_CellDelay;
        private System.Windows.Forms.CheckBox checkBox_BlInter;
        private System.Windows.Forms.CheckBox checkBox_CellInter;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBox_T8Mark;
        private System.Windows.Forms.CheckBox checkBox_T7Mark;
        private System.Windows.Forms.CheckBox checkBox_T6Mark;
        private System.Windows.Forms.CheckBox checkBox_T5Mark;
        private System.Windows.Forms.PictureBox pictureBox_Direct;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDown_AxisAngle;
    }
}