﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    internal partial class ProductParamForm : Form
    {
        RadioButton[] _selectCamera;
        ProductParam _compParam;

        public ProductParamForm()
        {
            InitializeComponent();
            _selectCamera = new RadioButton[7];
            _selectCamera[0] = radioButton_1234;
            _selectCamera[1] = radioButton_12;
            _selectCamera[2] = radioButton_23;
            _selectCamera[3] = radioButton_34;
            _selectCamera[4] = radioButton_41;
            _selectCamera[5] = radioButton_13;
            _selectCamera[6] = radioButton_24;
        }

        private void ProductParamForm_Load(object sender, EventArgs e)
        {
        }

        public void ReLoad(string productName)
        {
            _compParam = ProductMgr.products[productName];
            try
            {
                numericUpDown_OffsetX.Value = (decimal)_compParam.OffsetX;
                numericUpDown_OffsetY.Value = (decimal)_compParam.OffsetY;
                numericUpDown_OffsetU.Value = (decimal)FlatCalib.ToDeg(_compParam.OffsetU);
                numericUpDown_AxisAngle.Value = (decimal)FlatCalib.ToDeg(_compParam.AxisAngle);
                numericUpDown_ItCount.Value = _compParam.ItCount;
                numericUpDown_ItErrDis.Value = (decimal)_compParam.ItErrDis;
                numericUpDown_ItErrAng.Value = (decimal)_compParam.ItErrAng;
                numericUpDown_ErrorLen.Value = (decimal)_compParam.ErrLen;
                numericUpDown_MaxAngle.Value = (decimal)_compParam.MaxAngle;
                numericUpDown_MaxDis.Value = (decimal)_compParam.MaxDis;
                numericUpDown_BlDelay.Value = _compParam.BlDelay;
                numericUpDown_CellDelay.Value = _compParam.CellDelay;
                checkBox_BlInter.Checked = _compParam.BlInter;
                checkBox_CellInter.Checked = _compParam.CellInter;
                checkBox_BlInter.Enabled = checkBox_CellInter.Enabled = 0 == _compParam.SelectCamera;
                checkBox_T1Mark.Checked = _compParam.CellMarks[0];
                checkBox_T2Mark.Checked = _compParam.CellMarks[1];
                checkBox_T3Mark.Checked = _compParam.CellMarks[2];
                checkBox_T4Mark.Checked = _compParam.CellMarks[3];
                checkBox_T5Mark.Checked = _compParam.BlMarks[0];
                checkBox_T6Mark.Checked = _compParam.BlMarks[1];
                checkBox_T7Mark.Checked = _compParam.BlMarks[2];
                checkBox_T8Mark.Checked = _compParam.BlMarks[3];
                _selectCamera[_compParam.SelectCamera].Checked = true;
            }
            catch (Exception ec)
            {
                LogMgr.Error(ec.Message);
            }
        }

        private void numericUpDown_OffsetX_ValueChanged(object sender, EventArgs e)
        {
            _compParam.OffsetX = (double)numericUpDown_OffsetX.Value;
        }

        private void numericUpDown_OffsetY_ValueChanged(object sender, EventArgs e)
        {
            _compParam.OffsetY = (double)numericUpDown_OffsetY.Value;
        }

        private void numericUpDown_OffsetU_ValueChanged(object sender, EventArgs e)
        {
            _compParam.OffsetU = FlatCalib.ToRad((double)numericUpDown_OffsetU.Value);
        }

        private void numericUpDown_AxisAngle_ValueChanged(object sender, EventArgs e)
        {
            _compParam.AxisAngle = FlatCalib.ToRad((double)numericUpDown_AxisAngle.Value);
        }

        private void numericUpDown_ItCount_ValueChanged(object sender, EventArgs e)
        {
            _compParam.ItCount = (int)numericUpDown_ItCount.Value;
        }

        private void numericUpDown_ItErrDis_ValueChanged(object sender, EventArgs e)
        {
            _compParam.ItErrDis = (double)numericUpDown_ItErrDis.Value;
        }

        private void numericUpDown_ItErrAng_ValueChanged(object sender, EventArgs e)
        {
            _compParam.ItErrAng = (double)numericUpDown_ItErrAng.Value;
        }

        private void numericUpDown_ErrorLen_ValueChanged(object sender, EventArgs e)
        {
            _compParam.ErrLen = (double)numericUpDown_ErrorLen.Value;
        }

        private void numericUpDown_MaxAngle_ValueChanged(object sender, EventArgs e)
        {
            _compParam.MaxAngle = (double)numericUpDown_MaxAngle.Value;
        }

        private void numericUpDown_MaxDis_ValueChanged(object sender, EventArgs e)
        {
            _compParam.MaxDis = (double)numericUpDown_MaxDis.Value;
        }

        private void radioButton_12_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_12.Checked)
            {
                _compParam.SelectCamera = 1;
            }
        }

        private void radioButton_23_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_23.Checked)
            {
                _compParam.SelectCamera = 2;
            }
        }

        private void radioButton_34_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_34.Checked)
            {
                _compParam.SelectCamera = 3;
            }
        }

        private void radioButton_41_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_41.Checked)
            {
                _compParam.SelectCamera = 4;
            }
        }

        private void radioButton_13_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_13.Checked)
            {
                _compParam.SelectCamera = 5;
            }
        }

        private void radioButton_24_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_24.Checked)
            {
                _compParam.SelectCamera = 6;
            }
        }

        private void radioButton_1234_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_1234.Checked)
            {
                _compParam.SelectCamera = 0;
            }
            checkBox_BlInter.Enabled = checkBox_CellInter.Enabled = radioButton_1234.Checked;
        }

        private void checkBox_T1Mark_CheckedChanged(object sender, EventArgs e)
        {
            _compParam.CellMarks[0] = checkBox_T1Mark.Checked;
        }

        private void checkBox_T2Mark_CheckedChanged(object sender, EventArgs e)
        {
            _compParam.CellMarks[1] = checkBox_T2Mark.Checked;
        }

        private void checkBox_T3Mark_CheckedChanged(object sender, EventArgs e)
        {
            _compParam.CellMarks[2] = checkBox_T3Mark.Checked;
        }

        private void checkBox_T4Mark_CheckedChanged(object sender, EventArgs e)
        {
            _compParam.CellMarks[3] = checkBox_T4Mark.Checked;
        }

        private void checkBox_T5Mark_CheckedChanged(object sender, EventArgs e)
        {
            _compParam.BlMarks[0] = checkBox_T5Mark.Checked;
        }

        private void checkBox_T6Mark_CheckedChanged(object sender, EventArgs e)
        {
            _compParam.BlMarks[1] = checkBox_T6Mark.Checked;
        }

        private void checkBox_T7Mark_CheckedChanged(object sender, EventArgs e)
        {
            _compParam.BlMarks[2] = checkBox_T7Mark.Checked;
        }

        private void checkBox_T8Mark_CheckedChanged(object sender, EventArgs e)
        {
            _compParam.BlMarks[3] = checkBox_T8Mark.Checked;
        }

        private void numericUpDown_BlDelay_ValueChanged(object sender, EventArgs e)
        {
            _compParam.BlDelay = (int)numericUpDown_BlDelay.Value;
        }

        private void numericUpDown_CellDelay_ValueChanged(object sender, EventArgs e)
        {
            _compParam.CellDelay = (int)numericUpDown_CellDelay.Value;
        }

        private void checkBox_BlInter_CheckedChanged(object sender, EventArgs e)
        {
            _compParam.BlInter = checkBox_BlInter.Checked;
        }

        private void checkBox_CellInter_CheckedChanged(object sender, EventArgs e)
        {
            _compParam.CellInter = checkBox_CellInter.Checked;
        }
    }
}
