﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace fhs
{
    class ProductParam
    {
        public double OffsetX;
        public double OffsetY;
        public double OffsetU;
        public double AxisAngle;
        public double ItErrDis;
        public double ItErrAng;
        public double ErrLen;
        public double MaxAngle;
        public double MaxDis;
        public int ItCount;
        public int SelectCamera;
        public int BlDelay;
        public int CellDelay;
        public bool BlInter;
        public bool CellInter;
        public bool[] BlMarks = new bool[4];
        public bool[] CellMarks = new bool[4];

        static public ProductParam Load(string product)
        {
            ProductParam res = new ProductParam();
            string path = $"./Product/Config/{product}";
            res.OffsetX = DefaultLoad.Load(0.0, $"{path}/OffsetX.txt", 0);
            res.OffsetY = DefaultLoad.Load(0.0, $"{path}/OffsetY.txt", 0);
            res.OffsetU = DefaultLoad.Load(0.0, $"{path}/OffsetU.txt", 0);
            res.AxisAngle = DefaultLoad.Load(0.0, $"{path}/AxisAngle.txt", 0);
            res.ItErrDis = DefaultLoad.Load(0.05, $"{path}/ItErrDis.txt", 0);
            res.ItErrAng = DefaultLoad.Load(0.02, $"{path}/ItErrAng.txt", 0);
            res.ErrLen = DefaultLoad.Load(0.05, $"{path}/ErrLen.txt", 0);
            res.MaxAngle = DefaultLoad.Load(3.0, $"{path}/MaxAngle.txt", 0);
            res.MaxDis = DefaultLoad.Load(20.0, $"{path}/MaxDis.txt", 0);
            res.ItCount = DefaultLoad.Load(1, $"{path}/ItCount.txt", 0);
            res.SelectCamera = DefaultLoad.Load(0, $"{path}/SelectCamera.txt", 0);
            res.BlDelay = DefaultLoad.Load(0, $"{path}/BlDelay.txt", 0);
            res.CellDelay = DefaultLoad.Load(0, $"{path}/CellDelay.txt", 0);
            res.BlInter = DefaultLoad.Load(false, $"{path}/BlInter.txt", 0);
            res.CellInter = DefaultLoad.Load(false, $"{path}/CellInter.txt", 0);
            res.BlMarks[0] = DefaultLoad.Load(false, $"{path}/BlMarks.txt", 0);
            res.BlMarks[1] = DefaultLoad.Load(false, $"{path}/BlMarks.txt", 1);
            res.BlMarks[2] = DefaultLoad.Load(false, $"{path}/BlMarks.txt", 2);
            res.BlMarks[3] = DefaultLoad.Load(false, $"{path}/BlMarks.txt", 3);
            res.CellMarks[0] = DefaultLoad.Load(true, $"{path}/CellMarks.txt", 0);
            res.CellMarks[1] = DefaultLoad.Load(true, $"{path}/CellMarks.txt", 1);
            res.CellMarks[2] = DefaultLoad.Load(true, $"{path}/CellMarks.txt", 2);
            res.CellMarks[3] = DefaultLoad.Load(true, $"{path}/CellMarks.txt", 3);
            return res;
        }

        public void Save(string product)
        {
            try
            {
                string path = $"./Product/Config/{product}";
                Directory.CreateDirectory(path);
                File.WriteAllLines($"{path}/OffsetX.txt", new string[] { OffsetX.ToString() });
                File.WriteAllLines($"{path}/OffsetY.txt", new string[] { OffsetY.ToString() });
                File.WriteAllLines($"{path}/OffsetU.txt", new string[] { OffsetU.ToString() });
                File.WriteAllLines($"{path}/AxisAngle.txt", new string[] { AxisAngle.ToString() });
                File.WriteAllLines($"{path}/ItErrDis.txt", new string[] { ItErrDis.ToString() });
                File.WriteAllLines($"{path}/ItErrAng.txt", new string[] { ItErrAng.ToString() });
                File.WriteAllLines($"{path}/ErrLen.txt", new string[] { ErrLen.ToString() });
                File.WriteAllLines($"{path}/MaxAngle.txt", new string[] { MaxAngle.ToString() });
                File.WriteAllLines($"{path}/MaxDis.txt", new string[] { MaxDis.ToString() });
                File.WriteAllLines($"{path}/ItCount.txt", new string[] { ItCount.ToString() });
                File.WriteAllLines($"{path}/SelectCamera.txt", new string[] { SelectCamera.ToString() });
                File.WriteAllLines($"{path}/BlDelay.txt", new string[] { BlDelay.ToString() });
                File.WriteAllLines($"{path}/CellDelay.txt", new string[] { CellDelay.ToString() });
                File.WriteAllLines($"{path}/BlInter.txt", new string[] { BlInter.ToString() });
                File.WriteAllLines($"{path}/CellInter.txt", new string[] { CellInter.ToString() });
                File.WriteAllLines($"{path}/BlMarks.txt", new string[] { BlMarks[0].ToString(), BlMarks[1].ToString(), BlMarks[2].ToString(), BlMarks[3].ToString() });
                File.WriteAllLines($"{path}/CellMarks.txt", new string[] { CellMarks[0].ToString(), CellMarks[1].ToString(), CellMarks[2].ToString(), CellMarks[3].ToString() });
            }
            catch (System.Exception ex)
            {
                LogMgr.Error($"{product}参数保存异常{ex.Message}");
            }
        }
    }
}
