﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Go;

namespace fhs
{
    public partial class StateForm : Form
    {
        static public StateForm obj;
        generator _lightAction;
        bool _isWarning;

        public StateForm()
        {
            obj = this;
            _isWarning = false;
            InitializeComponent();
        }

        private void StateForm_Load(object sender, EventArgs e)
        {
            generator.go(GlobalData.mainStrand, RealTimeAction);
        }

        private async Task RealTimeAction()
        {
            while (true)
            {
                label_time.ForeColor = Color.Red;
                long lastTick = 0;
                while (true)
                {
                    DateTime now = DateTime.Now;
                    long currTick = now.Ticks / 10000000;
                    if (currTick != lastTick)
                    {
                        lastTick = currTick;
                        label_time.Text = now.ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    await generator.sleep(100);
                }
            }
        }

        private async Task FlashLight(Bitmap light)
        {
            while (true)
            {
                pictureBox_stateLight.Image = light;
                await generator.sleep(500);
                pictureBox_stateLight.Image = null;
                await generator.sleep(500);
            }
        }

        public void Run()
        {
            if (_isWarning)
            {
                return;
            }
            if (null != _lightAction)
            {
                _lightAction.stop(delegate ()
                {
                    pictureBox_stateLight.Image = Properties.Resources.light_green;
                    label_stateInfo.Text = "设备运行";
                    _lightAction = null;
                });
            }
            else
            {
                pictureBox_stateLight.Image = Properties.Resources.light_green;
                label_stateInfo.Text = "设备运行";
            }
        }

        public void Pause()
        {
            if (_isWarning)
            {
                return;
            }
            _lightAction?.stop();
            _lightAction = generator.tgo(GlobalData.mainStrand, functional.bind(FlashLight, Properties.Resources.light_green));
            label_stateInfo.Text = "设备暂停";
        }

        public void Stop()
        {
            if (_isWarning)
            {
                return;
            }
            if (null != _lightAction)
            {
                _lightAction.stop(delegate ()
                {
                    pictureBox_stateLight.Image = Properties.Resources.light_gray;
                    label_stateInfo.Text = "设备停止";
                    _lightAction = null;
                });
            }
            else
            {
                pictureBox_stateLight.Image = Properties.Resources.light_gray;
                label_stateInfo.Text = "设备停止";
            }
        }

        public void Clear()
        {
            _isWarning = false;
            pictureBox_stateLight.Image = Properties.Resources.light_gray;
            label_stateInfo.Text = null;
        }

        public void Warning(string info)
        {
            _isWarning = true;
            _lightAction?.stop();
            _lightAction = generator.tgo(GlobalData.mainStrand, functional.bind(FlashLight, Properties.Resources.light_red));
            label_stateInfo.Text = info;
        }

        private void button_log_Click(object sender, EventArgs e)
        {
            LogViewForm.obj.Activate();
            LogViewForm.obj.Show();
        }

        private void button_logFile_Click(object sender, EventArgs e)
        {
            try
            {
                string editPlus = @"C:\Program Files (x86)\EditPlus 3\EditPlus.exe";
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(File.Exists(editPlus) ? editPlus : "notepad.exe");
                psi.Arguments = $@"{System.Environment.CurrentDirectory}\fhs.log";
                System.Diagnostics.Process.Start(psi);
            }
            catch (System.Exception) { }
        }
    }
}
