﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace fhs
{
    class CameraMgr
    {
        [Serializable]
        public enum Type
        {
            GigEVision,
            GigEVision2,
            DirectShow
        }

        [Serializable]
        public enum Brand
        {
            HIKVISION,
            BASLER,
            DAHUA,
            DALSA,
        }

        static public readonly Dictionary<VisionName.Camera, CameraBase> cameras = new Dictionary<VisionName.Camera, CameraBase>();

        static public void Load()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@".\Config\Camera.xml");
            XmlElement root = doc.DocumentElement;
            foreach (XmlNode cameraItem in root.ChildNodes)
            {
                string cameraNameStr = ((XmlElement)cameraItem).GetAttribute("Name");
                VisionName.Camera cameraName = (VisionName.Camera)(-1);
                if (StringToEnum.Convert(cameraNameStr, ref cameraName))
                {
                    if (cameras.ContainsKey(cameraName))
                    {
                        LogMgr.Error($"{cameraNameStr} 相机名称重定义");
                        continue;
                    }
                    string cameraTypeStr = ((XmlElement)cameraItem).GetAttribute("Type");
                    string cameraId = ((XmlElement)cameraItem).GetAttribute("Id");
                    string cameraBrandStr = ((XmlElement)cameraItem).GetAttribute("Brand");
                    string cameraIPStr = ((XmlElement)cameraItem).GetAttribute("ForceIP");
                    string cameraRotate = ((XmlElement)cameraItem).GetAttribute("Rotate");
                    string cameraReflect = ((XmlElement)cameraItem).GetAttribute("Reflect");
                    string cameraImmediately = ((XmlElement)cameraItem).GetAttribute("Immediately");
                    try
                    {
                        CameraMgr.Type cameraType = (CameraMgr.Type)(-1);
                        CameraMgr.Brand cameraBrand = (CameraMgr.Brand)(-1);
                        if (StringToEnum.Convert(cameraTypeStr, ref cameraType) && StringToEnum.Convert(cameraBrandStr, ref cameraBrand))
                        {
                            CameraBase camera = null;
                            switch (cameraBrand)
                            {
                                case Brand.HIKVISION:
                                    camera = new Camera_HIKVISION(cameraName, cameraType, cameraIPStr, cameraId);
                                    break;
                                case Brand.DAHUA:
                                    camera = new Camera_DAHUA(cameraName, cameraType, cameraIPStr, cameraId);
                                    break;
                                case Brand.DALSA:
                                    camera = new Camera_DALSE(cameraName, cameraType, cameraIPStr, cameraId);
                                    break;
                                default:
                                    break;
                            }
                            if (cameraRotate.Length != 0)
                            {
                                camera.Rotate = ParseDefault.Double(cameraRotate, 0);
                            }
                            if (cameraReflect.Length != 0)
                            {
                                camera.Reflect = ParseDefault.Double(cameraReflect, 0);
                            }
                            if (cameraImmediately.Length != 0)
                            {
                                camera.Immediately = ParseDefault.Bool(cameraImmediately, true);
                            }
                            cameras[cameraName] = camera;
                        }
                        else
                        {
                            LogMgr.Error($"{cameraTypeStr}.{cameraBrandStr} 相机类型品牌未定义");
                        }
                    }
                    catch (Exception)
                    {
                        LogMgr.Error($"{cameraNameStr}.{cameraBrandStr}.{cameraTypeStr}.{cameraId} 相机初始化错误");
                    }
                }
                else
                {
                    LogMgr.Error($"{cameraNameStr} 相机未定义");
                }
            }
        }
    }
}
