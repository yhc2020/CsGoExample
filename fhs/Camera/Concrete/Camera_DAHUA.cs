﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HalconDotNet;

namespace fhs
{
    class Camera_DAHUA : CameraBase
    {
        public Camera_DAHUA(VisionName.Camera name,
            CameraMgr.Type type,
            string forceIP,
            string deviceName) : base(name, type, forceIP, deviceName) { }

    }
}
