﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HalconDotNet;

namespace fhs
{
    class Camera_DALSE : CameraBase
    {
        public enum Selector
        {
            LineStart,
            FrameStart,
        }

        public enum Line
        {
            Line1,
            Line2,
        }

        public Camera_DALSE(VisionName.Camera name,
            CameraMgr.Type type,
            string forceIP,
            string deviceName) : base(name, type, forceIP, deviceName) { }
 
        public void TriggerSelector(Selector selector)
        {
            SetParam("TriggerSelector", selector.ToString());
        }

        public void LineSelector(Line line)
        {
            SetParam("LineSelector", line.ToString());
        }

        public void LineRate(int rate)
        {
            SetParam("AcquisitionLineRate", rate);
        }
    }
}
