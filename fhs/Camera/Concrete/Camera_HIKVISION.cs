﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HalconDotNet;

namespace fhs
{
    class Camera_HIKVISION : CameraBase
    {
        public Camera_HIKVISION(VisionName.Camera name,
            CameraMgr.Type type,
            string forceIP,
            string deviceName) : base(name, type, forceIP, deviceName) { }

    }
}
