﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    abstract class LightBase
    {
        public struct Color
        {
            public double Red;
            public double Green;
            public double Blue;
        }

        public abstract void SetBrightness(double num);

        public virtual void SetColor(Color color)
        {

        }
    }
}
