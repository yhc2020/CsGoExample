﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace fhs
{
    class LightMgr
    {
        static readonly public Dictionary<LightName.Light, LightBase> lights = new Dictionary<LightName.Light, LightBase>();

        static public void Load()
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(@".\Config\Light.xml");
            }
            catch (Exception ec)
            {
                LogMgr.Error($"读取光源异常:{ec.Message}");
                return;
            }
            XmlElement root = doc.DocumentElement;
            foreach (XmlNode lightItem in root.ChildNodes)
            {
                string lightNameStr = ((XmlElement)lightItem).GetAttribute("Name");
                LightName.Light lightName = (LightName.Light)(-1);
                if (StringToEnum.Convert(lightNameStr, ref lightName))
                {
                    string type = ((XmlElement)lightItem).GetAttribute("Type");
                    LightBase light = null;
                    switch (type)
                    {
                        case "IO":
                            string idStr = ((XmlElement)lightItem).GetAttribute("Id");
                            IoName.Out id = (IoName.Out)(-1);
                            if (StringToEnum.Convert(idStr, ref id))
                            {
                                light = new Light_IO(id);
                            }
                            else
                            {
                                LogMgr.Error($"{type}.{idStr} 光源未定义");
                            }
                            break;
                        case "OPT_COM":
                        default:
                            LogMgr.Error($"{type} 光源类型未定义");
                            break;
                    }
                    if (null != light)
                    {
                        lights[lightName] = light;
                    }
                }
                else
                {
                    LogMgr.Error($"{lightNameStr} 未定义");
                }
            }
        }
    }
}
