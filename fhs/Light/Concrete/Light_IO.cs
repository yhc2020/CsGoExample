﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    class Light_IO : LightBase
    {
        IoName.Out _id;

        public Light_IO(IoName.Out id)
        {
            _id = id;
        }

        public override void SetBrightness(double num)
        {
            IoMgr.Outs[_id].Stat = num > 0 ? OutBase.State.高 : OutBase.State.低;
        }
    }
}
