﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Go;

namespace fhs
{
    partial class MatrixShowForm : Form
    {
        Bitmap _defaultShow;

        public MatrixShowForm(Bitmap defaultShow)
        {
            _defaultShow = defaultShow;
            InitializeComponent();
        }

        private void CameraShowForm_Load(object sender, EventArgs e)
        {
        }

        public void InitView(int rows, int cols)
        {
            if (rows > panel_camera.RowCount)
            {
                for (int i = panel_camera.RowCount; i < rows; i++)
                {
                    panel_camera.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                }
                panel_camera.RowCount = rows;
            }
            else if (rows < panel_camera.RowCount)
            {
                for (int i = panel_camera.RowCount; i > rows; i--)
                {
                    panel_camera.RowStyles.RemoveAt(panel_camera.RowStyles.Count - 1);
                }
                panel_camera.RowCount = rows;
            }

            if (cols > panel_camera.ColumnCount)
            {
                for (int i = panel_camera.ColumnCount; i < cols; i++)
                {
                    panel_camera.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
                }
                panel_camera.ColumnCount = cols;
            }
            else if (cols < panel_camera.ColumnCount)
            {
                for (int i = panel_camera.ColumnCount; i > cols; i--)
                {
                    panel_camera.ColumnStyles.RemoveAt(panel_camera.ColumnStyles.Count - 1);
                }
                panel_camera.ColumnCount = cols;
            }
            panel_camera.Controls.Clear();
            int width = panel_camera.Width;
            int height = panel_camera.Height;
            for (int row = 0; row < panel_camera.RowCount; row++)
            {
                for (int col = 0; col < panel_camera.ColumnCount; col++)
                {
                    PictureBox pictBox = new PictureBox();
                    pictBox.Image = _defaultShow;
                    pictBox.SizeMode = PictureBoxSizeMode.StretchImage;
                    pictBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
                    pictBox.Margin = new Padding(2);
                    pictBox.Name = (row * panel_camera.ColumnCount + col).ToString();
                    panel_camera.Controls.Add(pictBox, col, row);
                    //panel_camera.ColumnStyles[col].Width = width / panel_camera.ColumnCount;
                    //panel_camera.RowStyles[col].Height = height / panel_camera.RowCount;
                }
            }
        }

        private void AddThen(string lableTxt, Control ctrl, int col, int row)
        {
            string key = (row * panel_camera.ColumnCount + col).ToString();
            panel_camera.Controls.RemoveByKey(key);
            ctrl.Name = key;
            ctrl.Width = (int)panel_camera.ColumnStyles[col].Width;
            ctrl.Height = (int)panel_camera.RowStyles[row].Height;
            ctrl.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            ctrl.Margin = new Padding(2);

            if (null != lableTxt)
            {
                Label label = new Label();
                label.BackColor = Color.Transparent;
                label.Parent = ctrl;
                label.Text = lableTxt;
            }

            panel_camera.Controls.Add(ctrl, col, row);
        }

        public void Add(string lableTxt, Control ctrl, int col, int row)
        {
            ctrl.DoubleClick += new EventHandler((object sender, EventArgs e) => DoubleEventClick(col, row, sender, e));
            AddThen(lableTxt, ctrl, col, row);
        }

        public void Add(string lableTxt, CameraControl ctrl, int col, int row)
        {
            ctrl.DoubleClick += new EventHandler((object sender, EventArgs e) => DoubleEventClick(col, row, sender, e));
            AddThen(lableTxt, ctrl, col, row);
        }

        private void DoubleEventClick(int currcol, int currrow, object sender, EventArgs e)
        {
            if (panel_camera.ColumnStyles[currcol].Width != panel_camera.Width
                || panel_camera.RowStyles[currrow].Height != panel_camera.Height)
            {
                for (int row = 0; row < panel_camera.RowCount; row++)
                {
                    for (int col = 0; col < panel_camera.ColumnCount; col++)
                    {
                        panel_camera.ColumnStyles[col].Width = 0;
                        panel_camera.RowStyles[row].Height = 0;
                    }
                }
                panel_camera.ColumnStyles[currcol].Width = panel_camera.Width;
                panel_camera.RowStyles[currrow].Height = panel_camera.Height;
            }
            else
            {
                for (int row = 0; row < panel_camera.RowCount; row++)
                {
                    for (int col = 0; col < panel_camera.ColumnCount; col++)
                    {
                        panel_camera.ColumnStyles[col].Width = panel_camera.Width / panel_camera.ColumnCount;
                        panel_camera.RowStyles[row].Height = panel_camera.Height / panel_camera.RowCount;
                    }
                }
            }
        }
    }
}
