﻿using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using HalconDotNet;

#if ENABLE_WIN_HOOK
namespace fhs
{
    abstract public class HDevWindowStack
    {
        static readonly ThreadLocal<LinkedList<HTuple>> winStack = new ThreadLocal<LinkedList<HTuple>>();

        private static LinkedList<HTuple> Stack
        {
            get
            {
                LinkedList<HTuple> stack = winStack.Value;
                if (null == stack)
                {
                    stack = new LinkedList<HTuple>();
                    winStack.Value = stack;
                }
                return stack;
            }
        }

        public static void SetActive(HTuple win_handle)
        {
            LinkedList<HTuple> stack = Stack;
            Trace.Assert(0 != stack.Count, "队列为空，不能进行GetActive操作");
            stack.Last.Value = win_handle;
        }

        public static HTuple GetActive()
        {
            LinkedList<HTuple> stack = Stack;
            Trace.Assert(0 != stack.Count, "队列为空，不能进行GetActive操作");
            return stack.Last.Value;
        }

        public static bool IsOpen()
        {
            return true;
        }

        public static HTuple Pop()
        {
            LinkedList<HTuple> stack = Stack;
            Trace.Assert(0 != stack.Count, "队列为空，不能进行Pop操作");
            HTuple win = stack.Last.Value;
            stack.RemoveLast();
            return win;
        }

        public static void Push(HTuple win_handle)
        {
            LinkedList<HTuple> stack = Stack;
            stack.AddLast(win_handle);
        }
    }
}

public partial class HDevelopExport
{
    abstract public class HDevWindowStack : fhs.HDevWindowStack { }
}

namespace HalconDotNet
{
    abstract public class HDevWindowStack : fhs.HDevWindowStack { }
}
#endif