﻿namespace fhs
{
    partial class EnumCamerasForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_Info = new System.Windows.Forms.TextBox();
            this.comboBox_Name = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_Query = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button_Enum = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox_Info
            // 
            this.textBox_Info.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Info.Location = new System.Drawing.Point(12, 36);
            this.textBox_Info.Multiline = true;
            this.textBox_Info.Name = "textBox_Info";
            this.textBox_Info.ReadOnly = true;
            this.textBox_Info.Size = new System.Drawing.Size(844, 619);
            this.textBox_Info.TabIndex = 0;
            // 
            // comboBox_Name
            // 
            this.comboBox_Name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Name.FormattingEnabled = true;
            this.comboBox_Name.Location = new System.Drawing.Point(47, 10);
            this.comboBox_Name.Name = "comboBox_Name";
            this.comboBox_Name.Size = new System.Drawing.Size(121, 20);
            this.comboBox_Name.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "类型";
            // 
            // comboBox_Query
            // 
            this.comboBox_Query.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Query.FormattingEnabled = true;
            this.comboBox_Query.Location = new System.Drawing.Point(215, 10);
            this.comboBox_Query.Name = "comboBox_Query";
            this.comboBox_Query.Size = new System.Drawing.Size(121, 20);
            this.comboBox_Query.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(180, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "信息";
            // 
            // button_Enum
            // 
            this.button_Enum.Location = new System.Drawing.Point(353, 9);
            this.button_Enum.Name = "button_Enum";
            this.button_Enum.Size = new System.Drawing.Size(75, 23);
            this.button_Enum.TabIndex = 3;
            this.button_Enum.Text = "查询";
            this.button_Enum.UseVisualStyleBackColor = true;
            this.button_Enum.Click += new System.EventHandler(this.button_Enum_Click);
            // 
            // EnumCamerasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 667);
            this.Controls.Add(this.button_Enum);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox_Query);
            this.Controls.Add(this.comboBox_Name);
            this.Controls.Add(this.textBox_Info);
            this.Name = "EnumCamerasForm";
            this.Text = "EnumCameras";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_Info;
        private System.Windows.Forms.ComboBox comboBox_Name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_Query;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_Enum;
    }
}

