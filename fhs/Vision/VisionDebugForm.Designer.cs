﻿namespace fhs
{
    partial class VisionDebugForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_Select = new System.Windows.Forms.Panel();
            this.button_refresh = new System.Windows.Forms.Button();
            this.button_Open = new System.Windows.Forms.Button();
            this.textBox_ImageGray = new System.Windows.Forms.TextBox();
            this.button_saveDump = new System.Windows.Forms.Button();
            this.button_saveAll = new System.Windows.Forms.Button();
            this.button_save = new System.Windows.Forms.Button();
            this.button_stop = new System.Windows.Forms.Button();
            this.button_snap = new System.Windows.Forms.Button();
            this.button_real = new System.Windows.Forms.Button();
            this.comboBoxVision = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_EditForm = new System.Windows.Forms.Panel();
            this.panel_Camera = new System.Windows.Forms.Panel();
            this.vScrollBar_Preview = new System.Windows.Forms.VScrollBar();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel_preview = new System.Windows.Forms.TableLayoutPanel();
            this.vScrollBar_Params = new System.Windows.Forms.VScrollBar();
            this.hScrollBar_Params = new System.Windows.Forms.HScrollBar();
            this.panel_Select.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Select
            // 
            this.panel_Select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Select.Controls.Add(this.button_refresh);
            this.panel_Select.Controls.Add(this.button_Open);
            this.panel_Select.Controls.Add(this.textBox_ImageGray);
            this.panel_Select.Controls.Add(this.button_saveDump);
            this.panel_Select.Controls.Add(this.button_saveAll);
            this.panel_Select.Controls.Add(this.button_save);
            this.panel_Select.Controls.Add(this.button_stop);
            this.panel_Select.Controls.Add(this.button_snap);
            this.panel_Select.Controls.Add(this.button_real);
            this.panel_Select.Controls.Add(this.comboBoxVision);
            this.panel_Select.Controls.Add(this.label1);
            this.panel_Select.Location = new System.Drawing.Point(733, 9);
            this.panel_Select.Margin = new System.Windows.Forms.Padding(0);
            this.panel_Select.Name = "panel_Select";
            this.panel_Select.Size = new System.Drawing.Size(296, 118);
            this.panel_Select.TabIndex = 0;
            // 
            // button_refresh
            // 
            this.button_refresh.Location = new System.Drawing.Point(249, 91);
            this.button_refresh.Name = "button_refresh";
            this.button_refresh.Size = new System.Drawing.Size(44, 23);
            this.button_refresh.TabIndex = 10;
            this.button_refresh.Text = "刷新";
            this.button_refresh.UseVisualStyleBackColor = true;
            this.button_refresh.Click += new System.EventHandler(this.button_refresh_Click);
            // 
            // button_Open
            // 
            this.button_Open.Location = new System.Drawing.Point(210, 5);
            this.button_Open.Name = "button_Open";
            this.button_Open.Size = new System.Drawing.Size(83, 23);
            this.button_Open.TabIndex = 9;
            this.button_Open.Text = "导入";
            this.button_Open.UseVisualStyleBackColor = true;
            this.button_Open.Click += new System.EventHandler(this.button_Open_Click);
            // 
            // textBox_ImageGray
            // 
            this.textBox_ImageGray.Location = new System.Drawing.Point(14, 92);
            this.textBox_ImageGray.Name = "textBox_ImageGray";
            this.textBox_ImageGray.ReadOnly = true;
            this.textBox_ImageGray.Size = new System.Drawing.Size(230, 21);
            this.textBox_ImageGray.TabIndex = 8;
            // 
            // button_saveDump
            // 
            this.button_saveDump.Location = new System.Drawing.Point(210, 63);
            this.button_saveDump.Name = "button_saveDump";
            this.button_saveDump.Size = new System.Drawing.Size(83, 23);
            this.button_saveDump.TabIndex = 7;
            this.button_saveDump.Text = "截图";
            this.button_saveDump.UseVisualStyleBackColor = true;
            this.button_saveDump.Click += new System.EventHandler(this.button_saveDump_Click);
            // 
            // button_saveAll
            // 
            this.button_saveAll.Location = new System.Drawing.Point(112, 63);
            this.button_saveAll.Name = "button_saveAll";
            this.button_saveAll.Size = new System.Drawing.Size(83, 23);
            this.button_saveAll.TabIndex = 6;
            this.button_saveAll.Text = "保存所有";
            this.button_saveAll.UseVisualStyleBackColor = true;
            this.button_saveAll.Click += new System.EventHandler(this.button_saveAll_Click);
            // 
            // button_save
            // 
            this.button_save.Enabled = false;
            this.button_save.Location = new System.Drawing.Point(13, 63);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(83, 23);
            this.button_save.TabIndex = 5;
            this.button_save.Text = "保存";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // button_stop
            // 
            this.button_stop.Location = new System.Drawing.Point(210, 34);
            this.button_stop.Name = "button_stop";
            this.button_stop.Size = new System.Drawing.Size(84, 23);
            this.button_stop.TabIndex = 4;
            this.button_stop.Text = "停止";
            this.button_stop.UseVisualStyleBackColor = true;
            this.button_stop.Click += new System.EventHandler(this.button_stop_Click);
            // 
            // button_snap
            // 
            this.button_snap.Location = new System.Drawing.Point(111, 34);
            this.button_snap.Name = "button_snap";
            this.button_snap.Size = new System.Drawing.Size(84, 23);
            this.button_snap.TabIndex = 3;
            this.button_snap.Text = "单次";
            this.button_snap.UseVisualStyleBackColor = true;
            this.button_snap.Click += new System.EventHandler(this.button_snap_Click);
            // 
            // button_real
            // 
            this.button_real.Location = new System.Drawing.Point(12, 34);
            this.button_real.Name = "button_real";
            this.button_real.Size = new System.Drawing.Size(84, 23);
            this.button_real.TabIndex = 2;
            this.button_real.Text = "实时";
            this.button_real.UseVisualStyleBackColor = true;
            this.button_real.Click += new System.EventHandler(this.button_real_Click);
            // 
            // comboBoxVision
            // 
            this.comboBoxVision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxVision.FormattingEnabled = true;
            this.comboBoxVision.Location = new System.Drawing.Point(57, 7);
            this.comboBoxVision.Name = "comboBoxVision";
            this.comboBoxVision.Size = new System.Drawing.Size(137, 20);
            this.comboBoxVision.TabIndex = 1;
            this.comboBoxVision.SelectedIndexChanged += new System.EventHandler(this.comboBoxVision_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "视觉";
            // 
            // panel_EditForm
            // 
            this.panel_EditForm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_EditForm.Location = new System.Drawing.Point(733, 130);
            this.panel_EditForm.Name = "panel_EditForm";
            this.panel_EditForm.Size = new System.Drawing.Size(276, 604);
            this.panel_EditForm.TabIndex = 3;
            // 
            // panel_Camera
            // 
            this.panel_Camera.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Camera.Location = new System.Drawing.Point(2, 9);
            this.panel_Camera.Name = "panel_Camera";
            this.panel_Camera.Size = new System.Drawing.Size(580, 750);
            this.panel_Camera.TabIndex = 4;
            // 
            // vScrollBar_Preview
            // 
            this.vScrollBar_Preview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vScrollBar_Preview.LargeChange = 1;
            this.vScrollBar_Preview.Location = new System.Drawing.Point(716, 9);
            this.vScrollBar_Preview.Name = "vScrollBar_Preview";
            this.vScrollBar_Preview.Size = new System.Drawing.Size(17, 750);
            this.vScrollBar_Preview.TabIndex = 6;
            this.vScrollBar_Preview.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar_Preview_Scroll);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.tableLayoutPanel_preview);
            this.panel2.Location = new System.Drawing.Point(585, 9);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(128, 750);
            this.panel2.TabIndex = 7;
            // 
            // tableLayoutPanel_preview
            // 
            this.tableLayoutPanel_preview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel_preview.ColumnCount = 2;
            this.tableLayoutPanel_preview.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel_preview.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel_preview.Location = new System.Drawing.Point(1, 0);
            this.tableLayoutPanel_preview.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel_preview.Name = "tableLayoutPanel_preview";
            this.tableLayoutPanel_preview.RowCount = 32;
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_preview.Size = new System.Drawing.Size(126, 3500);
            this.tableLayoutPanel_preview.TabIndex = 5;
            // 
            // vScrollBar_Params
            // 
            this.vScrollBar_Params.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vScrollBar_Params.Location = new System.Drawing.Point(1012, 131);
            this.vScrollBar_Params.Maximum = 1000;
            this.vScrollBar_Params.Name = "vScrollBar_Params";
            this.vScrollBar_Params.Size = new System.Drawing.Size(17, 603);
            this.vScrollBar_Params.TabIndex = 8;
            this.vScrollBar_Params.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar_Params_Scroll);
            // 
            // hScrollBar_Params
            // 
            this.hScrollBar_Params.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.hScrollBar_Params.Location = new System.Drawing.Point(733, 741);
            this.hScrollBar_Params.Maximum = 500;
            this.hScrollBar_Params.Name = "hScrollBar_Params";
            this.hScrollBar_Params.Size = new System.Drawing.Size(294, 17);
            this.hScrollBar_Params.TabIndex = 9;
            this.hScrollBar_Params.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar_Params_Scroll);
            // 
            // VisionDebugForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 768);
            this.Controls.Add(this.hScrollBar_Params);
            this.Controls.Add(this.vScrollBar_Params);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.vScrollBar_Preview);
            this.Controls.Add(this.panel_Camera);
            this.Controls.Add(this.panel_EditForm);
            this.Controls.Add(this.panel_Select);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VisionDebugForm";
            this.Text = "VisionDebugForm";
            this.Load += new System.EventHandler(this.CameraDebugForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CameraDebugForm_Paint);
            this.panel_Select.ResumeLayout(false);
            this.panel_Select.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel_Select;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxVision;
        private System.Windows.Forms.Button button_stop;
        private System.Windows.Forms.Button button_snap;
        private System.Windows.Forms.Button button_real;
        private System.Windows.Forms.Panel panel_EditForm;
        private System.Windows.Forms.Panel panel_Camera;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.Button button_saveAll;
        private System.Windows.Forms.Button button_saveDump;
        private System.Windows.Forms.TextBox textBox_ImageGray;
        private System.Windows.Forms.Button button_Open;
        private System.Windows.Forms.VScrollBar vScrollBar_Preview;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_preview;
        private System.Windows.Forms.Button button_refresh;
        private System.Windows.Forms.VScrollBar vScrollBar_Params;
        private System.Windows.Forms.HScrollBar hScrollBar_Params;
    }
}