﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HalconDotNet;

namespace fhs
{
    partial class EnumCamerasForm : Form
    {
        static string[] names = new string[] { "GigEVision", "GigEVision2", "DirectShow" };
        static string[] queries = new string[] { "device", "info_boards" };

        bool _enumSign = false;

        public EnumCamerasForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < names.Length; i++)
            {
                comboBox_Name.Items.Add(names[i]);
            }
            for (int i = 0; i < queries.Length; i++)
            {
                comboBox_Query.Items.Add(queries[i]);
            }
            comboBox_Name.SelectedIndex = 0;
            comboBox_Query.SelectedIndex = 0;
        }

        private void button_Enum_Click(object sender, EventArgs e)
        {
            if (_enumSign)
            {
                return;
            }
            _enumSign = true;
            this.Enabled = false;
            Task.Run(() => EnumTask(comboBox_Name.Text, comboBox_Query.Text));
        }

        private void EnumTask(string name, string query)
        {
            try
            {
                HTuple info, values;
                HOperatorSet.InfoFramegrabber(name, query, out info, out values);
                this.BeginInvoke((Action)delegate ()
                {
                    ShowInfo(info, values);
                });
            }
            catch (System.Exception ec)
            {
                fhs.LogMgr.File(ec.Message);
                this.BeginInvoke((Action)delegate ()
                {
                    ShowInfo("", "error");
                });
            }
        }

        private void ShowInfo(HTuple info, HTuple values)
        {
            textBox_Info.Text = $"{info.ToString()}\r\n{values.ToString()}";
            this.Enabled = true;
            _enumSign = false;
        }
    }
}
