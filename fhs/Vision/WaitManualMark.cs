﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    public partial class WaitManualMark : Form
    {
        Action _closeCb;

        public WaitManualMark(string msg, Action closedCb)
        {
            _closeCb = closedCb;
            InitializeComponent();
            textBox_Msg.Text = msg;
        }

        private void button_确定_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void WaitManualMark_FormClosed(object sender, FormClosedEventArgs e)
        {
            _closeCb.Invoke();
        }
    }
}
