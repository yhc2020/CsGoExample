﻿namespace fhs
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.button_home = new System.Windows.Forms.Button();
            this.button_run = new System.Windows.Forms.Button();
            this.button_pause = new System.Windows.Forms.Button();
            this.button_stop = new System.Windows.Forms.Button();
            this.button_data = new System.Windows.Forms.Button();
            this.button_login = new System.Windows.Forms.Button();
            this.button_debug = new System.Windows.Forms.Button();
            this.panel_state = new System.Windows.Forms.Panel();
            this.panel_main = new System.Windows.Forms.Panel();
            this.panel_currPro = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // button_home
            // 
            this.button_home.BackgroundImage = global::fhs.Properties.Resources.Home_on;
            this.button_home.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_home.FlatAppearance.BorderSize = 0;
            this.button_home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_home.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button_home.Location = new System.Drawing.Point(13, 13);
            this.button_home.Name = "button_home";
            this.button_home.Size = new System.Drawing.Size(90, 90);
            this.button_home.TabIndex = 0;
            this.button_home.UseVisualStyleBackColor = true;
            this.button_home.Click += new System.EventHandler(this.button_home_Click);
            // 
            // button_run
            // 
            this.button_run.BackgroundImage = global::fhs.Properties.Resources.Start_On;
            this.button_run.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_run.FlatAppearance.BorderSize = 0;
            this.button_run.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_run.Location = new System.Drawing.Point(227, 13);
            this.button_run.Name = "button_run";
            this.button_run.Size = new System.Drawing.Size(90, 90);
            this.button_run.TabIndex = 2;
            this.button_run.UseVisualStyleBackColor = true;
            this.button_run.Click += new System.EventHandler(this.button_run_Click);
            // 
            // button_pause
            // 
            this.button_pause.BackgroundImage = global::fhs.Properties.Resources.Pause_On;
            this.button_pause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_pause.FlatAppearance.BorderSize = 0;
            this.button_pause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_pause.Location = new System.Drawing.Point(323, 13);
            this.button_pause.Name = "button_pause";
            this.button_pause.Size = new System.Drawing.Size(90, 90);
            this.button_pause.TabIndex = 3;
            this.button_pause.UseVisualStyleBackColor = true;
            this.button_pause.Click += new System.EventHandler(this.button_pause_Click);
            // 
            // button_stop
            // 
            this.button_stop.BackgroundImage = global::fhs.Properties.Resources.Stop_On;
            this.button_stop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_stop.FlatAppearance.BorderSize = 0;
            this.button_stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_stop.Location = new System.Drawing.Point(418, 13);
            this.button_stop.Name = "button_stop";
            this.button_stop.Size = new System.Drawing.Size(90, 90);
            this.button_stop.TabIndex = 4;
            this.button_stop.UseVisualStyleBackColor = true;
            this.button_stop.Click += new System.EventHandler(this.button_stop_Click);
            // 
            // button_data
            // 
            this.button_data.BackgroundImage = global::fhs.Properties.Resources.data;
            this.button_data.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_data.FlatAppearance.BorderSize = 0;
            this.button_data.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_data.Location = new System.Drawing.Point(527, 13);
            this.button_data.Name = "button_data";
            this.button_data.Size = new System.Drawing.Size(90, 90);
            this.button_data.TabIndex = 5;
            this.button_data.UseVisualStyleBackColor = true;
            this.button_data.Click += new System.EventHandler(this.button_data_Click);
            // 
            // button_login
            // 
            this.button_login.BackgroundImage = global::fhs.Properties.Resources.Login_On;
            this.button_login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_login.FlatAppearance.BorderSize = 0;
            this.button_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_login.Location = new System.Drawing.Point(623, 13);
            this.button_login.Name = "button_login";
            this.button_login.Size = new System.Drawing.Size(90, 90);
            this.button_login.TabIndex = 6;
            this.button_login.UseVisualStyleBackColor = true;
            this.button_login.Click += new System.EventHandler(this.button_login_Click);
            // 
            // button_debug
            // 
            this.button_debug.BackgroundImage = global::fhs.Properties.Resources.Mannual_On;
            this.button_debug.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_debug.FlatAppearance.BorderSize = 0;
            this.button_debug.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_debug.Location = new System.Drawing.Point(112, 13);
            this.button_debug.Name = "button_debug";
            this.button_debug.Size = new System.Drawing.Size(90, 90);
            this.button_debug.TabIndex = 1;
            this.button_debug.UseVisualStyleBackColor = true;
            this.button_debug.Click += new System.EventHandler(this.button_debug_Click);
            // 
            // panel_state
            // 
            this.panel_state.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_state.Location = new System.Drawing.Point(12, 828);
            this.panel_state.Margin = new System.Windows.Forms.Padding(0);
            this.panel_state.Name = "panel_state";
            this.panel_state.Size = new System.Drawing.Size(1563, 32);
            this.panel_state.TabIndex = 4;
            // 
            // panel_main
            // 
            this.panel_main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_main.Location = new System.Drawing.Point(13, 109);
            this.panel_main.Margin = new System.Windows.Forms.Padding(0);
            this.panel_main.Name = "panel_main";
            this.panel_main.Size = new System.Drawing.Size(1562, 719);
            this.panel_main.TabIndex = 5;
            // 
            // panel_currPro
            // 
            this.panel_currPro.Location = new System.Drawing.Point(719, 14);
            this.panel_currPro.Name = "panel_currPro";
            this.panel_currPro.Size = new System.Drawing.Size(519, 90);
            this.panel_currPro.TabIndex = 7;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1584, 861);
            this.Controls.Add(this.panel_currPro);
            this.Controls.Add(this.panel_main);
            this.Controls.Add(this.panel_state);
            this.Controls.Add(this.button_debug);
            this.Controls.Add(this.button_login);
            this.Controls.Add(this.button_data);
            this.Controls.Add(this.button_stop);
            this.Controls.Add(this.button_pause);
            this.Controls.Add(this.button_run);
            this.Controls.Add(this.button_home);
            this.Icon = global::fhs.Properties.Resources.msdn;
            this.Name = "MainForm";
            this.Text = "Cell贴合视觉对位";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_home;
        private System.Windows.Forms.Button button_run;
        private System.Windows.Forms.Button button_pause;
        private System.Windows.Forms.Button button_stop;
        private System.Windows.Forms.Button button_data;
        private System.Windows.Forms.Button button_login;
        private System.Windows.Forms.Button button_debug;
        private System.Windows.Forms.Panel panel_state;
        private System.Windows.Forms.Panel panel_main;
        private System.Windows.Forms.Panel panel_currPro;
    }
}

