﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    class GtsMotion : MotionBase
    {
        public const int Sts_报警 = 1 << 0;
        public const int Sts_跟随误差大 = 1 << 4;
        public const int Sts_正限位触发 = 1 << 5;
        public const int Sts_负限位触发 = 1 << 6;
        public const int Sts_平滑停止 = 1 << 7;
        public const int Sts_紧急停止 = 1 << 8;
        public const int Sts_使能 = 1 << 9;
        public const int Sts_运动 = 1 << 10;
        public const int Sts_到位 = 1 << 11;

        GtsMotionCard _card;
        int _no;
        int _smooth;
        bool _homeDirection;
        int _homeVec;
        int _homeOffset;
        int _homeDistance;

        public GtsMotion(GtsCard card, int no, PointName.Axis name, bool neg, int smooth, bool homeDirection, int homeVec, int homeOffset, int homeDistance) : base(name, neg)
        {
            _card = (GtsMotionCard)card;
            _no = no;
            _smooth = smooth;
            _homeDirection = homeDirection;
            _homeVec = homeVec;
            _homeOffset = homeOffset;
            _homeDistance = homeDistance;
        }

        public override int DstPos
        {
            get
            {
                int pos;
                if (!_card.GetDstPos(_no, out pos))
                {
                    throw new MotionException { error = Error.未知错误 };
                }
                return pos;
            }
        }

        public override int PrfPos
        {
            get
            {
                int pos;
                if (!_card.GetPrfPos(_no, out pos))
                {
                    throw new MotionException { error = Error.未知错误 };
                }
                return pos;
            }
        }

        public override int EncPos
        {
            get
            {
                int pos;
                if (!_card.GetEncPos(_no, out pos))
                {
                    throw new MotionException { error = Error.未知错误 };
                }
                return pos;
            }
        }

        public override int RawStat
        {
            get
            {
                int state;
                if (!_card.GetServoState(_no, out state))
                {
                    throw new MotionException { error = Error.未知错误 };
                }
                return state;
            }
        }

        public override bool Warning
        {
            get
            {
                return 0 != (RawStat & Sts_报警);
            }
        }

        public override bool FollowError
        {
            get
            {
                return 0 != (RawStat & Sts_跟随误差大);
            }
        }

        public override bool PosLimitTrig
        {
            get
            {
                return 0 != (RawStat & Sts_正限位触发);
            }
        }

        public override bool NegLimitTrig
        {
            get
            {
                return 0 != (RawStat & Sts_负限位触发);
            }
        }

        public override bool SmoothStop
        {
            get
            {
                return 0 != (RawStat & Sts_平滑停止);
            }
        }

        public override bool EmgStop
        {
            get
            {
                return 0 != (RawStat & Sts_紧急停止);
            }
        }

        public override bool Enable
        {
            get
            {
                return 0 != (RawStat & Sts_使能);
            }
        }

        public override bool Run
        {
            get
            {
                return 0 != (RawStat & Sts_运动);
            }
        }

        public override bool InPlace
        {
            get
            {
                return 0 != (RawStat & Sts_到位);
            }
        }

        public override int Vec
        {
            get
            {
                return _card.GetVel(_no);
            }
            set
            {
                _card.SetVel(_no, value);
            }
        }

        public override double Acc
        {
            get
            {
                return _card.GetAcc(_no);
            }
            set
            {
                _card.SetAcc(_no, value);
            }
        }

        public override double Dec
        {
            get
            {
                return _card.GetDec(_no);
            }
            set
            {
                _card.SetDec(_no, value);
            }
        }

        public override OutBase.State PosLimit
        {
            get
            {
                bool state;
                if (!_card.GetLimitPositive(_no, out state))
                {
                    throw new MotionException { error = Error.未知错误 };
                }
                return state ? OutBase.State.高 : OutBase.State.低;
            }
        }

        public override OutBase.State NegLimit
        {
            get
            {
                bool state;
                if (!_card.GetLimitNegative(_no, out state))
                {
                    throw new MotionException { error = Error.未知错误 };
                }
                return state ? OutBase.State.高 : OutBase.State.低;
            }
        }

        public override OutBase.State Org
        {
            get
            {
                bool state;
                if (!_card.GetHome(_no, out state))
                {
                    throw new MotionException { error = Error.未知错误 };
                }
                return state ? OutBase.State.高 : OutBase.State.低;
            }
        }

        public override void On()
        {
            if (!_card.ServoOn(_no))
            {
                throw new MotionException { error = Error.未知错误 };
            }
        }

        public override void Off()
        {
            if (!_card.ServoOff(_no))
            {
                throw new MotionException { error = Error.未知错误 };
            }
        }

        public override void Home()
        {
            if (!_card.HomeMove(_no, _homeDirection, _smooth, _homeVec, _homeOffset, _homeDistance))
            {
                throw new MotionException { error = Error.未知错误 };
            }
        }

        public override void Clear()
        {
            if (!_card.Clear(_no))
            {
                throw new MotionException { error = Error.未知错误 };
            }
        }

        public override void Zero()
        {
            if (!_card.ZeroPos(_no))
            {
                throw new MotionException { error = Error.未知错误 };
            }
        }

        public override void Abs(int pos)
        {
            _card.SetSmoothTime(_no, _smooth);
            if (!_card.AbsMove(_no, pos))
            {
                throw new MotionException { error = Error.未知错误 };
            }
        }

        public override void Rel(int pos)
        {
            _card.SetSmoothTime(_no, _smooth);
            if (!_card.RelativeMove(_no, pos))
            {
                throw new MotionException { error = Error.未知错误 };
            }
        }

        public override void Jog(bool pos)
        {
            if (!_card.JogMove(_no, pos))
            {
                throw new MotionException { error = Error.未知错误 };
            }
        }

        public override void Stop(bool emg)
        {
            if (!_card.StopAxis(_no, emg))
            {
                throw new MotionException { error = Error.未知错误 };
            }
        }

        public override Error GetLastErr()
        {
            return Error.未知错误;
        }
    }
}
