﻿namespace fhs
{
    partial class Robot4AxisForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_左 = new System.Windows.Forms.Button();
            this.button_前 = new System.Windows.Forms.Button();
            this.button_右 = new System.Windows.Forms.Button();
            this.button_后 = new System.Windows.Forms.Button();
            this.button_上 = new System.Windows.Forms.Button();
            this.button_停止 = new System.Windows.Forms.Button();
            this.button_下 = new System.Windows.Forms.Button();
            this.button_顺时针 = new System.Windows.Forms.Button();
            this.button_逆时针 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_X = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_Y = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_Z = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_U = new System.Windows.Forms.TextBox();
            this.textBox_MoveX = new System.Windows.Forms.TextBox();
            this.textBox_MoveY = new System.Windows.Forms.TextBox();
            this.textBox_MoveU = new System.Windows.Forms.TextBox();
            this.textBox_MoveZ = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button_左
            // 
            this.button_左.BackgroundImage = global::fhs.Properties.Resources.servo_Left;
            this.button_左.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_左.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_左.Location = new System.Drawing.Point(12, 94);
            this.button_左.Name = "button_左";
            this.button_左.Size = new System.Drawing.Size(75, 75);
            this.button_左.TabIndex = 0;
            this.button_左.Text = "左";
            this.button_左.UseVisualStyleBackColor = true;
            this.button_左.Click += new System.EventHandler(this.button_左_Click);
            this.button_左.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_左_MouseDown);
            this.button_左.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_左_MouseUp);
            // 
            // button_前
            // 
            this.button_前.BackgroundImage = global::fhs.Properties.Resources.servo_front;
            this.button_前.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_前.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_前.Location = new System.Drawing.Point(95, 12);
            this.button_前.Name = "button_前";
            this.button_前.Size = new System.Drawing.Size(75, 75);
            this.button_前.TabIndex = 0;
            this.button_前.Text = "前";
            this.button_前.UseVisualStyleBackColor = true;
            this.button_前.Click += new System.EventHandler(this.button_前_Click);
            this.button_前.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_前_MouseDown);
            this.button_前.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_前_MouseUp);
            // 
            // button_右
            // 
            this.button_右.BackgroundImage = global::fhs.Properties.Resources.servo_right;
            this.button_右.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_右.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_右.Location = new System.Drawing.Point(178, 94);
            this.button_右.Name = "button_右";
            this.button_右.Size = new System.Drawing.Size(75, 75);
            this.button_右.TabIndex = 0;
            this.button_右.Text = "右";
            this.button_右.UseVisualStyleBackColor = true;
            this.button_右.Click += new System.EventHandler(this.button_右_Click);
            this.button_右.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_右_MouseDown);
            this.button_右.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_右_MouseUp);
            // 
            // button_后
            // 
            this.button_后.BackgroundImage = global::fhs.Properties.Resources.servo_back;
            this.button_后.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_后.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_后.Location = new System.Drawing.Point(95, 176);
            this.button_后.Name = "button_后";
            this.button_后.Size = new System.Drawing.Size(75, 75);
            this.button_后.TabIndex = 0;
            this.button_后.Text = "后";
            this.button_后.UseVisualStyleBackColor = true;
            this.button_后.Click += new System.EventHandler(this.button_后_Click);
            this.button_后.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_后_MouseDown);
            this.button_后.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_后_MouseUp);
            // 
            // button_上
            // 
            this.button_上.BackgroundImage = global::fhs.Properties.Resources.servo_up;
            this.button_上.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_上.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_上.Location = new System.Drawing.Point(12, 12);
            this.button_上.Name = "button_上";
            this.button_上.Size = new System.Drawing.Size(75, 75);
            this.button_上.TabIndex = 0;
            this.button_上.Text = "上";
            this.button_上.UseVisualStyleBackColor = true;
            this.button_上.Click += new System.EventHandler(this.button_上_Click);
            this.button_上.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_上_MouseDown);
            this.button_上.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_上_MouseUp);
            // 
            // button_停止
            // 
            this.button_停止.BackgroundImage = global::fhs.Properties.Resources.servo_Stop;
            this.button_停止.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_停止.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_停止.Location = new System.Drawing.Point(95, 94);
            this.button_停止.Name = "button_停止";
            this.button_停止.Size = new System.Drawing.Size(75, 75);
            this.button_停止.TabIndex = 0;
            this.button_停止.Text = "停止";
            this.button_停止.UseVisualStyleBackColor = true;
            this.button_停止.Click += new System.EventHandler(this.button_停止_Click);
            // 
            // button_下
            // 
            this.button_下.BackgroundImage = global::fhs.Properties.Resources.servo_down;
            this.button_下.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_下.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_下.Location = new System.Drawing.Point(12, 176);
            this.button_下.Name = "button_下";
            this.button_下.Size = new System.Drawing.Size(75, 75);
            this.button_下.TabIndex = 0;
            this.button_下.Text = "下";
            this.button_下.UseVisualStyleBackColor = true;
            this.button_下.Click += new System.EventHandler(this.button_下_Click);
            this.button_下.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_下_MouseDown);
            this.button_下.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_下_MouseUp);
            // 
            // button_顺时针
            // 
            this.button_顺时针.BackgroundImage = global::fhs.Properties.Resources.servo_turn_right;
            this.button_顺时针.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_顺时针.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_顺时针.Location = new System.Drawing.Point(178, 12);
            this.button_顺时针.Name = "button_顺时针";
            this.button_顺时针.Size = new System.Drawing.Size(75, 75);
            this.button_顺时针.TabIndex = 0;
            this.button_顺时针.Text = "顺时针";
            this.button_顺时针.UseVisualStyleBackColor = true;
            this.button_顺时针.Click += new System.EventHandler(this.button_顺时针_Click);
            this.button_顺时针.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_顺时针_MouseDown);
            this.button_顺时针.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_顺时针_MouseUp);
            // 
            // button_逆时针
            // 
            this.button_逆时针.BackgroundImage = global::fhs.Properties.Resources.servo_turn_left;
            this.button_逆时针.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_逆时针.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_逆时针.Location = new System.Drawing.Point(178, 176);
            this.button_逆时针.Name = "button_逆时针";
            this.button_逆时针.Size = new System.Drawing.Size(75, 75);
            this.button_逆时针.TabIndex = 0;
            this.button_逆时针.Text = "逆时针";
            this.button_逆时针.UseVisualStyleBackColor = true;
            this.button_逆时针.Click += new System.EventHandler(this.button_逆时针_Click);
            this.button_逆时针.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_逆时针_MouseDown);
            this.button_逆时针.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_逆时针_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 262);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "X";
            // 
            // textBox_X
            // 
            this.textBox_X.Location = new System.Drawing.Point(30, 259);
            this.textBox_X.Name = "textBox_X";
            this.textBox_X.ReadOnly = true;
            this.textBox_X.Size = new System.Drawing.Size(95, 21);
            this.textBox_X.TabIndex = 3;
            this.textBox_X.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 289);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "Y";
            // 
            // textBox_Y
            // 
            this.textBox_Y.Location = new System.Drawing.Point(30, 286);
            this.textBox_Y.Name = "textBox_Y";
            this.textBox_Y.ReadOnly = true;
            this.textBox_Y.Size = new System.Drawing.Size(95, 21);
            this.textBox_Y.TabIndex = 3;
            this.textBox_Y.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 317);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "Z";
            // 
            // textBox_Z
            // 
            this.textBox_Z.Location = new System.Drawing.Point(30, 340);
            this.textBox_Z.Name = "textBox_Z";
            this.textBox_Z.ReadOnly = true;
            this.textBox_Z.Size = new System.Drawing.Size(95, 21);
            this.textBox_Z.TabIndex = 3;
            this.textBox_Z.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 345);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "U";
            // 
            // textBox_U
            // 
            this.textBox_U.Location = new System.Drawing.Point(30, 313);
            this.textBox_U.Name = "textBox_U";
            this.textBox_U.ReadOnly = true;
            this.textBox_U.Size = new System.Drawing.Size(95, 21);
            this.textBox_U.TabIndex = 3;
            this.textBox_U.Text = "0";
            // 
            // textBox_MoveX
            // 
            this.textBox_MoveX.Location = new System.Drawing.Point(158, 259);
            this.textBox_MoveX.Name = "textBox_MoveX";
            this.textBox_MoveX.Size = new System.Drawing.Size(95, 21);
            this.textBox_MoveX.TabIndex = 3;
            this.textBox_MoveX.Text = "0";
            // 
            // textBox_MoveY
            // 
            this.textBox_MoveY.Location = new System.Drawing.Point(158, 286);
            this.textBox_MoveY.Name = "textBox_MoveY";
            this.textBox_MoveY.Size = new System.Drawing.Size(95, 21);
            this.textBox_MoveY.TabIndex = 3;
            this.textBox_MoveY.Text = "0";
            // 
            // textBox_MoveU
            // 
            this.textBox_MoveU.Location = new System.Drawing.Point(158, 340);
            this.textBox_MoveU.Name = "textBox_MoveU";
            this.textBox_MoveU.Size = new System.Drawing.Size(95, 21);
            this.textBox_MoveU.TabIndex = 3;
            this.textBox_MoveU.Text = "0";
            // 
            // textBox_MoveZ
            // 
            this.textBox_MoveZ.Location = new System.Drawing.Point(158, 313);
            this.textBox_MoveZ.Name = "textBox_MoveZ";
            this.textBox_MoveZ.Size = new System.Drawing.Size(95, 21);
            this.textBox_MoveZ.TabIndex = 3;
            this.textBox_MoveZ.Text = "0";
            // 
            // Robot4AxisForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 371);
            this.Controls.Add(this.textBox_MoveZ);
            this.Controls.Add(this.textBox_U);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_MoveU);
            this.Controls.Add(this.textBox_Z);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_MoveY);
            this.Controls.Add(this.textBox_Y);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_MoveX);
            this.Controls.Add(this.textBox_X);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_下);
            this.Controls.Add(this.button_停止);
            this.Controls.Add(this.button_后);
            this.Controls.Add(this.button_顺时针);
            this.Controls.Add(this.button_逆时针);
            this.Controls.Add(this.button_右);
            this.Controls.Add(this.button_上);
            this.Controls.Add(this.button_前);
            this.Controls.Add(this.button_左);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Robot4AxisForm";
            this.Text = "Robot4AxisForm";
            this.Load += new System.EventHandler(this.Robot4AxisForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_左;
        private System.Windows.Forms.Button button_前;
        private System.Windows.Forms.Button button_右;
        private System.Windows.Forms.Button button_后;
        private System.Windows.Forms.Button button_上;
        private System.Windows.Forms.Button button_停止;
        private System.Windows.Forms.Button button_下;
        private System.Windows.Forms.Button button_顺时针;
        private System.Windows.Forms.Button button_逆时针;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox textBox_X;
        public System.Windows.Forms.TextBox textBox_Y;
        public System.Windows.Forms.TextBox textBox_Z;
        public System.Windows.Forms.TextBox textBox_U;
        public System.Windows.Forms.TextBox textBox_MoveX;
        public System.Windows.Forms.TextBox textBox_MoveY;
        public System.Windows.Forms.TextBox textBox_MoveU;
        public System.Windows.Forms.TextBox textBox_MoveZ;
    }
}