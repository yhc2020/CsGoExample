﻿namespace fhs
{
    partial class AxisOptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.radioButton_绝对 = new System.Windows.Forms.RadioButton();
            this.radioButton_Jog = new System.Windows.Forms.RadioButton();
            this.radioButton_相对 = new System.Windows.Forms.RadioButton();
            this.button_停止 = new System.Windows.Forms.Button();
            this.轴名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.轴原点 = new System.Windows.Forms.DataGridViewImageColumn();
            this.负极限 = new System.Windows.Forms.DataGridViewImageColumn();
            this.正极限 = new System.Windows.Forms.DataGridViewImageColumn();
            this.轴状态 = new System.Windows.Forms.DataGridViewImageColumn();
            this.轴目标 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.轴规划 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.轴反馈 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.轴移动 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.轴速度 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.加速度 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.减速度 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.正方向 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.反方向 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.原点 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.清零 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.清除 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.使能 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.轴名称,
            this.轴原点,
            this.负极限,
            this.正极限,
            this.轴状态,
            this.轴目标,
            this.轴规划,
            this.轴反馈,
            this.轴移动,
            this.轴速度,
            this.加速度,
            this.减速度,
            this.正方向,
            this.反方向,
            this.原点,
            this.清零,
            this.清除,
            this.使能});
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1148, 255);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDown);
            this.dataGridView1.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseUp);
            // 
            // radioButton_绝对
            // 
            this.radioButton_绝对.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_绝对.AutoSize = true;
            this.radioButton_绝对.Location = new System.Drawing.Point(165, 261);
            this.radioButton_绝对.Name = "radioButton_绝对";
            this.radioButton_绝对.Size = new System.Drawing.Size(47, 16);
            this.radioButton_绝对.TabIndex = 2;
            this.radioButton_绝对.TabStop = true;
            this.radioButton_绝对.Text = "绝对";
            this.radioButton_绝对.UseVisualStyleBackColor = true;
            this.radioButton_绝对.CheckedChanged += new System.EventHandler(this.radioButton_绝对_CheckedChanged);
            // 
            // radioButton_Jog
            // 
            this.radioButton_Jog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_Jog.AutoSize = true;
            this.radioButton_Jog.Location = new System.Drawing.Point(82, 261);
            this.radioButton_Jog.Name = "radioButton_Jog";
            this.radioButton_Jog.Size = new System.Drawing.Size(41, 16);
            this.radioButton_Jog.TabIndex = 3;
            this.radioButton_Jog.TabStop = true;
            this.radioButton_Jog.Text = "Jog";
            this.radioButton_Jog.UseVisualStyleBackColor = true;
            this.radioButton_Jog.CheckedChanged += new System.EventHandler(this.radioButton_Jog_CheckedChanged);
            // 
            // radioButton_相对
            // 
            this.radioButton_相对.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_相对.AutoSize = true;
            this.radioButton_相对.Location = new System.Drawing.Point(0, 261);
            this.radioButton_相对.Name = "radioButton_相对";
            this.radioButton_相对.Size = new System.Drawing.Size(47, 16);
            this.radioButton_相对.TabIndex = 4;
            this.radioButton_相对.TabStop = true;
            this.radioButton_相对.Text = "相对";
            this.radioButton_相对.UseVisualStyleBackColor = true;
            this.radioButton_相对.CheckedChanged += new System.EventHandler(this.radioButton_相对_CheckedChanged);
            // 
            // button_停止
            // 
            this.button_停止.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_停止.Location = new System.Drawing.Point(543, 261);
            this.button_停止.Name = "button_停止";
            this.button_停止.Size = new System.Drawing.Size(605, 70);
            this.button_停止.TabIndex = 5;
            this.button_停止.Text = "停止";
            this.button_停止.UseVisualStyleBackColor = true;
            this.button_停止.Click += new System.EventHandler(this.button_停止_Click);
            // 
            // 轴名称
            // 
            this.轴名称.HeaderText = "轴名称";
            this.轴名称.Name = "轴名称";
            this.轴名称.ReadOnly = true;
            this.轴名称.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // 轴原点
            // 
            this.轴原点.HeaderText = "轴原点";
            this.轴原点.Image = global::fhs.Properties.Resources.light_gray;
            this.轴原点.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.轴原点.Name = "轴原点";
            this.轴原点.ReadOnly = true;
            this.轴原点.Width = 50;
            // 
            // 负极限
            // 
            this.负极限.HeaderText = "负极限";
            this.负极限.Image = global::fhs.Properties.Resources.light_gray;
            this.负极限.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.负极限.Name = "负极限";
            this.负极限.ReadOnly = true;
            this.负极限.Width = 50;
            // 
            // 正极限
            // 
            this.正极限.HeaderText = "正极限";
            this.正极限.Image = global::fhs.Properties.Resources.light_gray;
            this.正极限.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.正极限.Name = "正极限";
            this.正极限.ReadOnly = true;
            this.正极限.Width = 50;
            // 
            // 轴状态
            // 
            this.轴状态.HeaderText = "轴状态";
            this.轴状态.Image = global::fhs.Properties.Resources.light_gray;
            this.轴状态.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.轴状态.Name = "轴状态";
            this.轴状态.ReadOnly = true;
            this.轴状态.Width = 50;
            // 
            // 轴目标
            // 
            this.轴目标.HeaderText = "轴目标";
            this.轴目标.Name = "轴目标";
            this.轴目标.ReadOnly = true;
            this.轴目标.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.轴目标.Width = 80;
            // 
            // 轴规划
            // 
            this.轴规划.HeaderText = "轴规划";
            this.轴规划.Name = "轴规划";
            this.轴规划.ReadOnly = true;
            this.轴规划.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.轴规划.Width = 80;
            // 
            // 轴反馈
            // 
            this.轴反馈.HeaderText = "轴反馈";
            this.轴反馈.Name = "轴反馈";
            this.轴反馈.ReadOnly = true;
            this.轴反馈.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.轴反馈.Width = 80;
            // 
            // 轴移动
            // 
            this.轴移动.HeaderText = "轴移动";
            this.轴移动.MaxInputLength = 10;
            this.轴移动.Name = "轴移动";
            this.轴移动.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.轴移动.Width = 80;
            // 
            // 轴速度
            // 
            this.轴速度.HeaderText = "轴速度";
            this.轴速度.MaxInputLength = 3;
            this.轴速度.Name = "轴速度";
            this.轴速度.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.轴速度.Width = 50;
            // 
            // 加速度
            // 
            this.加速度.HeaderText = "加速度";
            this.加速度.MaxInputLength = 3;
            this.加速度.Name = "加速度";
            this.加速度.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.加速度.Width = 50;
            // 
            // 减速度
            // 
            this.减速度.HeaderText = "减速度";
            this.减速度.MaxInputLength = 3;
            this.减速度.Name = "减速度";
            this.减速度.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.减速度.Width = 50;
            // 
            // 正方向
            // 
            this.正方向.HeaderText = "正方向";
            this.正方向.Name = "正方向";
            this.正方向.ReadOnly = true;
            this.正方向.Width = 50;
            // 
            // 反方向
            // 
            this.反方向.HeaderText = "反方向";
            this.反方向.Name = "反方向";
            this.反方向.ReadOnly = true;
            this.反方向.Width = 50;
            // 
            // 原点
            // 
            this.原点.HeaderText = "原点";
            this.原点.Name = "原点";
            this.原点.ReadOnly = true;
            this.原点.Width = 50;
            // 
            // 清零
            // 
            this.清零.HeaderText = "清零";
            this.清零.Name = "清零";
            this.清零.ReadOnly = true;
            this.清零.Width = 50;
            // 
            // 清除
            // 
            this.清除.HeaderText = "清除";
            this.清除.Name = "清除";
            this.清除.ReadOnly = true;
            this.清除.Width = 50;
            // 
            // 使能
            // 
            this.使能.HeaderText = "使能";
            this.使能.Name = "使能";
            this.使能.ReadOnly = true;
            this.使能.Width = 50;
            // 
            // AxisOptionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1148, 335);
            this.Controls.Add(this.button_停止);
            this.Controls.Add(this.radioButton_绝对);
            this.Controls.Add(this.radioButton_Jog);
            this.Controls.Add(this.radioButton_相对);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AxisOptionForm";
            this.Text = "AxisEditForm";
            this.Load += new System.EventHandler(this.AxisEditForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.RadioButton radioButton_绝对;
        private System.Windows.Forms.RadioButton radioButton_Jog;
        private System.Windows.Forms.RadioButton radioButton_相对;
        private System.Windows.Forms.Button button_停止;
        private System.Windows.Forms.DataGridViewTextBoxColumn 轴名称;
        private System.Windows.Forms.DataGridViewImageColumn 轴原点;
        private System.Windows.Forms.DataGridViewImageColumn 负极限;
        private System.Windows.Forms.DataGridViewImageColumn 正极限;
        private System.Windows.Forms.DataGridViewImageColumn 轴状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn 轴目标;
        private System.Windows.Forms.DataGridViewTextBoxColumn 轴规划;
        private System.Windows.Forms.DataGridViewTextBoxColumn 轴反馈;
        private System.Windows.Forms.DataGridViewTextBoxColumn 轴移动;
        private System.Windows.Forms.DataGridViewTextBoxColumn 轴速度;
        private System.Windows.Forms.DataGridViewTextBoxColumn 加速度;
        private System.Windows.Forms.DataGridViewTextBoxColumn 减速度;
        private System.Windows.Forms.DataGridViewButtonColumn 正方向;
        private System.Windows.Forms.DataGridViewButtonColumn 反方向;
        private System.Windows.Forms.DataGridViewButtonColumn 原点;
        private System.Windows.Forms.DataGridViewButtonColumn 清零;
        private System.Windows.Forms.DataGridViewButtonColumn 清除;
        private System.Windows.Forms.DataGridViewCheckBoxColumn 使能;
    }
}