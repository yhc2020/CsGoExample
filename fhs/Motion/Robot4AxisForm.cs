﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    public partial class Robot4AxisForm : Form
    {
        public enum MoveDriect
        {
            停止,
            上,
            下,
            前,
            后,
            左,
            右,
            逆时针,
            顺时针
        }

        private Action<MoveDriect> _moveHandler;

        public Robot4AxisForm()
        {
            InitializeComponent();
        }

        public void SetEvent(Action<MoveDriect> moveHandler)
        {
            _moveHandler = moveHandler;
        }

        private void Robot4AxisForm_Load(object sender, EventArgs e)
        {

        }

        private void button_上_Click(object sender, EventArgs e)
        {
            _moveHandler(MoveDriect.上);
        }

        private void button_下_Click(object sender, EventArgs e)
        {
            _moveHandler(MoveDriect.下);
        }

        private void button_左_Click(object sender, EventArgs e)
        {
            _moveHandler(MoveDriect.左);
        }

        private void button_右_Click(object sender, EventArgs e)
        {
            _moveHandler(MoveDriect.右);
        }

        private void button_前_Click(object sender, EventArgs e)
        {
            _moveHandler(MoveDriect.前);
        }

        private void button_后_Click(object sender, EventArgs e)
        {
            _moveHandler(MoveDriect.后);
        }

        private void button_顺时针_Click(object sender, EventArgs e)
        {
            _moveHandler(MoveDriect.顺时针);
        }

        private void button_逆时针_Click(object sender, EventArgs e)
        {
            _moveHandler(MoveDriect.逆时针);
        }

        private void button_停止_Click(object sender, EventArgs e)
        {
            _moveHandler(MoveDriect.停止);
        }

        private void radioButton_相对_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void radioButton_Jog_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void radioButton_绝对_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void button_上_MouseDown(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.上);
        }

        private void button_上_MouseUp(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.停止);
        }

        private void button_下_MouseDown(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.下);
        }

        private void button_下_MouseUp(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.停止);
        }

        private void button_前_MouseDown(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.前);
        }

        private void button_前_MouseUp(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.停止);
        }

        private void button_后_MouseDown(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.后);
        }

        private void button_后_MouseUp(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.停止);
        }

        private void button_左_MouseDown(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.左);
        }

        private void button_左_MouseUp(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.停止);
        }

        private void button_右_MouseDown(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.右);
        }

        private void button_右_MouseUp(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.停止);
        }

        private void button_顺时针_MouseDown(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.顺时针);
        }

        private void button_顺时针_MouseUp(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.停止);
        }

        private void button_逆时针_MouseDown(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.逆时针);
        }

        private void button_逆时针_MouseUp(object sender, MouseEventArgs e)
        {
            _moveHandler(MoveDriect.停止);
        }
    }
}
