﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    abstract class MotionBase
    {
        public enum Error
        {
            成功,
            掉电,
            跟随误差大,
            碰撞,
            通信错误,
            未知错误
        }
        
        public class MotionException : System.Exception
        {
            public Error error;
        }

        readonly PointName.Axis _name;
        readonly protected bool _neg;

        protected MotionBase(PointName.Axis name, bool neg)
        {
            _name = name;
            _neg = neg;
        }

        public PointName.Axis Name
        {
            get
            {
                return _name;
            }
        }

        public bool Neg
        {
            get
            {
                return _neg;
            }
        }

        public abstract int DstPos { get; }
        public abstract int PrfPos { get; }
        public abstract int EncPos { get; }
        public abstract int RawStat { get; }
        public abstract bool Warning { get; }
        public abstract bool FollowError { get; }
        public abstract bool PosLimitTrig { get; }
        public abstract bool NegLimitTrig { get; }
        public abstract bool SmoothStop { get; }
        public abstract bool EmgStop { get; }
        public abstract bool Enable { get; }
        public abstract bool Run { get; }
        public abstract bool InPlace { get; }
        public abstract int Vec { get; set; }
        public abstract double Acc { get; set; }
        public abstract double Dec { get; set; }
        public abstract OutBase.State PosLimit { get; }
        public abstract OutBase.State NegLimit { get; }
        public abstract OutBase.State Org { get; }

        public abstract void On();
        public abstract void Off();
        public abstract void Home();
        public abstract void Clear();
        public abstract void Zero();
        public abstract void Abs(int pos);
        public abstract void Rel(int pos);
        public abstract void Jog(bool pos);
        public abstract void Stop(bool emg);
        public abstract Error GetLastErr();
    }
}
