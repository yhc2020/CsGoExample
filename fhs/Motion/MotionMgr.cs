﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace fhs
{
    class MotionMgr
    {
        static public readonly Dictionary<PointName.Axis, MotionBase> motions = new Dictionary<PointName.Axis, MotionBase>();

        static public void Load()
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(@".\Config\Motion.xml");
            }
            catch (Exception ec)
            {
                LogMgr.Error($"读取轴异常:{ec.Message}");
                return;
            }
            XmlElement root = doc.DocumentElement;
            foreach (XmlNode motionItem in root.ChildNodes)
            {
                string axisNameStr = ((XmlElement)motionItem).GetAttribute("Name");
                PointName.Axis axisName = (PointName.Axis)(-1);
                if (!StringToEnum.Convert(axisNameStr, ref axisName))
                {
                    LogMgr.Error($"{axisNameStr} 轴名称未定义");
                    continue;
                }
                if (!motions.ContainsKey(axisName))
                {
                    string brand = ((XmlElement)motionItem).GetAttribute("Brand");
                    switch (brand.ToLower())
                    {
                        case "gts":
                            {
                                string card = ((XmlElement)motionItem).GetAttribute("Card");
                                string no = ((XmlElement)motionItem).GetAttribute("No");
                                string neg = ((XmlElement)motionItem).GetAttribute("Neg");
                                string smooth = ((XmlElement)motionItem).GetAttribute("Smooth");
                                string homeDirection = ((XmlElement)motionItem).GetAttribute("HomeDirection");
                                string homeVec = ((XmlElement)motionItem).GetAttribute("HomeVec");
                                string homeOffset = ((XmlElement)motionItem).GetAttribute("HomeOffset");
                                string homeDistance = ((XmlElement)motionItem).GetAttribute("HomeDistance");
                                if (CardMgr.cards.ContainsKey(card))
                                {
                                    try
                                    {
                                        GtsCard gtsCard = (GtsCard)CardMgr.cards[card];
                                        motions[axisName] = new GtsMotion(gtsCard, int.Parse(no), axisName, "true" == neg.ToLower(), int.Parse(smooth), "Positive" == homeDirection, int.Parse(homeVec), int.Parse(homeOffset), int.Parse(homeDistance));
                                    }
                                    catch (System.Exception ec)
                                    {
                                        LogMgr.Error($"轴初始化失败{card}:{no} {ec.Message}");
                                    }
                                }
                                else
                                {
                                    LogMgr.Error($"{card}卡未定义");
                                }
                            }
                            break;
                        default:
                            LogMgr.Error($"未定义的轴品牌 {brand}");
                            break;
                    }
                }
                else
                {
                    LogMgr.Error($"{axisName}轴名称重定义");
                }
            }
        }
    }
}
