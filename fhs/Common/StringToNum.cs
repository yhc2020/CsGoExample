﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    class StringToNum
    {
        static public int[] Int(string[] strs)
        {
            int[] res = new int[strs.Length];
            for (int i = 0; i < strs.Length; i++)
            {
                res[i] = int.Parse(strs[i]);
            }
            return res;
        }

        static public double[] Num(string[] strs)
        {
            double[] res = new double[strs.Length];
            for (int i = 0; i < strs.Length; i++)
            {
                res[i] = double.Parse(strs[i]);
            }
            return res;
        }
    }
}
