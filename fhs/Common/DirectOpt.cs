﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Go;

namespace fhs
{
    class DirectOpt
    {
        static public bool CopyDirectory(string srcPath, string destPath)
        {
            try
            {
                Directory.CreateDirectory(destPath);
                DirectoryInfo dir = new DirectoryInfo(srcPath);
                FileSystemInfo[] fileinfo = dir.GetFileSystemInfos();
                foreach (FileSystemInfo i in fileinfo)
                {
                    string dstPath = $@"{destPath}\{i.Name}";
                    if (i is DirectoryInfo)
                    {
                        if (!Directory.Exists(dstPath))
                        {
                            Directory.CreateDirectory(dstPath);
                        }
                        if (!CopyDirectory(i.FullName, dstPath))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        File.Copy(i.FullName, dstPath, true);
                    }
                }
                return true;
            }
            catch (Exception ec)
            {
                LogMgr.Error($"拷贝目录异常: {ec.Message}");
                return false;
            }
        }

        static public bool RemoveDirectory(string path)
        {
            try
            {
                Directory.Delete(path, true);
                return true;
            }
            catch (Exception ec)
            {
                LogMgr.Error($"删除目录异常: {ec.Message}");
                return !Directory.Exists(path);
            }
        }

        static public async Task RemoveDirectory(shared_strand removeStrand, string path)
        {
            string[] files = await generator.send_strand(removeStrand, () => Directory.GetFiles(path));
            foreach (string file in files)
            {
                await generator.send_strand(removeStrand, () => File.Delete(file));
            }
            string[] dirs = await generator.send_strand(removeStrand, () => Directory.GetDirectories(path));
            foreach (string dir in dirs)
            {
                await RemoveDirectory(removeStrand, dir);
            }
            try
            {
                await generator.send_strand(removeStrand, () => Directory.Delete(path));
            }
            catch (System.Exception) { }
        }

        static public async Task RemoveDirectory(work_service removeService, string path)
        {
            string[] files = await generator.send_service(removeService, () => Directory.GetFiles(path));
            foreach (string file in files)
            {
                await generator.send_service(removeService, () => File.Delete(file));
            }
            string[] dirs = await generator.send_service(removeService, () => Directory.GetDirectories(path));
            foreach (string dir in dirs)
            {
                await RemoveDirectory(removeService, dir);
            }
            try
            {
                await generator.send_service(removeService, () => Directory.Delete(path));
            }
            catch (System.Exception) { }
        }
    }
}
