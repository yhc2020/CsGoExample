﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    static class NumberSegment
    {
        static public List<double> Parse(string fmtStr)
        {
            List<double> result = new List<double>();
            int lastIdx = 0;
            int sidx;
            while ((sidx = fmtStr.IndexOf(',', lastIdx)) >= 0)
            {
                double num = 0;
                double.TryParse(fmtStr.Substring(lastIdx, sidx - lastIdx), out num);
                result.Add(num);
                lastIdx = sidx + 1;
            }
            if (lastIdx < fmtStr.Length)
            {
                double num = 0;
                double.TryParse(fmtStr.Substring(lastIdx, fmtStr.Length - lastIdx), out num);
                result.Add(num);
            }
            return result;
        }
    }
}
