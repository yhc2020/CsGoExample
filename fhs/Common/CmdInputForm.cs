﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    public partial class CmdInpuForm : Form
    {
        public int number;
        public int max;
        public int min;
        public bool ok = false;

        public CmdInpuForm(int iniNum = 0, int max = int.MaxValue, int min = int.MinValue)
        {
            InitializeComponent();
            numericUpDown_Num.Value = iniNum;
            this.max = max;
            this.min = min;
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            number = (int)numericUpDown_Num.Value;
            if (number >= min && number <= max)
            {
                ok = true;
                Close();
            }
            else
            {
                MessageBox.Show($"设置数值超过 {min} -> {max} 限制！");
            }
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
