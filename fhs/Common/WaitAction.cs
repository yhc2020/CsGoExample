﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Go;

namespace fhs
{
    static class WaitAction
    {
        static public void Run(out generator gen, generator.action action)
        {
            generator.go(out gen, GlobalData.mainStrand, async delegate ()
            {
                generator waitAct = null;
                try
                {
                    waitAct = generator.tgo(GlobalData.mainStrand, async delegate ()
                    {
                        await generator.sleep(1000);
                        bool closed = false;
                        WaitForm waitForm = new WaitForm();
                        try
                        {
                            await generator.send_control(MainForm.obj, delegate ()
                            {
                                if (!closed)
                                {
                                    waitForm.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
                                    waitForm.ShowDialog();
                                }
                            });
                        }
                        catch (generator.stop_exception)
                        {
                            closed = true;
                            waitForm.Close();
                            throw;
                        }
                    });
                    await action();
                }
                catch (Exception ec)
                {
                    LogMgr.Error(ec.Message);
                }
                generator.lock_shield();
                await generator.stop_other(waitAct);
                await generator.unlock_shield();
            });
        }
    }
}
