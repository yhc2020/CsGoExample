﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace fhs
{
    static class DefaultLoad
    {
        static public int Load(int def, string file, int idx)
        {
            try
            {
                return int.Parse(File.ReadAllLines(file)[idx]);
            }
            catch (Exception ec)
            {
                LogMgr.File($"{file}载入失败:{ec.Message}");
                return def;
            }
        }

        static public double Load(double def, string file, int idx)
        {
            try
            {
                return double.Parse(File.ReadAllLines(file)[idx]);
            }
            catch (Exception ec)
            {
                LogMgr.File($"{file}载入失败:{ec.Message}");
                return def;
            }
        }

        static public bool Load(bool def, string file, int idx)
        {
            try
            {
                return bool.Parse(File.ReadAllLines(file)[idx]);
            }
            catch (Exception ec)
            {
                LogMgr.File($"{file}载入失败:{ec.Message}");
                return def;
            }
        }

        static public string Load(string def, string file, int idx)
        {
            try
            {
                if (idx >= 0)
                {
                    return File.ReadAllLines(file)[idx];
                }
                return File.ReadAllText(file);
            }
            catch (Exception ec)
            {
                LogMgr.File($"{file}载入失败:{ec.Message}");
                return def;
            }
        }
    }
}
