﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    static class Make
    {
        static private T[] Array_<T>(params T[] values)
        {
            return values;
        }

        static public T[] Array<T>(T v1)
        {
            return Array_(v1);
        }

        static public T[] Array<T>(T v1, T v2)
        {
            return Array_(v1, v2);
        }

        static public T[] Array<T>(T v1, T v2, T v3)
        {
            return Array_(v1, v2, v3);
        }

        static public T[] Array<T>(T v1, T v2, T v3, T v4)
        {
            return Array_(v1, v2, v3, v4);
        }

        static public T[] Array<T>(T v1, T v2, T v3, T v4, T v5)
        {
            return Array_(v1, v2, v3, v4, v5);
        }

        static public T[] Array<T>(T v1, T v2, T v3, T v4, T v5, T v6)
        {
            return Array_(v1, v2, v3, v4, v5, v6);
        }

        static public T[] Array<T>(T v1, T v2, T v3, T v4, T v5, T v6, T v7)
        {
            return Array_(v1, v2, v3, v4, v5, v6, v7);
        }

        static public T[] Array<T>(T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8)
        {
            return Array_(v1, v2, v3, v4, v5, v6, v7, v8);
        }

        static public T[] Array<T>(T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8, T v9)
        {
            return Array_(v1, v2, v3, v4, v5, v6, v7, v8, v9);
        }
    }
}
