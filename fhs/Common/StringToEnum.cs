﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    class StringToEnum
    {
        static public T Convert<T>(string enumName) where T : struct
        {
            Type enumType = typeof(T);
            if (enumType.IsEnum)
            {
                try
                {
                    return (T)Enum.Parse(enumType, enumName);
                }
                catch (Exception)
                {
                    LogMgr.Error($"StringToEnum 转换错误 {enumName}->{typeof(T)}");
                    throw;
                }
            }
            else
            {
                throw new System.Exception("StringToEnum.Convert不是枚举类型");
            }
        }

        static public bool Convert<T>(string enumName, ref T result) where T : struct
        {
            try
            {
                result = Convert<T>(enumName);
                return true;
            }
            catch (Exception) { }
            return false;
        }

        static public T[] Convert<T>(string[] enumNames) where T : struct
        {
            int n = enumNames.Length;
            T[] result = new T[n];
            for (int i = 0; i < n; i++)
            {
                result[i] = Convert<T>(enumNames[i]);
            }
            return result;
        }
    }
}
