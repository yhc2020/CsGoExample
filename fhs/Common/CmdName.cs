﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace fhs
{
    partial class CmdName
    {
        public enum Type
        {
            WORD,
            DWORD
        }

        public struct Addr
        {
            public int addr;
            public Type type;
            public bool readOnly;
            public int max;
            public int min;

            public static implicit operator int(Addr addr)
            {
                return addr.addr;
            }

            public static implicit operator Type(Addr addr)
            {
                return addr.type;
            }
        }

        static public McOption mc = null;
        static public readonly Dictionary<MC, Addr> mcs = new Dictionary<MC, Addr>();

        static public void Load()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(@".\Config\McCmd.xml");
                XmlElement root = doc.DocumentElement;
                mc = new McOption(root.GetAttribute("IP"), int.Parse(root.GetAttribute("Port")), int.Parse(root.GetAttribute("Heartbeat")));
                foreach (XmlNode cmdItem in root.ChildNodes)
                {
                    if (XmlNodeType.Element != cmdItem.NodeType)
                    {
                        continue;
                    }
                    XmlElement cmdEle = (XmlElement)cmdItem;
                    string mcNameStr = cmdEle.GetAttribute("Name");
                    string mcTypeStr = cmdEle.GetAttribute("Type");
                    string mcAddrStr = cmdEle.GetAttribute("Addr");
                    string mcReadOnlyStr = cmdEle.GetAttribute("ReadOnly");
                    string mcMaxStr = cmdEle.GetAttribute("Max");
                    string mcMinStr = cmdEle.GetAttribute("Min");
                    MC mcName = (MC)(-1);
                    Type mcType = (Type)(-1);
                    int mcAddr = -1;
                    bool readOnly = false;
                    int max, min;
                    if (StringToEnum.Convert(mcNameStr, ref mcName)
                        && StringToEnum.Convert(0 == mcTypeStr.Length ? "WORD" : mcTypeStr, ref mcType)
                        && int.TryParse(mcAddrStr, out mcAddr)
                        && bool.TryParse(0 == mcReadOnlyStr.Length ? "False" : mcReadOnlyStr, out readOnly)
                        && int.TryParse(0 == mcMaxStr.Length ? int.MaxValue.ToString() : mcMaxStr, out max)
                        && int.TryParse(0 == mcMinStr.Length ? int.MinValue.ToString() : mcMinStr, out min))
                    {
                        if (mcs.ContainsKey(mcName))
                        {
                            LogMgr.Error($"重复的 {mcNameStr} 命令");
                            continue;
                        }
                        mcs[mcName] = new Addr { addr = mcAddr, type = mcType, readOnly = readOnly, max = max, min = min };
                    }
                    else
                    {
                        LogMgr.Error($"{mcNameStr} 命令定义错误");
                    }
                }
            }
            catch (Exception ec)
            {
                LogMgr.Error($"通信命令导入失败{ec.Message}");
            }
        }
    }
}
