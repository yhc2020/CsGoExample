﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fhs
{
    struct MyPoint
    {
        public double X;
        public double Y;

        public double Row
        {
            get
            {
                return Y;
            }
            set
            {
                Y = value;
            }
        }

        public double Col
        {
            get
            {
                return X;
            }
            set
            {
                X = value;
            }
        }

        public double Mod
        {
            get
            {
                return Math.Sqrt(X * X + Y * Y);
            }
        }

        public static MyPoint operator +(MyPoint l, MyPoint r)
        {
            return new MyPoint { X = l.X + r.X, Y = l.Y + r.Y };
        }

        public static MyPoint operator -(MyPoint l, MyPoint r)
        {
            return new MyPoint { X = l.X - r.X, Y = l.Y - r.Y };
        }

        public static MyPoint operator *(MyPoint l, double c)
        {
            return new MyPoint { X = l.X * c, Y = l.Y * c };
        }

        public static MyPoint operator *(double c, MyPoint r)
        {
            return new MyPoint { X = r.X * c, Y = r.Y * c };
        }

        public void Load(string path)
        {
            try
            {
                string[] ps = System.IO.File.ReadAllLines(path);
                X = double.Parse(ps[0]);
                Y = double.Parse(ps[1]);
            }
            catch (System.Exception)
            {
                LogMgr.Error($"{path} 读取异常");
            }
        }

        public void Save(string path)
        {
            try
            {
                System.IO.File.WriteAllLines(path, new string[] { X.ToString(), Y.ToString() });
            }
            catch (System.Exception)
            {
                LogMgr.Error($"{path} 保存异常");
            }
        }
    }

    struct MyCircle
    {
        public MyPoint Center;
        public double Radius;
    }
}
