﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace fhs
{
    abstract class Roi
    {
        public struct Rect
        {
            public double Len1;
            public double Len2;
            public double Row;
            public double Col;
            public double Phi;

            public void Load(string path)
            {
                try
                {
                    string[] nums = File.ReadAllLines(path);
                    Len1 = double.Parse(nums[0]);
                    Len2 = double.Parse(nums[1]);
                    Row = double.Parse(nums[2]);
                    Col = double.Parse(nums[3]);
                    Phi = double.Parse(nums[4]);
                }
                catch (Exception)
                {
                    LogMgr.Error($"{path} 读取异常");
                }
            }

            public void Save(string path)
            {
                try
                {
                    File.WriteAllLines(path, new string[] { Len1.ToString(), Len2.ToString(), Row.ToString(), Col.ToString(), Phi.ToString() });
                }
                catch (Exception)
                {
                    LogMgr.Error($"{path} 保存异常");
                }
            }

            public double Ang
            {
                get
                {
                    return Phi * 180 / Math.PI;
                }
                set
                {
                    Phi = value * Math.PI / 180;
                }
            }

            public bool R2L
            {
                get
                {
                    double Ang = 90 + Phi * 180 / Math.PI;
                    if (Ang < 0)
                    {
                        Ang = Ang - Math.Floor(Ang / 360) * 360;
                    }
                    else
                    {
                        Ang %= 360;
                    }
                    return Ang > 180;
                }
                set
                {
                    Phi = value ? Math.PI : 0;
                }
            }

            public bool L2R
            {
                get
                {
                    return !R2L;
                }
                set
                {
                    R2L = !value;
                }
            }

            public bool T2B
            {
                get
                {
                    double Ang = Phi * 180 / Math.PI;
                    if (Ang < 0)
                    {
                        Ang = Ang - Math.Floor(Ang / 360) * 360;
                    }
                    else
                    {
                        Ang %= 360;
                    }
                    return Ang > 180;
                }
                set
                {
                    Phi = value ? -Math.PI / 2 : Math.PI / 2;
                }
            }

            public bool B2T
            {
                get
                {
                    return !T2B;
                }
                set
                {
                    T2B = !value;
                }
            }

            public HalconDotNet.HTuple ToHTuple1()
            {
                return new HalconDotNet.HTuple(new double[] { Row, Col, Row + Len2 - 1, Col + Len1 - 1 });
            }

            public HalconDotNet.HTuple ToHTuple2()
            {
                return new HalconDotNet.HTuple(new double[] { Row, Col, Phi, Len1, Len2 });
            }

            public void FromHTuple1(HalconDotNet.HTuple tp)
            {
                try
                {
                    Row = tp[0];
                    Col = tp[1];
                    Len1 = tp[3] - tp[1] + 1;
                    Len2 = tp[2] - tp[0] + 1;
                    Phi = 0;
                }
                catch (Exception)
                {
                    LogMgr.Error("Rect.FromHTuple 出错");
                }
            }

            public void FromHTuple2(HalconDotNet.HTuple tp)
            {
                try
                {
                    Row = tp[0];
                    Col = tp[1];
                    Phi = tp[2];
                    Len1 = tp[3];
                    Len2 = tp[4];
                }
                catch (Exception)
                {
                    LogMgr.Error("Rect.FromHTuple2 出错");
                }
            }
        }

        public struct Cir
        {
            public double Row;
            public double Col;
            public double Rad;

            public void Load(string path)
            {
                try
                {
                    string[] nums = File.ReadAllLines(path);
                    Row = double.Parse(nums[0]);
                    Col = double.Parse(nums[1]);
                    Rad = double.Parse(nums[2]);
                }
                catch (Exception)
                {
                    LogMgr.Error($"{path} 读取异常");
                }
            }

            public void Save(string path)
            {
                try
                {
                    File.WriteAllLines(path, new string[] { Row.ToString(), Col.ToString(), Rad.ToString() });
                }
                catch (Exception)
                {
                    LogMgr.Error($"{path} 保存异常");
                }
            }

            public HalconDotNet.HTuple ToHTuple()
            {
                return new HalconDotNet.HTuple(new double[] { Row, Col, Rad });
            }

            public void FromHTuple(HalconDotNet.HTuple tp)
            {
                try
                {
                    Row = tp[0];
                    Col = tp[1];
                    Rad = tp[2];
                }
                catch (Exception)
                {
                    LogMgr.Error("Cir.FromHTuple 出错");
                }
            }
        }

        public struct Ring
        {
            public double Row;
            public double Col;
            public double Rad1;
            public double Rad2;

            public void Load(string path)
            {
                try
                {
                    string[] nums = File.ReadAllLines(path);
                    Row = double.Parse(nums[0]);
                    Col = double.Parse(nums[1]);
                    Rad1 = double.Parse(nums[2]);
                    Rad2 = double.Parse(nums[3]);
                }
                catch (Exception)
                {
                    LogMgr.Error($"{path} 读取异常");
                }
            }

            public void Save(string path)
            {
                try
                {
                    File.WriteAllLines(path, new string[] { Row.ToString(), Col.ToString(), Rad1.ToString(), Rad2.ToString() });
                }
                catch (Exception)
                {
                    LogMgr.Error($"{path} 保存异常");
                }
            }
        }
    }
}
