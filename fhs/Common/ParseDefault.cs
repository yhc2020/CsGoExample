﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace fhs
{
    abstract class ParseDefault
    {
        static public bool Bool(string str, bool def)
        {
            try
            {
                return bool.Parse(str);
            }
            catch (Exception) { }
            return def;
        }

        static public int Int(string str, int def)
        {
            try
            {
                return int.Parse(str);
            }
            catch (Exception) { }
            return def;
        }

        static public uint UInt(string str, uint def)
        {
            try
            {
                return uint.Parse(str);
            }
            catch (Exception) { }
            return def;
        }

        static public float Float(string str, float def)
        {
            try
            {
                return float.Parse(str);
            }
            catch (Exception) { }
            return def;
        }

        static public double Double(string str, double def)
        {
            try
            {
                return double.Parse(str);
            }
            catch (Exception) { }
            return def;
        }

        static public bool FileBool(string path, int idx, bool def)
        {
            try
            {
                return bool.Parse(File.ReadAllLines(path)[idx]);
            }
            catch (Exception) { }
            return def;
        }

        static public int FileInt(string path, int idx, int def)
        {
            try
            {
                return int.Parse(File.ReadAllLines(path)[idx]);
            }
            catch (Exception) { }
            return def;
        }

        static public uint FileUInt(string path, int idx, uint def)
        {
            try
            {
                return uint.Parse(File.ReadAllLines(path)[idx]);
            }
            catch (Exception) { }
            return def;
        }

        static public float FileFloat(string path, int idx, float def)
        {
            try
            {
                return float.Parse(File.ReadAllLines(path)[idx]);
            }
            catch (Exception) { }
            return def;
        }

        static public double FileDouble(string path, int idx, double def)
        {
            try
            {
                return double.Parse(File.ReadAllLines(path)[idx]);
            }
            catch (Exception) { }
            return def;
        }

        static public bool ArrayBool(string[] strs, int idx, bool def)
        {
            try
            {
                return bool.Parse(strs[idx]);
            }
            catch (Exception) { }
            return def;
        }

        static public int ArrayInt(string[] strs, int idx, int def)
        {
            try
            {
                return int.Parse(strs[idx]);
            }
            catch (Exception) { }
            return def;
        }

        static public uint ArrayUInt(string[] strs, int idx, uint def)
        {
            try
            {
                return uint.Parse(strs[idx]);
            }
            catch (Exception) { }
            return def;
        }

        static public float ArrayFloat(string[] strs, int idx, float def)
        {
            try
            {
                return float.Parse(strs[idx]);
            }
            catch (Exception) { }
            return def;
        }

        static public double ArrayDouble(string[] strs, int idx, double def)
        {
            try
            {
                return double.Parse(strs[idx]);
            }
            catch (Exception) { }
            return def;
        }
    }
}
