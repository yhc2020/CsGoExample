﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    public partial class HomeForm : Form
    {
        MatrixShowForm _cameraMat;
        DataViewForm _dataShow;
        HomeOptionForm _homeOpt;

        public HomeForm()
        {
            InitializeComponent();
        }

        private void HomeForm_Load(object sender, EventArgs e)
        {
            {
                _cameraMat = new MatrixShowForm(Properties.Resources.Vision_Off);
                _cameraMat.TopLevel = false;
                _cameraMat.Dock = DockStyle.Fill;
                _cameraMat.Visible = true;
                tableLayoutPanel1.Controls.Add(_cameraMat, 0, 0);
            }
            {
                _dataShow = new DataViewForm();
                _dataShow.TopLevel = false;
                _dataShow.Dock = DockStyle.Fill;
                _dataShow.Visible = true;
                tableLayoutPanel2.Controls.Add(_dataShow, 0, 0);
            }
            {
                _homeOpt = new HomeOptionForm();
                _homeOpt.TopLevel = false;
                _homeOpt.Dock = DockStyle.Fill;
                _homeOpt.Visible = true;
                tableLayoutPanel2.Controls.Add(_homeOpt, 1, 0);
            }
            InitOption.InitCameraShow(_cameraMat);
        }
    }
}
