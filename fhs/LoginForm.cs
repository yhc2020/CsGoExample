﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fhs
{
    public partial class LoginForm : Form
    {
        public enum Mode
        {
            Undefined = -1,
            Operator = 0,
            Debug = 1,
            Engineer = 2
        }

        public bool _isLogin = false;
        public Mode _mode = Mode.Undefined;

        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            comboBox_user.SelectedIndex = (int)Mode.Operator;
            GlobalData.mainStrand.post(() => textBox_password.Focus());
        }

        private void button_login_Click(object sender, EventArgs e)
        {
            _mode = (Mode)comboBox_user.SelectedIndex;
            switch ((Mode)comboBox_user.SelectedIndex)
            {
                case Mode.Operator:
                    _isLogin = true;
                    break;
                case Mode.Debug:
                    _isLogin = true;
                    break;
                case Mode.Engineer:
                    _isLogin = true;
                    break;
                default:
                    break;
            }
            Close();
        }

        private void textBox_password_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keys.Enter == e.KeyCode)
            {
                button_login_Click(sender, e);
            }
        }
    }
}
